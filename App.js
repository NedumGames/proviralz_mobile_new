import React, { Component } from "react";
import { Linking, Platform } from "react-native";
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { createDrawerNavigator } from "react-navigation-drawer";
// import { AppLoading } from "expo";
// import * as Font from "expo-font";
import DrawerPanel from "./src/screens/DrawerPanel";
import Home from "./src/screens/Home";
import Login from "./src/screens/Login";
import PasswordReset from "./src/screens/PasswordReset";
import ForgetPassword from "./src/screens/ForgetPassword";
import Profile from "./src/screens/Profile";
import Transactions from "./src/screens/Transactions";
import Signup from "./src/screens/Signup";
import Splash from "./src/screens/Splash";
import EditProfile from "./src/screens/EditProfile";
import Upload from "./src/screens/Upload";
import Activity from "./src/screens/Activity";
import MusicPlayer from "./src/screens/MusicPlayer";
import VerifyPhone from "./src/screens/VerifyPhone";
import Downloads from "./src/screens/Downloads";
import AdminScreen from './src/screens/Admin';

import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
// import firebase from '@react-native-firebase/app';

const DrawerNavigation = createDrawerNavigator({
  Splash: Splash,
  DrawerPanel: DrawerPanel,
  Home: Home,
  Login: Login,
  PasswordReset: PasswordReset,
  ForgetPassword: ForgetPassword,
  VerifyPhone: VerifyPhone,
  Profile: Profile,
  Downloads: Downloads,
  Transactions: Transactions,
  MusicPlayer: MusicPlayer,
  Signup: Signup,
  EditProfile: EditProfile,
  Activity: Activity,
  Upload: Upload,
  AdminScreen: AdminScreen,
});

const StackNavigation = createStackNavigator(
  {
    DrawerNavigation: {
      screen: DrawerNavigation
    },
    Splash: Splash,
    DrawerPanel: DrawerPanel,
    Home: {
      screen: Home,
      path: 'mobile'
    },
    Login: Login,
    PasswordReset: PasswordReset,
    VerifyPhone: VerifyPhone,
    ForgetPassword: ForgetPassword,
    Downloads: Downloads,
    Profile: Profile,
    Upload: Upload,
    Activity: Activity,
    EditProfile: EditProfile,
    Transactions: Transactions,
    MusicPlayer: {
      screen: MusicPlayer,
      path: "mobile/:id", // /:src/:title/:artist/:audio/:video/:genre/:token"
    },
    Signup: Signup,
    AdminScreen: AdminScreen
  },
  {
    headerMode: "none"
  }
);

const credentials = {
  clientId: '824493360358-lf2nj0i5bdcectb2udst15ou6t74g2og.apps.googleusercontent.com',
  appId: '1:824493360358:android:e61b96b8b1563463daa727',
  authDomain: 'proviralzmobile.firebaseapp.com',
  apiKey: 'AIzaSyAwAVBJW3QOPNF722nLz4aisioVwY50U3o',
  databaseURL: 'https://proviralzmobile.firebaseio.com',
  storageBucket: 'proviralzmobile.appspot.com',
  messagingSenderId: '824493360358',
  projectId: 'proviralzmobile',
};

const AppContainer = createAppContainer(StackNavigation);

class App extends Component {

  state = {
    isLoadingComplete: false
  }

  async loadResourcesAsync() {

    await Promise.all([
      Font.loadAsync({
        "roboto-regular": require("./src/assets/fonts/roboto-regular.ttf")
      })
    ]);
  }

  handleLoadingError(error) {
    console.warn(error);
  }

  handleFinishLoading(setLoadingComplete) {
    this.setState({ isLoadingComplete: true })
  }

  render() {
    const prefix = 'proviralzapp://mobile';
    return (
      <PaperProvider theme={theme}>
        <AppContainer
          uriPrefix={prefix}
          startAsync={this.loadResourcesAsync}
          onError={this.handleLoadingError}
          onFinish={() => this.handleFinishLoading(this.state.setLoadingComplete)}
        />
      </PaperProvider>
    )
  }
}

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: "rgba(241, 98, 35, 1)",
    accent: 'white',
  },
};

export default App;
