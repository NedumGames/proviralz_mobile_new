import React, { useState, Component } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  Image,
  ImageBackground,
  Platform,
  TouchableOpacity,
  ToastAndroid,
  ActivityIndicator,
  Alert,
  TextInput,
  Text,
  ScrollView,
  TouchableOpacityBase
} from "react-native";
import DropDownPicker from 'react-native-dropdown-picker';
import { Center } from "@builderx/utils";
import Divider from "../components/Divider";
import { Picker } from "@react-native-community/picker";
import ToolBar from "../components/ToolBar";
import Header from "../components/Header";
import { Uploads, AdminUploads } from "../components/Uploads";
import DocumentPicker from 'react-native-document-picker';
import RNFetchBlob from 'rn-fetch-blob';
import FilePickerManager from 'react-native-file-picker';
var CryptoJS = require("crypto-js");
import * as RNFS from 'react-native-fs';
import ImagePicker from 'react-native-image-picker';
import Icon from "react-native-vector-icons/FontAwesome";
import Loading from '../components/Loading';
import AsyncStorage from '@react-native-community/async-storage';
import { Button, FAB } from "react-native-paper";
import { isUndefined } from "lodash";

const genre_box = [
  { label: 'Hip-Hop', value: 'Hip-Hop' },
  { label: 'Rock', value: 'Rock' },
  { label: 'Jazz', value: 'Jazz' },
  { label: 'Pop Music', value: 'Pop Music' },
  { label: 'Raggae', value: 'Raggae' },
  { label: 'R&B', value: 'R&B' },
  { label: 'Disco', value: 'Disco' },
  { label: 'Raggae', value: 'Raggae' },
  { label: 'Soul', value: 'Soul' },
  { label: 'Gospel', value: 'Gospel' },
  { label: 'Orchestra', value: 'Orchestra' },
  { label: 'Afro Hip-Hop', value: 'Afro Hip-Hop' },
  { label: 'Trance', value: 'Trance' },
  { label: 'Trap', value: 'Trap' },
  { label: 'Latin', value: 'Latin' },
  { label: 'Indie Rock', value: 'Indie Rock' },
  { label: 'Ambient Music', value: 'Ambient Music' },
  { label: 'Country Music', value: 'Country Music' },
  { label: 'Afro Beats', value: 'Afro Beats' },
  { label: 'High Live', value: 'High live' },
  { label: 'Rap', value: 'Rap' },
]

class Upload extends Component {

  state = {
    uploadPage: 0,
    file_info: {},
    audio_file: {},
    cover_file: {},
    video_file: {},
    pic: "",
    loading: false,
    audiofile: "",
    videofile: "",
    coverfile: "",
    title: "",
    genre: "0",
    token: "",
    artist: "",
    description: "",
    musicData: [],
    nouploads: false,
    userid: "",
    songId: "",
    edit_song: false,
    usertype: 1
  }

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.runCheck()
  }

  async runCheck() {
    await this.fetchUserDetails()
    if (this.state.usertype == 1) {
      await this.getMyPost()
    } else {
      await this.getAllPost()
    }
    this.noUploadsView()
  }

  async fetchUserDetails() {
    this.setState({ loading: true })
    await AsyncStorage.getItem("userdetails", (err, result) => {
      var _res = JSON.parse(result)
      if (!isUndefined(_res.user.username)) {
        this.setState({ artist: _res.user.username })
        this.setState({ token: _res.token })
        this.setState({ usertype: _res.user.type })

        this.setState({ loading: false })
      }
    });
  }

  cloud = 'proviral'
  preset = "f0ohodol";
  upload_url = `https://api.cloudinary.com/v1_1/${this.cloud}/image/upload`
  video_url = `https://api.cloudinary.com/v1_1/${this.cloud}/video/upload`

  options = {
    title: 'Select Avatar',
    customButtons: [{ name: 'fb', title: 'Choose Photo' }],
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };


  selectAudioFile = () => {
    this.setState({ loading: true })
    // Pick a single file
    try {
      if (Platform.OS === "android") {
        FilePickerManager.showFilePicker(null, (response) => {
          if (!isUndefined(response.type))
            if (response && response.type.substring(0, response.type.lastIndexOf('/')) !== "audio") {
              this.setState({ loading: false })
              Alert.alert("Upload Error",
                "Hi! this file is not an audio file, Try again.",
                [{
                  text: "Ok"
                }],
                { cancelable: false }
              )
              return;
            }
          if (response.didCancel) {
            this.setState({ loading: false })
            return;
          }
          else if (response.error) {
            this.setState({ loading: false })
            return;
          }

          const uri = `file://${response.path}`;
          const type = response.type;
          const filename = response.fileName;
          const source = {
            uri,
            type,
            filename,
          }
          this.setState({ file_info: source })
          this.uploadAudio(response)
        });

      } else {
        const response = DocumentPicker.pick({
          type: [DocumentPicker.types.audio],
        });
        const uri = response.uri;
        const type = response.type;
        const name = response.name;
        const source = {
          uri,
          type,
          name,
        }
        this.setState({ file_info: source })
        this.uploadAudio(source)
      }
    } catch (err) {
      this.setState({ loading: false })
      if (DocumentPickerHandle.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  }

  selectVideoFile = () => {
    this.setState({ loading: true })
    // Pick a single file
    try {
      if (Platform.OS === "android") {
        FilePickerManager.showFilePicker(null, (response) => {
          if (!isUndefined(response.type))
            if (response && response.type.substring(0, response.type.lastIndexOf('/')) !== "video") {
              this.setState({ loading: false })
              Alert.alert("Upload Error",
                "Hi! this file is not a video, Try again.",
                [{
                  text: "Ok"
                }],
                { cancelable: false }
              )
              return;
            }

          if (response.didCancel) {
            this.setState({ loading: false })
            return;
          }
          else if (response.error) {
            this.setState({ loading: false })
            return;
          }

          const uri = `file://${response.path}`;
          const type = response.type;
          const filename = response.fileName;
          const source = {
            uri,
            type,
            filename,
          }
          this.setState({ video_file: source })
          this.uploadVideo(response)
        });

      } else {
        const response = DocumentPicker.pick({
          type: [DocumentPicker.types.video],
        });
        const uri = response.uri;
        const type = response.type;
        const name = response.name;
        const source = {
          uri,
          type,
          name,
        }
        this.setState({ video_file: source })
        this.uploadVideo(source)
      }
    } catch (err) {
      this.setState({ loading: false })
      if (DocumentPickerHandle.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  }

  uploadVideo = (file) => {

    // readFile(filepath: string, encoding?: string)
    RNFS.readFile(file.path, 'base64').then(res => {

      let xhr = new XMLHttpRequest();
      xhr.open('POST', this.video_url);
      xhr.onreadystatechange = () => {
        try {
          var json_ = {}
          json_ = JSON.parse(xhr.response)
          this.setState({ videofile: json_.url })
          this.setState({ loading: false })
        }
        catch (e) {
          this.setState({ loading: false })
        }
      };

      let formdata = new FormData();
      formdata.append('file', "data:image/jpeg;base64," + res);
      formdata.append('upload_preset', this.preset);
      formdata.append('resource_type', "video");
      xhr.send(formdata);
    })
      .catch(err => {
        this.setState({ loading: false })
      });

  }

  uploadAudio = (file) => {

    // readFile(filepath: string, encoding?: string)
    RNFS.readFile(file.path, 'base64').then(res => {

      let xhr = new XMLHttpRequest();
      xhr.open('POST', this.video_url);
      xhr.onreadystatechange = () => {
        try {
          var json_ = {}
          json_ = JSON.parse(xhr.response)
          this.setState({ audiofile: json_.url })
          this.setState({ loading: false })
        }
        catch (e) {
          this.setState({ loading: false })
        }
      };

      let formdata = new FormData();
      formdata.append('file', "data:image/jpeg;base64," + res);
      formdata.append('upload_preset', this.preset);
      formdata.append('resource_type', "video");
      xhr.send(formdata);
    })
      .catch(err => {
        this.setState({ loading: false })
      });

  }

  selectImageFile = () => {
    this.setState({ loading: true })
    // Open Image Library:
    ImagePicker.launchImageLibrary(this.options, (response) => {
      this.setState({ pic: `file://${response.path}` })
      if (!response.didCancel) {
        this.setState({ cover_file: response })
        this.setState({ coverfile: "data:image/jpeg;base64," + response.data })
      } else {
        ToastAndroid.showWithGravity(
          "Cover upload canceled by user.",
          ToastAndroid.LONG,
          ToastAndroid.CENTER
        )
      }
      this.setState({ loading: false })
    });

  }

  uploadSong = () => {
    const _data = {
      "user_id": this.state.userid,
      "cover": this.state.coverfile,
      "artist": this.state.artist,
      "audio": this.state.audiofile,
      "video": this.state.videofile,
      "title": this.state.title,
      "description": this.state.description,
      "genre": this.state.genre
    }

    if (this.state.coverfile.length < 5 ||
      this.state.artist.length < 1 ||
      this.state.audiofile.length < 5 ||
      this.state.title.length < 1 ||
      this.state.genre.length < 1) {
      Alert.alert("Upload Error",
        "Hi! Your music details are not complete, ensure required files are selected or URL to required file are entered correctly.",
        [{
          text: "Ok"
        }],
        { cancelable: false }
      )
      return;
    }

    this.setState({ loading: true })
    // this.addMusic(_data)

    fetch("https://proviralzmobileapp.proviralz.com/api/createpost", {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        "authorization": `Bearer ${this.state.token}`
      },
      body: JSON.stringify(_data) // body data type must match "Content-Type" header
    })
      .then(res => res.json())
      .then((result) => {
        this.setState({ loading: false })
        if (!result.success) {
          Alert.alert(
            'Upload Error.',
            `Hi!, ${result.message}`,
            [
              {
                text: 'Ok',
              }
            ],
            { cancelable: false }
          );
          return;
        } else {

          Alert.alert(
            'Upload Successful!',
            'Hi! Your Song has been created successful.',
            [
              {
                text: 'Got it',
                onPress: () => {
                  this.setState({ uploadPage: 0 })
                  this.getMyPost()
                }
              }
            ],
            { cancelable: false }
          );
        }
      })
      .catch((e) => {
        ToastAndroid.showWithGravity(
          "Network Error",
          ToastAndroid.LONG,
          ToastAndroid.CENTER
        )
        console.log(e, " Error")
        this.setState({ loading: false })
      })
  }

  getMyPost = async () => {
    this.setState({ loading: true })
    await this.fetchUserDetails()
    await fetch("https://proviralzmobileapp.proviralz.com/api/getpostbyid", {
      method: 'GET', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        "authorization": `Bearer ${this.state.token}`
      },
      // body: JSON.stringify(_data) // body data type must match "Content-Type" header
    })
      .then(res => res.json())
      .then((result) => {
        this.setState({ loading: false })
        if (!result.success) {
          Alert.alert(
            'Upload Error.',
            `Hi!, ${result.message}`,
            [
              {
                text: 'Ok',
              }
            ],
            { cancelable: false }
          );
          return;
        } else {
          this.setState({ musicData: result.data })
        }
      })
      .catch((e) => {
        this.setState({ loading: false })
      })
  }

  getAllPost = async () => {

    this.setState({
      loading: true
    })

    try {
      await fetch("http://proviralzmobileapp.proviralz.com/api/getallpostadmin", {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'authorization': `Bearer ${this.state.token}`
        },
        // body: JSON.stringify(signupdata) // body data type must match "Content-Type" header
      })
        .then(res => res.json())
        .then(result => {
          this.setState({
            musicData: result.data,
            loading: false
          })
        })
    } catch (error) {
      this.setState({
        loading: false
      })
    }
  }

  editMusic = (data) => {
    this.setState({
      edit_song: true,
      uploadPage: 1,
      audiofile: data.audio,
      videofile: data.video,
      coverfile: data.cover,
      artist: data.artist,
      title: data.title,
      genre: data.genre,
      description: data.description,
      songId: data.id
    })
  }

  deleteMusic = (data) => {
    Alert.alert(
      'Song delete confirmation.',
      `Do you want to delete this song?`,
      [
        {
          text: 'No',
        },
        {
          text: 'Yes',
          onPress: async () => {
            await this.finalDelete(data)
          }
        }
      ],
      { cancelable: false }
    );
  }

  finalDelete = async (data) => {
    this.setState({ loading: true })
    var id = data.id
    await fetch("https://proviralzmobileapp.proviralz.com/api/deletepost/" + id, {
      method: 'PUT', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        "authorization": `Bearer ${this.state.token}`
      },
      body: JSON.stringify(_data) // body data type must match "Content-Type" header
    })
      .then(res => res.json())
      .then((result) => {
        this.setState({ loading: false })
        if (!result.success) {
          Alert.alert(
            'Song delete Error.',
            `Hi!, ${result.message}`,
            [
              {
                text: 'Ok',
              }
            ],
            { cancelable: false }
          );
          return;
        } else {

          Alert.alert(
            'Song deleted Successful!',
            'Hi! Your Song has been deleted successful.',
            [
              {
                text: 'Okay',
                onPress: () => {
                  this.getMyPost()
                }
              }
            ],
            { cancelable: false }
          );
        }
      })
      .catch((e) => {
        this.setState({ loading: false })
      })
  }

  updateSong = async () => {
    const _data = {
      "user_id": this.state.userid,
      "cover": this.state.coverfile,
      "artist": this.state.artist,
      "audio": this.state.audiofile,
      "video": this.state.videofile,
      "title": this.state.title,
      "description": this.state.description,
      "genre": this.state.genre
    }

    if (this.state.coverfile.length < 5 ||
      this.state.artist.length < 5 ||
      this.state.audiofile.length < 5 ||
      this.state.title.length < 1 ||
      this.state.genre.length < 5) {
      Alert.alert("Upload Error",
        "Hi! Your music details are not complete, ensure required files are selected or URL to required file are entered correctly.",
        [{
          text: "Ok"
        }],
        { cancelable: false }
      )
      return;
    }

    this.setState({ loading: true })
    this.addMusic(_data)
    await this.fetchUserDetails()

    fetch("https://proviralzmobileapp.proviralz.com/api/updatepost/" + this.state.songId, {
      method: 'PUT', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        "authorization": `Bearer ${this.state.token}`
      },
      body: JSON.stringify(_data) // body data type must match "Content-Type" header
    })
      .then(res => res.json())
      .then((result) => {
        this.setState({ loading: false })
        if (!result.success) {
          Alert.alert(
            'Upload Error.',
            `Hi!, ${result.message}`,
            [
              {
                text: 'Ok',
              }
            ],
            { cancelable: false }
          );
          return;
        } else {

          Alert.alert(
            'Upload Successful!',
            'Hi! Your Song has been created successful.',
            [
              {
                text: 'Got it',
                onPress: () => {
                  this.getMyPost()
                }
              }
            ],
            { cancelable: false }
          );
        }
      })
      .catch((e) => {
        ToastAndroid.showWithGravity(
          "Network Error",
          ToastAndroid.LONG,
          ToastAndroid.CENTER
        )
        console.log(e, " Error")
        this.setState({ loading: false })
      })
  }

  changeStatus = async (id) => {

    this.setState({
      loading: true
    })

    try {
      await fetch("http://proviralzmobileapp.proviralz.com/api/updatepoststatus/" + id, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'authorization': `Bearer ${this.state.token}`
        },
        // body: JSON.stringify(signupdata) // body data type must match "Content-Type" header
      })
        .then(res => res.json())
        .then(result => {
          this.setState({
            musicData: result.data,
            loading: false
          })
          this.getAllPost()
        })
    } catch (error) {
      this.setState({
        loading: false
      })
    }
  }

  changeStatusBack = async (id) => {

    this.setState({
      loading: true
    })

    try {
      await fetch("http://proviralzmobileapp.proviralz.com/api/returnpoststatus/" + id, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'authorization': `Bearer ${this.state.token}`
        },
        // body: JSON.stringify(signupdata) // body data type must match "Content-Type" header
      })
        .then(res => res.json())
        .then(result => {
          this.setState({
            musicData: result.data,
            loading: false
          })
          this.getAllPost()
        })
    } catch (error) {
      this.setState({
        loading: false
      })
    }
  }

  noUploadsView() {
    // this.setState({ loading: true })
    // setTimeout(() => {
    //   this.setState({ loading: false })
    //   if (this.state.musicData.length < 1 && this.state.uploadPage == 0) {
    //     this.setState({ nouploads: true })
    //   }
    // }, 5000);
  }

  render() {
    return (
      <View style={styles.rect}>
        <StatusBar hidden />
        <View style={styles.scrollArea}>
          <Header navigation={this.props.navigation} page="Upload" />
          {this.state.nouploads == true ?
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: "center",
                paddingTop: 100,
                flexDirection: "column"
              }}
            >
              <Text
                style={{
                  width: 200,
                  color: "rgba(0,0,0,0.3)",
                  fontSize: 13,
                  textAlign: "center",
                  marginBottom: 20,
                  lineHeight: 25
                }}
              >No uploaded songs yet, upload your very first song.</Text>
              <TouchableOpacity
                style={{
                  borderWidth: 1,
                  borderRadius: 5,
                  borderColor: "#ddd",
                  padding: 10
                }}
                onPress={() => this.setState({
                  edit_song: false,
                  uploadPage: 1,
                  audiofile: "",
                  videofile: "",
                  coverfile: "",
                  artist: "",
                  title: "",
                  genre: "",
                  description: "",
                  songId: ""
                })
                }
              >
                <Text
                  style={{
                    color: "#666",
                    fontSize: 14
                  }}
                >Upload Song</Text>
              </TouchableOpacity>
            </View>
            :
            this.state.uploadPage == 0 ?
              this.state.usertype == 1 ?
                <ScrollView horizontal={false}
                  style={{}}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text style={{ fontWeight: "bold", fontSize: 20, margin: 20, flex: 1 }}>My Uploads</Text>
                    <FAB
                      icon="upload"
                      label="Upload song"
                      color="#000"
                      style={{ backgroundColor: "grey", margin: 20 }}
                      onPress={() => {
                        this.setState({ uploadPage: 1 })
                      }}
                    />
                  </View>
                  <Uploads usertoken={this.state.token} deleteSong={this.deleteMusic} editsong={this.editMusic} navigation={this.props.navigation} musicData={this.state.musicData} />
                </ScrollView>
                :
                <>
                  <Text style={{ fontWeight: "bold", fontSize: 20, margin: 30 }}>My Uploads</Text>
                  <AdminUploads usertoken={this.state.token} changestatusback={this.changeStatusBack} changestatus={this.changeStatus} editsong={this.editMusic} navigation={this.props.navigation} musicData={this.state.musicData} />
                </>
              : null
          }
          {this.state.loading ? <ActivityIndicator style={{ marginTop: 150, marginBottom: 150 }} size="large" color="#000" /> : null}
          {this.state.uploadPage == 1 ?
            <ScrollView
              horizontal={false}
              contentContainerStyle={styles.scrollArea_contentContainerStyle}
            >
              {
                this.state.usertype == 1 ?
                  <View style={styles.actions}>

                    <View >
                      <Text style={{ color: "#666" }}>Song Title*</Text>
                      <TextInput
                        // placeholder="Phone Number"
                        clearButtonMode="always"
                        placeholderTextColor="rgba(0,0,0,0.5)"
                        style={styles.phoneInput}
                        onChange={value => {
                          this.setState({ title: value.nativeEvent.text })
                        }}
                      >{this.state.title}</TextInput>
                      <Text style={{ color: "#666", marginTop: 10 }}>Genre*</Text>
                      <View
                        style={styles.textPicker}
                      >
                        <Picker
                          mode="dialog"
                          selectedValue={this.state.genre}
                          style={{
                            height: 40,
                          }}
                          onValueChange={(itemValue, itemIndex) => {
                            this.setState({ genre: itemValue })
                          }}
                        >
                          <Picker.Item label="Select Genre" color="grey" value="0" />
                          {genre_box.length > 0 && genre_box.map((gen, indx) => {
                            return <Picker.Item key={`item_${indx}`} label={gen.label} value={gen.value} />
                          })}
                        </Picker>
                      </View>
                      <Text style={{ color: "#666", marginTop: 10 }}>Description*</Text>
                      <TextInput
                        multiline={true}
                        numberOfLines={5}
                        textAlignVertical="top"
                        clearButtonMode="always"
                        placeholderTextColor="rgba(0,0,0,0.5)"
                        style={styles.textarea}
                        onChange={value => {
                          this.setState({ description: value.nativeEvent.text })
                        }}
                      >{this.state.description}</TextInput>

                      <View
                        style={{
                          flexDirection: "row"
                        }}
                      >
                        <View
                          style={{ flexDirection: "column", alignItems: "center", marginTop: 20, flex: 1 }}
                        >
                          <FAB
                            style={{
                              color: "#fff",
                              backgroundColor: "grey",
                              width: 55
                            }}
                            size={30}
                            medium
                            icon={this.state.pic ? "check" : "image"}
                            onPress={() => this.selectImageFile()}
                          />
                          <Text style={styles.uploadtext}>Select cover image* </Text>
                        </View>
                        <View
                          style={{ flexDirection: "column", alignItems: "center", marginTop: 20, flex: 1 }}
                        >
                          <FAB
                            style={{
                              color: "#fff",
                              backgroundColor: "grey",
                              width: 55
                            }}
                            size={30}
                            medium
                            icon={this.state.audiofile.length > 0 ? "check" : "music"}
                            onPress={() => this.selectAudioFile()}
                          />
                          <Text style={styles.uploadtext}>Select audio file* </Text>
                        </View>
                      </View>
                      <View
                        style={{ flexDirection: "column", alignItems: "center", justifyContent: "flex-start", marginTop: 20 }}
                      >
                        <FAB
                          style={{
                            color: "#fff",
                            backgroundColor: "grey",
                            width: 55
                          }}
                          size={30}
                          medium
                          icon={this.state.videofile.length > 0 ? "check" : "video"}
                          onPress={() => this.selectVideoFile()}
                        />
                        <Text style={styles.uploadtext}>Select Video file* </Text>
                      </View>
                      <View
                        style={{
                          flexDirection: "row"
                        }}
                      >
                        <TouchableOpacity style={styles.rectDisabled}
                          disabled={this.state.loading}
                          onPress={() =>
                            this.setState({ uploadPage: 0 })
                          }
                        >

                          <Text style={{
                            color: "rgba(0,0,0,0.8)",
                            fontSize: 14,
                            lineHeight: 20,
                            alignSelf: "center",
                            elevation: 5
                          }}>Cancel</Text>
                        </TouchableOpacity>
                        {this.state.edit_song ?
                          <TouchableOpacity style={styles.rect3}
                            disabled={this.state.loading}
                            onPress={() =>
                              this.updateSong()
                            }
                          >
                            <Text style={{
                              color: "rgba(0,0,0,0.8)",
                              fontSize: 14,
                              lineHeight: 20,
                              alignSelf: "center",
                              elevation: 5
                            }}>Update</Text>
                          </TouchableOpacity>
                          :
                          <TouchableOpacity style={styles.rect3}
                            disabled={this.state.loading}
                            onPress={() =>
                              this.uploadSong()
                            }
                          >
                            <Text style={{
                              color: "rgba(0,0,0,0.8)",
                              fontSize: 14,
                              lineHeight: 20,
                              alignSelf: "center",
                              elevation: 5
                            }}>Submit</Text>
                          </TouchableOpacity>
                        }
                      </View>
                    </View>

                  </View>
                  : null}
            </ScrollView> : null}
          <View style={{ flexDirection: "row", paddingBottom: 20 }}>
          </View>
          <ToolBar navigation={this.props.navigation} active={2} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  rect: {
    flex: 1
  },
  camerabutton: {
    height: 50,
    flex: 1,
    backgroundColor: "rgba(241, 98, 35, 1)",
    borderRadius: 10,
    justifyContent: "center",
    margin: 20,
    alignSelf: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    padding: 10
  },
  dropdownstyle: {
    backgroundColor: "#e4e7e8",
    alignSelf: "flex-start",
    textAlign: "left",
    alignContent: "flex-start",
  },
  editor: {
    flexDirection: "row",
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5
  },
  editText: {
    width: "50%",
    height: 40,
    color: "rgba(0,0,0,0.7)",
    borderRadius: 5,
    fontSize: 14,
    borderColor: "#ddd",
    borderWidth: 1,
    backgroundColor: "#e4e7e8",
    lineHeight: 20,
    marginTop: 10,
    marginLeft: 20,
    marginRight: 20,
    padding: 5
  },
  actions: {
    flexDirection: "column",
    marginTop: 30,
    paddingLeft: 20,
    paddingRight: 20,

  },
  textPicker: {
    width: "100%",
    // height: 45,
    color: "rgba(0,0,0,0.7)",
    borderRadius: 5,
    fontSize: 14,
    borderColor: "#ddd",
    borderWidth: 1,
    backgroundColor: "#e4e7e8",
    lineHeight: 20,
    marginTop: 5,
    marginLeft: 0,
    marginRight: 0,
    padding: 5
  },
  phoneInput: {
    width: "100%",
    height: 45,
    color: "#000",
    borderRadius: 5,
    fontSize: 15,
    borderColor: "#ddd",
    borderWidth: 1,
    backgroundColor: "#e4e7e8",
    lineHeight: 20,
    marginTop: 5,
    marginLeft: 0,
    marginRight: 0,
    padding: 5,
    paddingLeft: 10
  },
  textarea: {
    width: "100%",
    height: 70,
    color: "#000",
    borderRadius: 5,
    fontSize: 15,
    borderColor: "#ddd",
    borderWidth: 1,
    backgroundColor: "#e4e7e8",
    lineHeight: 20,
    marginTop: 5,
    marginLeft: 0,
    marginRight: 20,
    padding: 5,
    paddingLeft: 10
  },
  textstat: {
    marginTop: 10,
    // color: "",
    fontSize: 15,
  },
  uploadtext: {
    marginTop: 10,
    marginBottom: 30,
    color: "#666",
    textAlign: "center",
    fontSize: 13,
  },
  actionText: {
    marginTop: 0,
    marginLeft: 10,
    color: "#fff",
    fontSize: 14,
    justifyContent: "center"
  },
  rect2: {
    backgroundColor: "#000",
    // flex: 1,
    height: "15%"
  },
  image: {
    height: "100%",
    // flexDirection: "row"
  },
  button: {
    width: 40,
    height: 40,
    backgroundColor: "rgba(28,42,56,0.8)",
    opacity: 0.76,
    borderRadius: 100,
    justifyContent: "center"
  },
  icon2: {
    color: "rgba(255,255,255,1)",
    fontSize: 24,
    alignSelf: "center"
  },
  icon: {
    color: "rgba(255,255,255,1)",
    fontSize: 25,
    height: 27,
    width: 25,
    marginLeft: 264,
    marginTop: 5
  },
  buttonRow: {
    height: 40,
    flexDirection: "row",
    flex: 1,
    marginRight: 24,
    marginLeft: 22,
    marginTop: 25
  },
  image2: {
    width: 75,
    height: 75,
    borderRadius: 100,
    // borderColor: "#000",
    borderWidth: 5,
    alignSelf: "center",
    marginTop: 30,
    justifyContent: "center"
  },
  statimage: {
    width: 20,
    marginTop: 30,
    marginLeft: 30,
    tintColor: "#666",
    height: 20
  },
  uploadimage: {
    width: 50,
    marginTop: 30,
    // marginLeft: 30,
    tintColor: "#666",
    height: 50
  },
  image3: {
    width: 20,
    // height: 35,
    marginLeft: 20,
    tintColor: "#fff",
    height: 20
  },
  text10: {
    color: "rgba(255,255,255,0.8)",
    fontSize: 18,
    lineHeight: 20,
    marginTop: 20,
    alignSelf: "center",
    justifyContent: "center"
  },
  profileText2: {
    color: "rgba(255,255,255,0.5)",
    fontSize: 13,
    lineHeight: 20,
    marginTop: 10,
    alignSelf: "center",
    marginBottom: 20
    // justifyContent: "center"
  },
  childText: {
    color: "rgba(255,255,255,0.7)",
    fontSize: 15,
    lineHeight: 20,
    marginTop: 10,
    alignSelf: "center",
    justifyContent: "center"
  },
  rect5: {
    width: 134,
    height: 20,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  icon4: {
    color: "#798894",
    fontSize: 15
  },
  profileText: {
    color: "#798894",
    fontSize: 13,
    lineHeight: 20
  },
  rect4: {
    width: 163,
    height: 20,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  icon3: {
    color: "#798894",
    fontSize: 15
  },
  text2: {
    color: "#798894",
    fontSize: 16,
    lineHeight: 20
  },
  rect5Row: {
    // flexDirection: "row",
    alignItems: "center",
    marginTop: 10,
    marginBottom: 5
    // marginLeft: 22,
    // marginRight: 49
  },
  rect7: {
    width: 92,
    height: 21,
    // flexDirection: "row",
    justifyContent: "space-around",
    alignSelf: "center",
    marginBottom: 4
  },
  text6: {
    color: "rgba(255,255,255,1)",
    fontSize: 14,
    lineHeight: 20
  },
  visitors: {
    color: "#798894",
    fontSize: 14,
    lineHeight: 20
  },
  rect6: {
    width: 92,
    height: 21,
    flexDirection: "row",
    justifyContent: "space-between",
    marginLeft: 27,
    marginTop: 6
  },
  text4: {
    color: "rgba(255,255,255,1)",
    fontSize: 14,
    lineHeight: 20
  },
  likes: {
    color: "#798894",
    fontSize: 14,
    lineHeight: 20,
    alignSelf: "center",
    textAlign: "left"
  },
  text: {
    color: "#000",
    fontSize: 30,
    lineHeight: 20,
    alignSelf: "center"
  },
  rect3: {
    height: 50,
    flex: 1,
    width: "50%",
    backgroundColor: "rgba(241, 98, 35, 1)",
    borderRadius: 100,
    justifyContent: "center",
    marginTop: 20,
    marginLeft: 10,
    marginRight: 20,
    marginBottom: 20,
    alignSelf: "center",
  },
  rectDisabled: {
    height: 50,
    flex: 1,
    width: "50%",
    backgroundColor: "grey",
    borderRadius: 100,
    justifyContent: "center",
    marginTop: 20,
    marginLeft: 10,
    marginRight: 20,
    marginBottom: 20,
    alignSelf: "center",
  },
  actionButton: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: "rgba(241, 98, 35, 1)",
    borderRadius: 100,
    marginTop: 5,
    marginLeft: 5,
    marginRight: 20,
    height: 35,
    alignItems: "center"
  },
  text: {
    color: "#fff",
    fontSize: 14,
    lineHeight: 20,
    alignSelf: "center"
  },
  rect7Row: {
    height: 30,
    flexDirection: "row",
    marginTop: 12,
    marginLeft: 20,
    marginRight: 20
  },
  scrollArea: {
    backgroundColor: "rgba(255,255,255,1)",
    width: "100%",
    height: "10%",
    flex: 1
  },
  scrollArea_contentContainerStyle: {
    // height: 1902,
    width: "100%",
    paddingBottom: 50,
    marginBottom: 50
  },
  rect8: {
    height: 51,
    flexDirection: "row",
    justifyContent: "space-around"
  },
  divider: {
    top: 50,
    left: 0,
    height: 1,
    position: "absolute",
    right: 0
  },
  rect9: {
    flex: 0.21,
    borderColor: "#1da1f2",
    borderWidth: 0,
    borderBottomWidth: 4
  },
  myPosts: {
    color: "#1da1f2",
    fontSize: 18,
    lineHeight: 20,
    marginTop: 15,
    alignSelf: "center"
  },
  icon6: {
    top: 0,
    left: 47,
    position: "absolute",
    color: "rgba(128,128,128,1)",
    fontSize: 40,
    height: 43,
    width: 40
  },
  icon7: {
    top: 0,
    left: 160,
    position: "absolute",
    color: "rgba(28,33,82,1)",
    fontSize: 40,
    height: 43,
    width: 40
  },
  rect11: {
    flex: 0.15
  },
  icon8: {
    top: 0,
    left: 262,
    position: "absolute",
    color: "rgba(28,33,82,1)",
    fontSize: 40,
    height: 43,
    width: 40
  },
  rect12: {
    flex: 0.16
  },
  tweet2: {
    width: 360,
    height: 149
  },
  divider3: {
    height: 1,
    marginTop: 178
  },
  tweet: {
    width: 360,
    height: 149,
    marginTop: 1
  }
});

export default Upload;
