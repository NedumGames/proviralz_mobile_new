import React, { Component } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  Image,
  Text,
  TouchableOpacity
} from "react-native";
// import IoniconsIcon from "react-native-vector-icons/Ionicons";
import { Center } from "@builderx/utils";
import Divider from "../components/Divider";
// import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
// import MaterialIconsIcon from "react-native-vector-icons/MaterialIcons";

function DrawerPanel(props) {
  return (
    <View style={styles.rect}>
      <StatusBar hidden />
      <View style={styles.rect2}>
        <View style={styles.rect11}>
          <View style={styles.imageColumn}>
            <Image
              source={require("../assets/images/IMG_20190324_163843_1.jpg")}
              resizeMode="cover"
              style={styles.image}
            ></Image>
            <View style={styles.text8ColumnRow}>
              <View style={styles.text8Column}>
                <Text style={styles.text8}>Ankur Kedia</Text>
                <Text style={styles.text9}>@theankurkedia</Text>
              </View>
              {/* <IoniconsIcon
                name="ios-arrow-down"
                style={styles.icon8}
              ></IoniconsIcon> */}
            </View>
          </View>
          <View style={styles.imageColumnFiller}></View>
          <View style={styles.rect12Row}>
            <View style={styles.rect12}>
              <Text style={styles.text10}>211</Text>
              <Text style={styles.views}>Views</Text>
            </View>
            <View style={styles.rect13}>
              <Text style={styles.text12}>122</Text>
              <Center horizontal>
                <Text style={styles.likes}>Likes</Text>
              </Center>
            </View>
          </View>
        </View>
        <Divider style={styles.divider4}></Divider>
        <View style={styles.rect7}>
          <TouchableOpacity
            onPress={() => props.navigation.navigate("Profile")}
            style={styles.button}
          >
            <View style={styles.icon4Row}>
              {/* <FontAwesomeIcon
                name="home"
                style={styles.icon4}
              ></FontAwesomeIcon> */}
              <Text style={styles.home}>Home</Text>
            </View>
          </TouchableOpacity>
          <View style={styles.rect8}>
            <View style={styles.icon5Row}>
              {/* <MaterialIconsIcon
                name="dashboard"
                style={styles.icon5}
              ></MaterialIconsIcon> */}
              <Text style={styles.dashboard}>Dashboard</Text>
            </View>
          </View>
          <View style={styles.rect9}>
            <View style={styles.icon6Row}>
              {/* <MaterialIconsIcon
                name="bookmark-border"
                style={styles.icon6}
              ></MaterialIconsIcon> */}
              <Text style={styles.makeAPost}>Make a Post</Text>
            </View>
          </View>
          <View style={styles.rect10}>
            <View style={styles.icon7Row}>
              {/* <MaterialIconsIcon
                name="file-download"
                style={styles.icon7}
              ></MaterialIconsIcon> */}
              <Text style={styles.downloads}>Downloads</Text>
            </View>
          </View>
          <View style={styles.group}>
            <View style={styles.icon9Row}>
              {/* <MaterialIconsIcon
                name="show-chart"
                style={styles.icon9}
              ></MaterialIconsIcon> */}
              <Text style={styles.transaction}>Transaction</Text>
            </View>
          </View>
          <View style={styles.group2}>
            <View style={styles.icon10Row}>
              {/* <MaterialIconsIcon
                name="settings"
                style={styles.icon10}
              ></MaterialIconsIcon> */}
              <Text style={styles.profile}>Profile</Text>
            </View>
          </View>
        </View>
        <TouchableOpacity
          onPress={() => props.navigation.navigate("Home")}
          style={styles.button1}
        >
          <Text style={styles.logOut}>Log Out</Text>
        </TouchableOpacity>
        <Divider style={styles.divider}></Divider>
        <View style={styles.rect4}>
          <Text style={styles.text}>Settings and privacy</Text>
          <Text style={styles.text2}>Help Center</Text>
        </View>
      </View>
      <TouchableOpacity
        style={styles.back}
        onPress={() => props.navigation.goBack(null)}
      ></TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  rect: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: "#141f28",
  },
  back: {
    width: 100
  },
  rect2: {
    width: 250,
    backgroundColor: "rgba(51,60,137,1)",
    elevation: 54,
    shadowOffset: {
      height: 9,
      width: 3
    },
    shadowColor: "rgba(0,0,0,1)",
    shadowOpacity: 0.4,
    shadowRadius: 18,
    flex: 1
  },
  rect11: {
    width: 188,
    height: 123,
    marginTop: 20,
    marginLeft: 22
  },
  image: {
    width: 50,
    height: 50,
    borderRadius: 100,
    marginLeft: 1
  },
  text8: {
    color: "rgba(255,255,255,1)",
    fontSize: 16,
    lineHeight: 20,
    marginLeft: 1
  },
  text9: {
    color: "#798894",
    fontSize: 14,
    lineHeight: 20,
    marginTop: 1
  },
  text8Column: {
    width: 104
  },
  icon8: {
    color: "#1da5f8",
    fontSize: 20,
    marginLeft: 119,
    marginTop: 3
  },
  text8ColumnRow: {
    height: 41,
    flexDirection: "row",
    marginTop: 6
  },
  imageColumn: {
    marginRight: -48
  },
  imageColumnFiller: {
    flex: 1
  },
  rect12: {
    width: 92,
    height: 21,
    flexDirection: "row",
    justifyContent: "space-around"
  },
  text10: {
    color: "rgba(255,255,255,1)",
    fontSize: 14,
    lineHeight: 20
  },
  views: {
    color: "#798894",
    fontSize: 14,
    lineHeight: 20
  },
  rect13: {
    width: 92,
    height: 21,
    flexDirection: "row",
    justifyContent: "space-between",
    marginLeft: 3
  },
  text12: {
    color: "rgba(255,255,255,1)",
    fontSize: 14,
    lineHeight: 20
  },
  likes: {
    color: "#798894",
    fontSize: 14,
    lineHeight: 20,
    textAlign: "left"
  },
  rect12Row: {
    height: 21,
    flexDirection: "row",
    alignItems: "flex-end",
    marginRight: 1
  },
  divider4: {
    width: 276,
    height: 1,
    marginTop: 5
  },
  rect7: {
    height: 312,
    justifyContent: "space-around",
    marginTop: 1,
    marginLeft: 15,
    marginRight: 6
  },
  button: {
    width: 228,
    height: 25,
    flexDirection: "row"
  },
  icon4: {
    color: "#8899a6",
    fontSize: 25
  },
  home: {
    color: "#fefefe",
    fontSize: 18,
    lineHeight: 20,
    marginLeft: 20,
    marginTop: 3
  },
  icon4Row: {
    height: 25,
    flexDirection: "row",
    flex: 1,
    marginRight: 135
  },
  rect8: {
    width: 137,
    height: 25,
    flexDirection: "row"
  },
  icon5: {
    color: "#8899a6",
    fontSize: 25
  },
  dashboard: {
    color: "#fefefe",
    fontSize: 18,
    lineHeight: 20,
    marginLeft: 18,
    marginTop: 3
  },
  icon5Row: {
    height: 25,
    flexDirection: "row",
    flex: 1,
    marginRight: 5
  },
  rect9: {
    width: 137,
    height: 25,
    flexDirection: "row"
  },
  icon6: {
    color: "#8899a6",
    fontSize: 25
  },
  makeAPost: {
    color: "#fefefe",
    fontSize: 18,
    lineHeight: 20,
    marginLeft: 18,
    marginTop: 3
  },
  icon6Row: {
    height: 25,
    flexDirection: "row",
    flex: 1,
    marginRight: -5
  },
  rect10: {
    width: 137,
    height: 25,
    flexDirection: "row"
  },
  icon7: {
    color: "#8899a6",
    fontSize: 25
  },
  downloads: {
    color: "#fefefe",
    fontSize: 18,
    lineHeight: 20,
    marginLeft: 18,
    marginTop: 3
  },
  icon7Row: {
    height: 25,
    flexDirection: "row",
    flex: 1,
    marginRight: 3
  },
  group: {
    width: 137,
    height: 25,
    flexDirection: "row"
  },
  icon9: {
    color: "#8899a6",
    fontSize: 25
  },
  transaction: {
    color: "#fefefe",
    fontSize: 18,
    lineHeight: 20,
    marginLeft: 18,
    marginTop: 3
  },
  icon9Row: {
    height: 25,
    flexDirection: "row",
    flex: 1,
    marginRight: 1
  },
  group2: {
    width: 137,
    height: 25,
    flexDirection: "row"
  },
  icon10: {
    color: "#8899a6",
    fontSize: 25
  },
  profile: {
    color: "#fefefe",
    fontSize: 18,
    lineHeight: 20,
    marginLeft: 18,
    marginTop: 3
  },
  icon10Row: {
    height: 25,
    flexDirection: "row",
    flex: 1,
    marginRight: 41
  },
  button1: {
    width: 215,
    height: 50,
    backgroundColor: "rgba(239,17,25,1)",
    borderRadius: 100,
    justifyContent: "center",
    marginTop: 30,
    marginLeft: 29
  },
  logOut: {
    color: "#ffffff",
    fontSize: 24,
    lineHeight: 20,
    alignSelf: "center"
  },
  divider: {
    width: 276,
    height: 1,
    marginTop: 44
  },
  rect4: {
    width: 249,
    height: 93,
    justifyContent: "space-around",
    marginTop: 25,
    marginLeft: 20
  },
  text: {
    color: "#fefefe",
    fontSize: 18,
    lineHeight: 20
  },
  text2: {
    color: "#fefefe",
    fontSize: 18,
    lineHeight: 20
  }
});

export default DrawerPanel;
