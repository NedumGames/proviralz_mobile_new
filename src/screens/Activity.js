import React, { useState, useEffect, Component } from "react";
import {
    StyleSheet,
    View,
    StatusBar,
    Image,
    ImageBackground,
    TouchableOpacity,
    //   AsyncStorage,
    TextInput,
    Alert,
    Text,
    ScrollView,
    TouchableOpacityBase
} from "react-native";
import DropDownPicker from 'react-native-dropdown-picker';
import { Center } from "@builderx/utils";
import Divider from "../components/Divider";
import ToolBar from "../components/ToolBar";
import Header from "../components/Header";
import Uploads from "../components/Uploads";
import AsyncStorage from '@react-native-community/async-storage';
import Loading from '../components/Loading';


function Activity(props) {

    const [token, setToken] = useState("");
    const [id, setId] = useState("");
    const [musicdata, setMusicData] = useState([])
    const [loading, setLoading] = useState(false);

    const data = musicdata

    function getMyPost() {
        setLoading(true)
        const userDetails = AsyncStorage.getItem("userdetails", (err, result) => {
            if (err)
                props.navigation.navigate("Login")
            var _res = JSON.parse(result)

            fetch("https://proviralzmobileapp.proviralz.com/api/getactivity/" + _res.user.id, {
                method: 'GET', // *GET, POST, PUT, DELETE, etc.
                mode: 'cors', // no-cors, *cors, same-origin
                cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                credentials: 'same-origin', // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    "authorization": `Bearer ${_res.token}`
                },
                // body: JSON.stringify(_data) // body data type must match "Content-Type" header
            })
                .then(res => res.json())
                .then((result) => {

                    setLoading(false)
                    if (!result.success) {
                        Alert.alert(
                            'Data Error.',
                            `Hi!, ${result.message}`,
                            [
                                {
                                    text: 'Ok',
                                }
                            ],
                            { cancelable: false }
                        );
                        return;
                    } else {
                        setMusicData(result.data)
                    }
                })
                .catch((e) => {
                    setLoading(false)

                })
        })

    }

    function manageActivity() {
        const show = []
        const rows = []
        const post_likes = []
        const post_comment = []
        post_comment.push(
            <View style={{ flexDirection: "row" }}>
                <Image
                    source={require("../assets/images/chat.png")}
                    resizeMode="cover"
                    style={styles.commentimage}
                ></Image>
                <Text style={{ marginLeft: 10, marginTop: 30, color: "#05cd51", fontSize: 11, alignSelf: "center" }}>
                    Commented on your post
            </Text>
            </View>
        )
        post_likes.push(
            <View style={{ flexDirection: "row" }}>
                <Image
                    source={require("../assets/images/heart.png")}
                    resizeMode="contain"
                    style={styles.likeimage}
                ></Image>
                <Text style={{ marginLeft: 10, marginTop: 30, color: "rgba(241, 98, 35, 1)", fontSize: 11, alignSelf: "center" }}>
                    Likes your post
            </Text>
            </View>
        )
        rows.push(<Header navigation={props.navigation} page="Activities" />)

        for (let i = 0; i < data.length; i++) {
            show.push(
                <View style={styles.capsules}>
                    <View style={{ flex: 1 }} >
                        <Image
                            source={{ uri: data[i].actor_pic }}
                            resizeMode="cover"
                            style={styles.musicimage}
                        ></Image>
                        <Text style={{ marginLeft: 20, marginTop: 5, marginBottom: 10, alignSelf: "center" }}>@{data[i].actor}</Text>
                    </View>
                    <View style={{ flex: 1, height: 50 }}>
                        {data[i].activity == "like" ?
                            post_likes[0] :
                            post_comment[0]}
                    </View>
                    <View style={{ flex: 1, marginTop: 10 }}>
                        <TouchableOpacity>
                            <Image
                                source={{ uri: data[i].post }}
                                resizeMode="cover"
                                style={styles.statimage}
                            ></Image>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }
        rows.push(<ScrollView horizontal={false}>{show}</ScrollView>)

        rows.push(<ToolBar navigation={props.navigation} active={3} />)

        if (musicdata.length < 1) {
            return (
                <>
                    <Header navigation={props.navigation} page="Activities" />
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: "center"
                        }}
                    >
                        <Text
                            style={{
                                width: 200,
                                color: "rgba(0,0,0,0.3)",
                                fontSize: 13,
                                textAlign: "center",
                                marginBottom: 20,
                                lineHeight: 25
                            }}
                        >No activities available at the moment, upload a song or share your song to the world.</Text>
                        <TouchableOpacity
                            style={{
                                borderWidth: 1,
                                borderRadius: 5,
                                borderColor: "#ddd",
                                padding: 10
                            }}
                            onPress={() => props.navigation.navigate("Upload")}
                        >
                            <Text
                                style={{
                                    color: "#666",
                                    fontSize: 14
                                }}
                            >Upload or share song</Text>
                        </TouchableOpacity>
                    </View>
                    <ToolBar navigation={props.navigation} active={3} />
                </>
            )
        }

        return (rows);
    }

    useEffect(() => {
        getMyPost()

    }, [])

    return (

        // loading ?
        //     <Loading load={loading} />
        //     :
            manageActivity()
    )
}

const styles = StyleSheet.create({
    likeimage: {
        width: 15,
        height: 15,
        marginTop: 0,
        tintColor: "rgba(241, 98, 35, 1)",
        alignSelf: "flex-end"
    },
    commentimage: {
        width: 15,
        height: 15,
        marginTop: 0,
        tintColor: "#05cd51",
        alignSelf: "flex-end"
    },
    statimage: {
        width: 50,
        height: 50,
        marginTop: 0,
        marginRight: 10,
        // tintColor: "rgba(241, 98, 35, 1)",
        alignSelf: "flex-end"
    },
    musicimage: {
        width: 50,
        height: 50,
        borderRadius: 100,
        marginTop: 5,
        marginLeft: 10,
        // tintColor: "rgba(241, 98, 35, 1)",
        alignSelf: "center"
    },
    statimage2: {
        width: 15,
        height: 15,
        marginTop: 20,
        tintColor: "rgba(241, 98, 35, 1)",
        alignSelf: "center"
    },
    capsules: {
        // borderWidth: 1,
        // borderTopColor: "transparent",
        // borderLeftColor: "transparent",
        // borderRightColor: "transparent",
        // borderBottomColor: "#ddd",
        flexDirection: "row",
        width: "90%",
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10
    },
    divider: {
        height: 1,
        backgroundColor: "#000",
        alignSelf: "stretch"
    },
});

export default Activity;