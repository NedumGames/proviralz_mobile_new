import React, { useState, Component } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  Image,
  ImageBackground,
  TouchableOpacity,
  Alert,
  ScrollView,
  TouchableOpacityBase
} from "react-native";
import Divider from "../components/Divider";
import ToolBar from "../components/ToolBar";
import Header from "../components/Header";
import Loading from "../components/Loading";
import Earnings from "../components/Earnings";
import AsyncStorage from '@react-native-community/async-storage';
import { isUndefined } from "lodash";
import Modal from "react-native-modal";
import { TextInput, Text, Button } from 'react-native-paper';
import * as base64 from "base-64";
import * as utf8 from "utf8";



class Profile extends Component {

  state = {
    user: {
      name: "",
      username: ""
    },
    token: "",
    userid: 0,
    loading: false,
    likes: 0,
    views: 0,
    userid: 0,
    token: "",
    musicData: [],
    showModal: false,

    amount: 0,
    location: "",
    bankname: "",
    bankaccountname: "",
    bankaccountnumber: ""
  }
  ctr = 0

  constructor(prop) {
    super(prop)

    this.state = {
      user: {
        username: "",
        name: ""
      }
    }
  }

  componentDidMount() {
    this.getUserDetails()
  }

  async getPosts(_res) {
    this.setState({ loading: true })
    await fetch("https://proviralzmobileapp.proviralz.com/api/getpostbyid", {
      method: 'GET', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        "authorization": `Bearer ${_res.token}`
      },
      // body: JSON.stringify(_data) // body data type must match "Content-Type" header
    })
      .then(res => res.json())
      .then((result) => {
        this.setState({ loading: false })
        if (!result.success) {
          Alert.alert(
            'Upload Error.',
            `Hi!, ${result.message}`,
            [
              {
                text: 'Ok',
              }
            ],
            { cancelable: false }
          );
          return;
        } else {
          this.setState({ musicData: result.data })
        }
      })
      .catch((e) => {
        this.setState({ loading: false })
      })
  }

  async getUserDetails() {
    this.setState({ loading: true })

    const userdetails = await AsyncStorage.getItem('userdetails', (err, res) => {
      const _res = JSON.parse(res);

      if (isUndefined(_res.user)) {
        this.setState({ loading: true })

        Alert.alert(
          'Profile update error!',
          `Hi!, there is an issue getting your profile information, please try again or sign out and login again, Thank you.`,
          [
            {
              text: 'Logout',
              onPress: async () => {
                const save = await AsyncStorage.removeItem("userdetails", (err, yes) => {
                  this.props.navigation.navigate("Login")
                })
              }
            },
            {
              text: 'Try again.',
              onPress: () => {
                this.getUserDetails()
              }
            }
          ],
          { cancelable: false }
        );

      } else {
        this.fetchProfile(_res)
        this.getPosts(_res)
        this.setState({ user: _res })
      }
    })
  }

  async fetchProfile(_res) {
    this.setState({token: _res.token})
    await fetch("https://proviralzmobileapp.proviralz.com/api/getuserbyid/" + _res.user.id, {
      method: 'GET', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        "Authorization": `Bearer ${_res.token}`
      },
      // body: JSON.stringify(data) // body data type must match "Content-Type" header
    })
      .then(res => res.json())
      .then(result => {

        if (result.success === true) {
          this.setState({
            user: result.user,
            loading: false,
            likes: result.likes,
            views: result.views
          })
          return;
        } else {
          Alert.alert(
            'Profile update error!',
            `Hi!, there is an issue getting your profile information, please try again or sign out and login again, Thank you.`,
            [
              {
                text: 'Logout',
                onPress: () => {
                  const save = AsyncStorage.removeItem("userdetails", (err, yes) => {
                    this.props.navigation.navigate("Login")
                  })
                }
              },
              {
                text: 'Try again.',
                onPress: () => {
                  this.getUserDetails()
                }
              }
            ],
            { cancelable: false }
          );
        }

        this.setState({ loading: false })
      })
      .catch((e) => {

        this.setState({ loading: false })
      })
  }

  showWithDrawForm() {
    var totalearnings = this.state.user.earnings
    var withdraws = this.state.user.withdraws

    if ((totalearnings - withdraws) < this.state.amount) {
      Alert.alert(
        'Withdrawal process terminated!',
        `Hi!, your wallet balance is lesser than the amount requested!`,
        [
          {
            text: 'Ok',
          }
        ],
        { cancelable: false }
      );
      return
    } else if (totalearnings - withdraws < 1000) {
      Alert.alert(
        'Withdrawal process terminated!',
        "Hi!, your wallet balance is less than the withdrawal limit of ₦ 1000",
        [
          {
            text: 'Ok',
          }
        ],
        { cancelable: false }
      );
      return
    } else
      this.setState({ showModal: true })
  }

  async withDraw() {    

    if (this.state.amount.length < 3 || this.state.location.length < 5 || this.state.bankname.length < 2 || this.state.bankaccountnumber.length < 5 || this.state.bankaccountname.length < 5){
      Alert.alert(
        'Withdrawal request Error.',
        `Hi!, please ensure you enter the required fields.`,
        [
          {
            text: 'Ok',
          }
        ],
        { cancelable: false }
      );
      return
    }

    var data = {
      amount : this.state.amount,
      location: this.state.location,
      bankname: this.state.bankname,
      bankaccountname: this.state.bankaccountname,
      bankaccountnumber: this.state.bankaccountnumber
    }

    this.setState({ loading: true })

    await fetch("https://proviralzmobileapp.proviralz.com/api/makewithdrawal", {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        "authorization": `Bearer ${this.state.token}`
      },
      body: JSON.stringify(data) // body data type must match "Content-Type" header
    })
      .then(res => res.json())
      .then((result) => {
        this.setState({ loading: false })
        if (!result.success) {
          Alert.alert(
            'Withdrawal Error.',
            `Hi!, ${result.message}`,
            [
              {
                text: 'Ok',
              }
            ],
            { cancelable: false }
          );
          return;
        } else {
          Alert.alert(
            'Withdrawal request is processing.',
            `Hi!, ${result.message}`,
            [
              {
                text: 'Ok',
              }
            ],
            { cancelable: false }
          );
        }
      })
      .catch((e) => {
        this.setState({ loading: false })
      })
  }

  editProfile() {
    this.props.navigation.push("EditProfile", {
      "user": this.state.user,
    })
  }

  render() {
    return (
      <View style={styles.rect}>
        <StatusBar hidden />
        {/* <Loading load={this.state.loading} /> */}
        <Modal
          isVisible={this.state.showModal}
          animationIn="slideInUp"
          animationInTiming={500}
          animationOut="slideOutDown"
          animationOutTiming={500}
          onBackButtonPress={() => {
            this.setState({ showModal: false })
          }}
        >
          <View
            style={{ flex: 1, justifyContent: "center", alignContent: "center", flexDirection: "column" }}
          >
            <View style={{
              backgroundColor: "#fff",
              margin: 20,
              // height: 200,
              borderRadius: 5,
              flexDirection: "column",
              padding: 10
            }}>

              <Text style={{ marginTop: 10, marginBottom: 0 }}>Enter your withdraw amount:</Text>
              <TextInput
                placeholder="amount"
                mode="outlined"
                style={{
                  height: 40
                }}
                value={this.state.amount}
                onChangeText={text => this.setState({ amount: text })}
              />
              <Text style={{ marginTop: 10, marginBottom: 0 }}>Enter your location</Text>
              <TextInput
                placeholder="location"
                mode="outlined"
                style={{
                  height: 40
                }}
                value={this.state.location}
                onChangeText={text => this.setState({ location: text })}
              />
              <Text style={{ marginTop: 10, marginBottom: 0 }}>Enter your Bank name:</Text>
              <TextInput
                placeholder="bank name"
                mode="outlined"
                style={{
                  height: 40
                }}
                value={this.state.bankname}
                onChangeText={text => this.setState({ bankname: text })}
              />
              <Text style={{ marginTop: 10, marginBottom: 0 }}>Enter your bank account name:</Text>
              <TextInput
                placeholder="account name"
                mode="outlined"
                style={{
                  height: 40
                }}
                value={this.state.bankaccountname}
                onChangeText={text => this.setState({ bankaccountname: text })}
              />
              <Text style={{ marginTop: 10, marginBottom: 0 }}>Enter your bank account number:</Text>
              <TextInput
                placeholder="account number"
                mode="outlined"
                style={{
                  height: 40
                }}
                value={this.state.bankaccountnumber}
                onChangeText={text => this.setState({ bankaccountnumber: text })}
              />
              <Button style={{ marginTop: 20, marginBottom: 20 }} mode="outlined" onPress={() => this.withDraw()}>
                Submit
              </Button>
            </View>
          </View>
        </Modal>

        <View style={styles.scrollArea}>
          <Header navigation={this.props.navigation} page="Profile" />
          <ScrollView
            horizontal={false}
            contentContainerStyle={styles.scrollArea_contentContainerStyle}
          >
            <View style={styles.rect2}>
              <ImageBackground
                source={{ uri: this.state.user.profile_image }}
                resizeMode="cover"
                style={styles.image}
              >
                <View style={{ width: "100%", height: "100%", backgroundColor: "rgba(0,0,0,0.6)" }}>

                  <Image
                    source={{ uri: this.state.user.profile_image }}
                    resizeMode="cover"
                    style={styles.image2}
                  ></Image>
                  {this.state.user != undefined ?
                  <Text
                    style={{
                      fontSize: 15,
                      color: "#fff",
                      margin: 10,
                      textAlign: "center"
                    }}
                  >{this.state.user.name && this.state.user.name + "\n\n" + "@" + this.state.user.username} </Text> : null }
                  <Text style={styles.childText}>{this.state.user.profession}</Text>
                  <View style={styles.rect5Row}>
                    <Text style={styles.profileText}>{this.state.user.location}</Text>
                  </View>
                  <View style={styles.rect7Row}>
                    <View style={styles.rect7}>
                      <Text style={styles.profileText2}>Likes</Text>
                      <Text style={styles.childText}>{this.state.likes}</Text>
                    </View>
                    <View style={styles.rect7}>
                      <Text style={styles.profileText2}>Views</Text>
                      <Text style={styles.childText}>{this.state.views}</Text>
                    </View>
                    <TouchableOpacity
                      style={styles.rect3}

                      onPress={() => this.editProfile()}
                    >
                      <Text style={styles.text}>Edit profile</Text>
                    </TouchableOpacity>
                  </View>

                </View>
              </ImageBackground>
            </View>
            <View style={styles.actions}>
              <TouchableOpacity
                style={styles.actionButton}
                onPress={() => this.props.navigation.push("Transactions")}
                // disabled
              >
                <Image
                  source={require("../assets/images/notebook.png")}
                  resizeMode="cover"
                  style={styles.image3}
                ></Image>
                <Text style={styles.actionText}>Transaction</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.actionButton}
                onPress={() => this.props.navigation.push("Downloads")}
              >
                <Image
                  source={require("../assets/images/download.png")}
                  resizeMode="cover"
                  style={styles.image3}
                ></Image>
                <Text style={styles.actionText}>Downloads</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.actionButton}
                onPress={() => this.showWithDrawForm()}
              >
                <Image
                  source={require("../assets/images/001-money.png")}
                  resizeMode="cover"
                  style={styles.image3}
                ></Image>
                <Text style={styles.actionText}>Withdraw</Text>
              </TouchableOpacity>
            </View>
            <View style={{ flexDirection: "row" }}>
              <Image
                source={require("../assets/images/001-money.png")}
                resizeMode="cover"
                style={styles.statimage}
              ></Image>
              <Text style={styles.textstat}>Post Stat</Text>
            </View>

            <Earnings usertoken={this.state.token} editsong={this.editMusic} navigation={this.props.navigation} musicData={this.state.musicData} />
            <View style={{
              height: 100
            }} />
          </ScrollView>
          <ToolBar navigation={this.props.navigation} active={4} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  rect: {
    flex: 1
  },
  editor: {
    flexDirection: "row",
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5
  },
  editText: {
    width: "50%",
    height: 40,
    color: "rgba(0,0,0,0.7)",
    borderRadius: 5,
    fontSize: 14,
    borderColor: "#ddd",
    borderWidth: 1,
    backgroundColor: "#e4e7e8",
    lineHeight: 20,
    marginTop: 10,
    marginLeft: 20,
    marginRight: 20,
    padding: 5
  },
  actions: {
    flexDirection: "row",
    // flex: 1,
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10
  },
  textstat: {
    marginTop: 30,
    marginLeft: 10,
    color: "#70757a",
    fontSize: 15,
  },
  actionText: {
    marginTop: 0,
    marginLeft: 3,
    color: "#fff",
    fontSize: 11,
    justifyContent: "center"
  },
  rect2: {
    backgroundColor: "#000",
    // flex: 1,
    height: 300
  },
  image: {
    height: "100%",
    // flexDirection: "row"
  },
  button: {
    width: 40,
    height: 40,
    backgroundColor: "rgba(28,42,56,0.8)",
    opacity: 0.76,
    borderRadius: 100,
    justifyContent: "center"
  },
  icon2: {
    color: "rgba(255,255,255,1)",
    fontSize: 24,
    alignSelf: "center"
  },
  icon: {
    color: "rgba(255,255,255,1)",
    fontSize: 25,
    height: 27,
    width: 25,
    marginLeft: 264,
    marginTop: 5
  },
  buttonRow: {
    height: 40,
    flexDirection: "row",
    flex: 1,
    marginRight: 24,
    marginLeft: 22,
    marginTop: 25
  },
  image2: {
    width: 75,
    height: 75,
    borderRadius: 100,
    // borderColor: "#000",
    borderWidth: 5,
    alignSelf: "center",
    marginTop: 30,
    justifyContent: "center"
  },
  statimage: {
    width: 20,
    marginTop: 30,
    marginLeft: 30,
    tintColor: "#666",
    height: 20
  },
  image3: {
    width: 15,
    // height: 35,
    marginLeft: 10,
    tintColor: "#fff",
    height: 15
  },
  text10: {
    color: "rgba(255,255,255,0.8)",
    fontSize: 18,
    lineHeight: 20,
    marginTop: 20,
    alignSelf: "center",
    justifyContent: "center"
  },
  profileText2: {
    color: "rgba(255,255,255,0.7)",
    fontSize: 13,
    lineHeight: 20,
    marginTop: 10,
    alignSelf: "center",
    marginBottom: 20
    // justifyContent: "center"
  },
  childText: {
    color: "rgba(255,255,255,0.7)",
    fontSize: 15,
    lineHeight: 20,
    marginTop: 10,
    alignSelf: "center",
    justifyContent: "center"
  },
  rect5: {
    width: 134,
    height: 20,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  icon4: {
    color: "#798894",
    fontSize: 15
  },
  profileText: {
    color: "#798894",
    fontSize: 13,
    lineHeight: 20
  },
  rect4: {
    width: 163,
    height: 20,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  icon3: {
    color: "#798894",
    fontSize: 15
  },
  text2: {
    color: "#798894",
    fontSize: 16,
    lineHeight: 20
  },
  rect5Row: {
    // flexDirection: "row",
    alignItems: "center",
    marginTop: 0,
    marginBottom: 5
    // marginLeft: 22,
    // marginRight: 49
  },
  rect7: {
    width: 92,
    height: 21,
    // flexDirection: "row",
    justifyContent: "space-around",
    alignSelf: "center",
    marginBottom: 4
  },
  text6: {
    color: "rgba(255,255,255,1)",
    fontSize: 14,
    lineHeight: 20
  },
  visitors: {
    color: "#798894",
    fontSize: 14,
    lineHeight: 20
  },
  rect6: {
    width: 92,
    height: 21,
    flexDirection: "row",
    justifyContent: "space-between",
    marginLeft: 27,
    marginTop: 6
  },
  text4: {
    color: "rgba(255,255,255,1)",
    fontSize: 14,
    lineHeight: 20
  },
  likes: {
    color: "#798894",
    fontSize: 14,
    lineHeight: 20,
    alignSelf: "center",
    textAlign: "left"
  },
  rect3: {
    height: 35,
    flex: 1,
    width: "100%",
    backgroundColor: "rgba(241, 98, 35, 1)",
    borderRadius: 100,
    justifyContent: "center",
    marginTop: 5,
    marginLeft: 10,
    marginRight: 20,
    alignSelf: "center",
  },
  actionButton: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: "rgba(241, 98, 35, 1)",
    borderRadius: 100,
    marginTop: 5,
    marginLeft: 5,
    marginRight: 20,
    height: 35,
    alignItems: "center"
  },
  text: {
    color: "#fff",
    fontSize: 14,
    lineHeight: 20,
    alignSelf: "center"
  },
  rect7Row: {
    height: 30,
    flexDirection: "row",
    marginTop: 5,
    marginLeft: 20,
    marginRight: 20
  },
  scrollArea: {
    backgroundColor: "rgba(255,255,255,1)",
    width: "100%",
    height: "10%",
    flex: 1
  },
  scrollArea_contentContainerStyle: {
    // height: 1002,
    width: 375
  },
  rect8: {
    height: 51,
    flexDirection: "row",
    justifyContent: "space-around"
  },
  divider: {
    top: 50,
    left: 0,
    height: 1,
    position: "absolute",
    right: 0
  },
  rect9: {
    flex: 0.21,
    borderColor: "#1da1f2",
    borderWidth: 0,
    borderBottomWidth: 4
  },
  myPosts: {
    color: "#1da1f2",
    fontSize: 18,
    lineHeight: 20,
    marginTop: 15,
    alignSelf: "center"
  },
  icon6: {
    top: 0,
    left: 47,
    position: "absolute",
    color: "rgba(128,128,128,1)",
    fontSize: 40,
    height: 43,
    width: 40
  },
  icon7: {
    top: 0,
    left: 160,
    position: "absolute",
    color: "rgba(28,33,82,1)",
    fontSize: 40,
    height: 43,
    width: 40
  },
  rect11: {
    flex: 0.15
  },
  icon8: {
    top: 0,
    left: 262,
    position: "absolute",
    color: "rgba(28,33,82,1)",
    fontSize: 40,
    height: 43,
    width: 40
  },
  rect12: {
    flex: 0.16
  },
  tweet2: {
    width: 360,
    height: 149
  },
  divider3: {
    height: 1,
    marginTop: 178
  },
  tweet: {
    width: 360,
    height: 149,
    marginTop: 1
  }
});

export default Profile;
