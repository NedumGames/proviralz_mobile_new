import React, { Component, useState, useEffect } from "react";
import {
    StyleSheet,
    View,
    StatusBar,
    Text,
    Alert,
    TextInput,
    TouchableOpacity,
    Image
} from "react-native";
import Loading from "../components/Loading";


function ForgetPassword(props) {

    const [email, setEmail] = useState("");
    const [load, setLoad] = useState(false);

    const sendPin = () => {

        var a = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        var n;
        var r = [];
        for (n = 1; n <= 7; ++n) {
            var i = Math.floor((Math.random() * (9 - n)) + 1);
            r.push(a[i]);
            a[i] = a[9 - n];
        }
        var pin = "";
        for (i = 0; i < 7; i++) {
            pin += r[i] + "";
        }

        setLoad(true)

        fetch("https://proviralzmobileapp.proviralz.com/api/forgetpassword/" + email + "/" + pin, {
            method: 'GET', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
        })
            .then(res => res.json())
            .then((result) => {

                setLoad(false)
                if (!result.success) {
                    Alert.alert(
                        'Forget password Error',
                        'Hi! your password recovery was not successful at the moment.',
                        [
                            {
                                text: 'ok',
                                onPress: async () => {
                                    props.navigation.navigate("Login")
                                }
                            }
                        ],
                        { cancelable: false }
                    );
                    return;
                } else {
                    Alert.alert(
                        'Password Recovery Message',
                        'Hi! a pin has been sent to your mail box, retrieve the pin and use it to update your password.',
                        [
                            {
                                text: 'ok',
                                onPress: async () => {
                                    props.navigation.push("PasswordReset")
                                }
                            }
                        ],
                        { cancelable: false }
                    );
                }
            })
            .catch((e) => {
                setLoad(false)
            })
    }

    return (
        <View style={styles.rect}>
            <StatusBar hidden />
            <Loading load={load} />
            <View style={styles.rect3}>
                <Image
                    source={require("../assets/images/proviralz_logo.jpg")}
                    resizeMode="contain"
                    style={styles.logo}
                ></Image>
                <Text style={styles.proviralz}>Forgot Password</Text>
            </View>
            <Text style={styles.logInToProviralz}>Enter the email address you registered with in the box below.</Text>

            <TextInput
                autoCompleteType="email"
                placeholder="Registered Email address"
                clearButtonMode="always"
                placeholderTextColor="rgba(0,0,0,0.5)"
                style={styles.phoneInput}
                onChange={value => {
                    setEmail(value.nativeEvent.text)
                }}
            ></TextInput>

            <View style={styles.materialButtonViolet2Row}>
                <TouchableOpacity
                    onPress={() => {
                        sendPin()
                    }}
                    style={styles.registerbutton}>
                    <Text style={styles.register}>Submit</Text>
                </TouchableOpacity>
            </View>
            <TouchableOpacity
                onPress={() => props.navigation.navigate("Login")}>
                <Text style={styles.text5}>Back to login.</Text>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    rect: {
        flex: 1,
        backgroundColor: "rgba(255,255,255,1)"
    },
    logo: {
        width: 30,
        height: 30,
        marginLeft: 20,
        marginBottom: 0,
        alignSelf: "center"
    },
    register: {
        color: "rgba(86,2,4,1)",
        fontSize: 15,
        lineHeight: 20,
        marginLeft: 20,
        marginRight: 20,
        alignSelf: "center"
    },
    or: {
        color: "rgba(0,0,0,1)",
        fontSize: 18,
        lineHeight: 20,
        marginTop: 20,
        marginLeft: 20,
        marginRight: 20,
    },
    rect3: {
        height: 50,
        flexDirection: "row",
        backgroundColor: "#000",
        marginTop: 0,
    },
    registerbutton: {
        height: 50,
        width: 100,
        backgroundColor: "rgba(241, 98, 35, 1)",
        borderRadius: 100,
        justifyContent: "center",
        marginTop: 0,
        alignSelf: "center",
    },
    googlebutton: {
        width: 150,
        height: 50,
        backgroundColor: "rgba(239,17,25,1)",
        borderRadius: 5,
        justifyContent: "center",
    },
    phoneInput: {
        width: "90%",
        height: 40,
        color: "rgba(0,0,0,0.7)",
        borderRadius: 5,
        fontSize: 14,
        borderColor: "#ddd",
        borderWidth: 1,
        backgroundColor: "#e4e7e8",
        lineHeight: 20,
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
        padding: 5
    },
    materialButtonViolet2Row: {
        flexDirection: "row",
        marginRight: 20,
        marginLeft: 20,
        marginTop: 50
    },
    proviralz: {
        color: "rgba(255,255,255,0.6)",
        fontSize: 16,
        alignSelf: "flex-start",
        marginTop: 15,
        textAlignVertical: "center",
        marginLeft: 20,
    },
    logInToProviralz: {
        color: "rgba(0,0,0,0.7)",
        fontSize: 12,
        lineHeight: 20,
        marginTop: 30,
        width: "75%",
        marginBottom: 100,
        textAlign: "center",
        alignSelf: "center"
    },
    loremIpsum: {
        top: 0,
        left: 108,
        position: "absolute",
        fontFamily: "roboto-regular",
        color: "#121212"
    },
    logInToProviralzStack: {
        width: 258,
        height: 50,
        marginTop: 147,
        marginLeft: 33
    },
    google: {
        color: "#ffffff",
        fontSize: 12,
        lineHeight: 20,
        marginLeft: 20,
        marginRight: 20,
        alignSelf: "center"
    },
    googlebutton: {
        width: 150,
        height: 50,
        backgroundColor: "rgba(239,17,25,1)",
        borderRadius: 5,
        justifyContent: "center",
    },
    rect2: {
        height: 70,
        backgroundColor: "rgba(86,2,4,1)",
        marginTop: 0
    },
    proviralz1: {
        color: "rgba(255,255,255,0.6)",
        fontSize: 24,
        alignSelf: "flex-start",
        marginTop: 20,
        marginLeft: 20,
    },
    text5: {
        color: "rgba(0,0,0,1)",
        fontSize: 11,
        lineHeight: 20,
        marginTop: 20,
        alignSelf: "center"
    },
    textInput3: {
        width: 312,
        height: 42,
        color: "rgba(0,0,0,1)",
        borderColor: "rgba(0,0,0,1)",
        borderWidth: 0,
        borderBottomWidth: 2,
        fontSize: 15,
        lineHeight: 20,
        marginTop: 20,
        marginLeft: 20,
        marginRight: 20,
    },
    textInput4: {
        width: 312,
        height: 42,
        color: "rgba(0,0,0,1)",
        borderColor: "rgba(0,0,0,1)",
        borderWidth: 0,
        borderBottomWidth: 2,
        fontSize: 15,
        lineHeight: 20,
        marginTop: 20,
        marginLeft: 20,
        marginRight: 20,
    },
    or2: {
        color: "rgba(0,0,0,1)",
        fontSize: 18,
        lineHeight: 20,
        marginTop: 20,
        marginLeft: 20,
        marginRight: 20,
    },
    materialButtonDanger: {
        height: 48,
        width: 150,
        marginTop: 0,
        marginLeft: 0
    },
    logInToProviralzStackColumn: {
        marginTop: -5,
        marginLeft: 1,
        marginRight: -1
    },
    logInToProviralzStackColumnFiller: {
        flex: 1
    },
    rect4: {
        height: 42,
        flexDirection: "row",
        marginTop: 20,
        marginLeft: 20,
        marginRight: 10
    },
    divider: {
        width: 360,
        height: 1
    },
    button2: {
        width: 100,
        height: 50,
        backgroundColor: "rgba(0,0,0,0)",
        borderColor: "rgba(86,2,4,1)",
        borderWidth: 2,
        borderRadius: 100,
        justifyContent: "center",
    },
    text6: {
        color: "rgba(86,2,4,1)",
        fontSize: 15,
        lineHeight: 20,
        marginLeft: 20,
        marginRight: 20,
        alignSelf: "center"
    }
});

export default ForgetPassword;