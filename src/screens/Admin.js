import React from 'react';
import {
    Text,
    TouchableOpacity,
    View,
    StyleSheet
} from 'react-native';
import ToolBar from "../components/ToolBar";
import Header from "../components/Header";


export default function AdminScreen(props) {


    return (
        <View style={styles.adminpage}>
            <Header navigation={props.navigation} page="Admin" />
            <Text style={{
                flex: 1
            }}>Admin</Text>
            <ToolBar navigation={props.navigation} active={5} />
        </View>
    )
}

const styles = StyleSheet.create({
    adminpage: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: "#fff"
    }
})