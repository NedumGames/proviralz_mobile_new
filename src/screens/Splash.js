import React, { Component, useEffect, useState } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  Text,
  TouchableOpacity,
  Image,
  Platform,
  Linking
} from "react-native";
import SongLinking from "../components/DeepLinkingSong";
import AsyncStorage from '@react-native-community/async-storage';
import Loading from "../components/Loading";
import * as base64 from "base-64";
import * as utf8 from "utf8";


function Splash(props) {


  return (
    <View style={styles.rect}>
      <StatusBar hidden />
      <Image
        source={require("../assets/images/bg.jpg")}
        resizeMode="cover"
        style={styles.backgroundimage}
      ></Image>
      <SongLinking {...props} />
      <View style={styles.bgcolor}>
        <View style={styles.textColumn}>
          <Image
            source={require("../assets/images/proviralz_logo.jpg")}
            resizeMode="contain"
            style={styles.image}
          ></Image>
          <Text style={styles.text}>
            A world where music is valueable; Share your music and live your dreams...
        </Text>
          <TouchableOpacity
            onPress={() =>
              props.navigation.push("Signup")
            }
            style={styles.button}
          >
            <Text style={styles.text2}>Create account</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          onPress={() => props.navigation.push("Login")}
          style={styles.button2}
        >
          <Text style={styles.text3}>Have an account already?</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            Linking.openURL("https://www.nedumstudios.com/me").catch((err) => console.error('An error occurred', err));
          }}
        >
          <View
            style={{
              flexDirection: "row",
              alignSelf: "center"
            }}
          >
            <Text style={styles.attribute}>Powered By</Text>
            <Text style={styles.attribute2}> Nedumstudios.</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  rect: {
    flex: 1,
    backgroundColor: "rgba(0,0,0,1)"
  },
  bgcolor: {
    flex: 1,
    backgroundColor: "rgba(0,0,0,0.8)"
  },
  backgroundimage: {
    width: "100%",
    height: "100%",
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    zIndex: -1000,
    opacity: 1
  },
  text: {
    color: "rgba(255,255,255,0.7)",
    fontSize: 20,
    // lineHeight: ,
    textAlign: "center",
    marginTop: "15%",
    width: "90%",
    alignSelf: "center",
  },
  button: {
    height: 50,
    backgroundColor: "rgba(241, 98, 35, 1)",
    borderRadius: 100,
    justifyContent: "center",
    marginTop: 50,
    alignSelf: "center",
    width: "75%"
  },
  text2: {
    color: "rgba(0,0,0,0.6)",
    fontSize: 15,
    alignSelf: "center"
  },
  image: {
    width: 100,
    height: 100,
    marginTop: 50,
    marginLeft: 0,
    marginBottom: 0,
    alignSelf: "center"
  },
  textColumn: {
    marginTop: 100,
    marginLeft: 31,
    marginRight: 31
  },
  textColumnFiller: {
    flex: 1
  },
  button2: {
    height: 39,
    marginBottom: "35%",
    marginLeft: 28,
    marginRight: 28,
    alignSelf: "center"
  },
  text3: {
    color: "rgba(255,255,255,0.7)",
    fontSize: 14,
    marginTop: 20
  },
  attribute: {
    color: "rgba(255,255,255,0.7)",
    fontSize: 10,
    // marginTop: "30%",
    // alignSelf: "center"
  },
  attribute2: {
    color: "rgba(241, 98, 35, 1)",
    fontSize: 10,
    // marginTop: "30%",
  }
});

export default Splash;
