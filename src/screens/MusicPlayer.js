import React, { useEffect, useState } from "react";
import {
    View,
    ScrollView,
    ImageBackground,
    Image,
    FlatList,
    TouchableOpacity,
    Text,
    Share,
    Alert,
    Dimensions,
    TextInput,
    TouchableWithoutFeedback,
} from "react-native";

import ToolBar from "../components/ToolBar";
import Header from "../components/Header";
import MusicList from "../components/MusicList";
import Comment from "../components/Comment";
import { Card } from '../components/Card';
import { Title } from '../components/Title';
import { TouchableHighlight } from "react-native-gesture-handler";
import TrackPlayer, { usePlaybackState } from 'react-native-track-player';
import AudioPlayer from '../components/AudioPlayer';
import VideoPlay from '../components/VideoPlayer';
import { isUndefined } from "lodash";
import AdVert from "../components/AdVert";
import * as base64 from "base-64";
import * as utf8 from "utf8";
import RNFS from 'react-native-fs';
import RNFetchBlob from 'rn-fetch-blob'
import { BannerAd, BannerAdSize, TestIds } from '@react-native-firebase/admob';


export default function MusicPlayer(props) {

    const playbackState = usePlaybackState();
    const adUnitId = __DEV__ ? TestIds.BANNER : 'ca-app-pub-4700820342978062/5098111024';
    var prop = null
    var isPlaying = false
    const [play_Video, setPlayVideo] = useState(false);
    const [share, setShare] = useState("");
    const [comment, setComments] = useState("");
    const [allcomment, setAllcomments] = useState([]);
    const [disableCommentSend, setDisableComment] = useState(false)

    const { id, src, title, artist, audio, video, genre, token } = props.navigation.state.params

    const music = {
        id: id,
        src: src,
        title: title,
        artist: artist,
        audio: audio,
        video: video,
        genre: genre
    }

    useEffect(() => {
        createView()
        setup();
        if (comment === "")
            setDisableComment(false)
    }, [])

    const getComments = () => {

        fetch("https://proviralzmobileapp.proviralz.com/api/getallcomments/" + id, {
            method: 'GET', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                "Authorization": `Bearer ${token}`
            },
            // body: JSON.stringify(_data) // body data type must match "Content-Type" header
        })
            .then(res => res.json())
            .then(rest => {
                if (rest.success == true) {
                    setAllcomments(rest.data)
                    return;
                }

            })
            .catch((e) => {
            })
    }

    const showComment = () => {
        var rest_ = []
        for (var i = 0; i < allcomment.length; i++) {
            rest_.push(
                <Comment
                    username={allcomment[i].username}
                    userpic={allcomment[i].profile_image}
                    comment={allcomment[i].comment}

                    navigation={props.navigation}
                />
            )
        }

        return (
            <ScrollView
                horizontal={false}
                contentContainerStyle={styles.scrollArea_contentContainerStyle}
                onMomentumScrollEnd={() => getComments()}
            >
                {rest_ ? rest_ :
                    <Text
                        style={{
                            color: "#fff",
                            textAlign: "center",
                            fontSize: 11,
                            fontStyle: "italic",
                            width: "75%",
                            alignSelf: "center",
                            marginTop: 20,
                            marginBottom: 20
                        }}
                    >No comment on this post yet, be the first to comment.</Text>}
            </ScrollView>
        );
    }

    async function setup() {
        getComments()
        TrackPlayer.setupPlayer({});
        TrackPlayer.updateOptions({
            stopWithApp: true,
            capabilities: [
                TrackPlayer.CAPABILITY_PLAY,
                TrackPlayer.CAPABILITY_PAUSE,
                TrackPlayer.CAPABILITY_SKIP_TO_NEXT,
                TrackPlayer.CAPABILITY_SKIP_TO_PREVIOUS,
                TrackPlayer.CAPABILITY_STOP
            ],
            compactCapabilities: [
                TrackPlayer.CAPABILITY_PLAY,
                TrackPlayer.CAPABILITY_PAUSE
            ]
        });
    }

    async function runAudio() {    
        getComments()
        await TrackPlayer.reset();
        // await TrackPlayer.add(playlistData);
        await TrackPlayer.add({
            id: 1,
            url: music.audio,
            title: music.title,
            artist: music.artist,
            artwork: music.src,
            //   duration: 28
        });
        await TrackPlayer.play();
        isPlaying = true
    }

    async function stopSong() {
        await TrackPlayer.stop()
    }

    function shareText() {
        var encodedText = utf8.encode(id.toString());
        return base64.encode(encodedText)
    }

    async function togglePlayback() {
        getComments()
        const currentTrack = await TrackPlayer.getCurrentTrack();
        if (currentTrack == null) {
            if (music.video && music.video.length > 0 && isPlaying == false) {
                Alert.alert(
                    'Music Video Available.',
                    'Hi! this music has a video, do you want to checking it out?',
                    [
                        {
                            text: 'Never mind',
                            onPress: () => {

                                runAudio()
                            }
                        },
                        {
                            text: 'Play Video', onPress: () => {
                                setPlayVideo(true)
                            }
                        }
                    ],
                    { cancelable: false }
                );
            } else {
                if (music.audio) {
                    runAudio()
                }
                else {
                    if (playbackState === TrackPlayer.STATE_PAUSED) {
                        await TrackPlayer.play();
                        isPlaying = true
                    } else {
                        await TrackPlayer.pause();
                    }
                }

            }
        } else {
            if (playbackState === TrackPlayer.STATE_PAUSED) {
                await TrackPlayer.play();
                isPlaying = true
            } else {
                await TrackPlayer.pause();
            }
        }
    }

    async function hideModal() {
        getComments()
        setPlayVideo(false)
    }

    async function createView() {
        await fetch("https://proviralzmobileapp.proviralz.com/api/createviews/" + id, {
            method: 'GET', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                "Authorization": `Bearer ${token}`
            },
            // body: JSON.stringify(usercomment) // body data type must match "Content-Type" header
        })
            .then(res => res.json())
            .then(rest__ => {
            })
            .catch((e) => {
            })
    }

    async function sendComment() {
        var usercomment = {
            "post_id": id,
            "comment_id": 2,//2 is for comments while 1 is for likes both are activities
            "comment": comment
        }
        if (comment !== "") {

            await fetch("https://proviralzmobileapp.proviralz.com/api/createcomments", {
                method: 'POST', // *GET, POST, PUT, DELETE, etc.
                mode: 'cors', // no-cors, *cors, same-origin
                cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                credentials: 'same-origin', // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    "Authorization": `Bearer ${token}`
                },
                body: JSON.stringify(usercomment) // body data type must match "Content-Type" header
            })
                .then(res => res.json())
                .then(rest__ => {
                    setComments("")
                    getComments()
                    setDisableComment(false)
                })
                .catch((e) => {
                })
        } else {
            setDisableComment(false)
        }

    }

    return (
        <View
            style={styles.mainContainer}
        >
            <VideoPlay
                showModal={play_Video}
                cover={music.src}
                video={music.video}
                onToggleHideModal={hideModal}
            />
            <Header navigation={props.navigation} page={music.title + " by " + music.artist} />

            <ScrollView
                horinzontal={false}
                style={{ flex: 1, flowDirection: "column", alignContent: "center" }}
                onScroll={() => getComments()}
                scrollEventThrottle={8}
            >
                <View
                    style={{ flex: 1 }}
                >
                    <Text style={{ fontSize: 13, color: "#fff", alignSelf: "center", marginTop: 20 }}>{music.genre}</Text>
                    <Image
                        source={{ uri: music.src }}
                        resizeMode="cover"
                        style={styles.musicimage}
                    ></Image>
                    <Text style={styles.artistname}>{music.title}</Text>
                    <View style={{ flexDirection: "row", alignSelf: "center" }}>
                        <Text style={styles.songtitle}>by {music.artist}</Text>
                        <TouchableOpacity
                            onPress={() => Share.share({
                                message: `hi fella, View this song on Proviralz mobile app, like and share it as well; ${music.title} By ${music.artist} - http://www.proviralzapp.com/share/${shareText()}`,
                                title: `${music.title} By ${music.artist}`
                            })}
                        >
                            <Image
                                source={require("../assets/images/share.png")}
                                resizeMode="contain"
                                style={styles.shareimage}
                            ></Image>
                        </TouchableOpacity>
                    </View>
                    <AudioPlayer
                        postid={id}
                        token={token}
                        onNext={skipToNext}
                        style={styles.player}
                        onPrevious={skipToPrevious}
                        onTogglePlayback={togglePlayback}
                    />
                    <View style={styles.comment}>
                        <Text style={{
                            fontSize: 15,
                            fontWeight: "bold",
                            color: "#fff",
                            fontStyle: "italic",
                            marginLeft: 20
                        }}>Comments</Text>
                        <View style={{
                            flexDirection: "row",
                            marginBottom: 10
                        }}>
                            <TextInput
                                placeholder="Write something..."
                                placeholderTextColor="rgba(0,0,0,0.5)"
                                textBreakStrategy="highQuality"
                                clearButtonMode="always"
                                style={styles.commentTextBox}
                                value={comment}
                                onChangeText={text => {
                                    setComments(text)
                                }}
                            ></TextInput>
                            <TouchableOpacity
                                onPress={() => {
                                    sendComment()
                                }}
                                disabled={disableCommentSend}
                            >
                                <Image
                                    source={require("../assets/images/send.png")}
                                    resizeMode="contain"
                                    style={styles.statimage}
                                ></Image>
                            </TouchableOpacity>
                        </View>
                        <BannerAd
                            unitId={adUnitId}
                            size={BannerAdSize.FULL_BANNER}
                            requestOptions={{
                                requestNonPersonalizedAdsOnly: true,
                            }}
                            key={1}
                        />
                        {showComment()}
                    </View>
                </View>
            </ScrollView>
            {/* <ToolBar navigation={props.navigation} page={"MusicPlayer"} active={1} /> */}
        </View>
    );
}

function getStateName(state) {
    switch (state) {
        case TrackPlayer.STATE_NONE:
            return "None";
        case TrackPlayer.STATE_PLAYING:
            return "Playing";
        case TrackPlayer.STATE_PAUSED:
            return "Paused";
        case TrackPlayer.STATE_STOPPED:
            return "Stopped";
        case TrackPlayer.STATE_BUFFERING:
            return "Buffering";
    }
}

async function skipToNext() {
    try {
        await TrackPlayer.skipToNext();
    } catch (_) { }
}

async function skipToPrevious() {
    try {
        await TrackPlayer.skipToPrevious();
    } catch (_) { }
}

const random = (mn, mx) => {
    return Math.random() * (mx - mn) + mn;
}

const bgColor = () => {

    const arr = ["#34495e",
        "#2c3e50",
        "#c0392b",
        "#16a085",
        "#f39c12",
        "#e74c3c",
        "#e67e22",
        "#7f8c8d",
        "#1abc9c",
        "#8e44ad"]
    const bgColor_ = arr[Math.floor(random(0, arr.length))];

    return bgColor_
}

const styles = {
    mainContainer: {
        flex: 1,
        backgroundColor: bgColor()
    },
    commentTextBox: {
        flex: 1,
        color: "#000",
        height: 50,
        borderRadius: 100,
        justifyContent: "center",
        fontSize: 13,
        borderColor: "#ddd",
        borderWidth: 1,
        backgroundColor: "#fff",
        lineHeight: 20,
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
        alignSelf: "center",
        padding: 10
    },
    comment: {
        marginTop: 20,
        paddingTop: 20,
        borderWidth: 1,
        borderBottomColor: "transparent",
        borderLeftColor: "transparent",
        borderRightColor: "transparent",
        borderTopColor: "#ddd",
    },
    musicimage: {
        width: "65%",
        height: 180,
        alignSelf: "center",
        marginBottom: 20,
        marginTop: 30,
        borderRadius: 5,
        borderWidth: 5,
        borderColor: "rgba(0,0,0,0.6)"
    },
    playButton: {
        // flex: 1,
        width: 50,
        height: 50,
        marginTop: 10,
        marginBottom: 10,
        alignSelf: "center",
        tintColor: "#fff"
    },
    statimage: {
        width: 22,
        height: 22,
        marginTop: 25,
        marginLeft: 5,
        marginRight: 25,
        marginBottom: 10,
        alignSelf: "center",
        justifyContent: "center",
        tintColor: "#fff",
    },
    shareimage: {
        width: 20,
        height: 20,
        marginTop: 0,
        marginLeft: 10,
        marginBottom: 0,
        alignSelf: "center",
        justifyContent: "center",
        tintColor: "#fff",
    },
    otherButton: {
        // flex: 1,
        width: 22,
        height: 22,
        marginTop: 10,
        marginBottom: 10,
        alignSelf: "center",
        tintColor: "#fff",
        // shadowColor: "#000",
        // shadowOpacity: "100%",
        // shadowRadius: 5
    },
    control1: {
        // height: "15%",
        flexDirection: "row",
        height: 50,
        marginTop: 20,
        marginBottom: 20,
        width: "70%",
        alignSelf: "center"
    },
    songtitle: {
        alignSelf: "center",
        marginTop: 3,
        marginBottom: 10,
        fontSize: 13,
        fontStyle: "italic",
        // fontWeight: "bold",
        color: "#fff"
    },
    artistname: {
        alignSelf: "center",
        marginTop: 5,
        marginBottom: 3,
        fontSize: 13,
        fontWeight: "bold",
        color: "#fff"
    },
    text: {
        color: "#fff",
        fontSize: 12,
        // lineHeight: 20,
        alignSelf: "center"
    },
    headtext: {
        color: "#666",
        fontSize: 15,
        // lineHeight: 20,
        // alignSelf: "flex-start",
        marginLeft: 20,
        marginTop: 20,
        marginBottom: 15,
        fontWeight: "bold",
        fontStyle: "italic"
    },
    searchBox: {
        width: "100%",
        // height: 100,
        borderWidth: 1,
        flowDirection: "row",
        borderTopColor: "transparent",
        borderLeftColor: "transparent",
        borderRightColor: "transparent",
        borderBottomColor: "#ddd",
    },
    rect3: {
        height: 35,
        // flex: 1,
        // width: 
        padding: 10,
        backgroundColor: "rgba(241, 98, 35, 1)",
        borderRadius: 100,
        // justifyContent: "center",
        marginTop: 20,
        marginBottom: 20,
        marginLeft: 10,
        marginRight: 20,
        // alignSelf: "center",
    },
    scrollArea_contentContainerStyle: {
        // height: "100%"
    },
    textInput: {
        width: "90%",
        color: "#666",
        height: 45,
        borderRadius: 100,
        justifyContent: "center",
        fontSize: 13,
        borderColor: "#ddd",
        borderWidth: 1,
        backgroundColor: "#fff",
        lineHeight: 20,
        marginTop: 25,
        marginLeft: 0,
        marginRight: 0,
        alignSelf: "center",
        padding: 15
    },
}