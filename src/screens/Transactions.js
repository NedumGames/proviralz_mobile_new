import React, { Component } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  Image,
  ImageBackground,
  TouchableOpacity,
  TextInput,
  Text,
  ScrollView,
  TouchableOpacityBase
} from "react-native";
import { Center } from "@builderx/utils";
import Divider from "../components/Divider";
import ToolBar from "../components/ToolBar";
import Header from "../components/Header";
import Transaction from "../components/Transaction";

function Transactions(props) {
  return (
    <View style={styles.rect}>
      <StatusBar hidden />   
      <View style={styles.scrollArea}>
        <Header navigation={props.navigation} page="Transactions" />
        <ScrollView
          horizontal={false}
          contentContainerStyle={styles.scrollArea_contentContainerStyle}
        >
          
          <View style={{flexDirection: "row"}}>
          <Image
              source={require("../assets/images/001-money.png")}
              resizeMode="cover"
              style={styles.statimage}
            ></Image>
            <Text style={styles.textstat}>Transactions</Text>
          </View>
          <Transaction {...props} />            
        </ScrollView>
        <ToolBar navigation={props.navigation} active={4} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  rect: {
    flex: 1
  },
  editor: {
    flexDirection: "row",
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5
  },
  editText: {
    width: "50%",
    height: 40,
    color: "rgba(0,0,0,0.7)",
    borderRadius: 5,
    fontSize: 14,
    borderColor: "#ddd",
    borderWidth: 1,
    backgroundColor: "#e4e7e8",
    lineHeight: 20,
    marginTop: 10,
    marginLeft: 20,
    marginRight: 20,
    padding: 5
  },
  actions: {
    flexDirection: "row",
    // flex: 1,
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10
  },
  textstat: {
    marginTop: 10,
    marginLeft: 10,
    color: "#666",
    fontSize: 15,
  },
  actionText: {
    marginTop: 0,
    marginLeft: 10,
    color: "#fff",
    fontSize: 14,
    justifyContent: "center"
  },
  rect2: {
    backgroundColor: "#000",
    // flex: 1,
    height: 300
  },
  image: {
    height: "100%",
    // flexDirection: "row"
  },
  button: {
    width: 40,
    height: 40,
    backgroundColor: "rgba(28,42,56,0.8)",
    opacity: 0.76,
    borderRadius: 100,
    justifyContent: "center"
  },
  icon2: {
    color: "rgba(255,255,255,1)",
    fontSize: 24,
    alignSelf: "center"
  },
  icon: {
    color: "rgba(255,255,255,1)",
    fontSize: 25,
    height: 27,
    width: 25,
    marginLeft: 264,
    marginTop: 5
  },
  buttonRow: {
    height: 40,
    flexDirection: "row",
    flex: 1,
    marginRight: 24,
    marginLeft: 22,
    marginTop: 25
  },
  image2: {
    width: 75,
    height: 75,
    borderRadius: 100,
    // borderColor: "#000",
    borderWidth: 5,
    alignSelf: "center",
    marginTop: 30,
    justifyContent: "center"
  },
  statimage: {
    width: 20,
    marginTop: 10,
    marginLeft: 20,
    tintColor: "#666",
    height: 20
  },
  image3: {
    width: 20,
    // height: 35,
    marginLeft: 20,
    tintColor: "#fff",
    height: 20
  },
  text10: {
    color: "rgba(255,255,255,0.8)",
    fontSize: 18,
    lineHeight: 20,
    marginTop: 20,
    alignSelf: "center",
    justifyContent: "center"
  },
  TransactionsText2: {
    color: "rgba(255,255,255,0.5)",
    fontSize: 13,
    lineHeight: 20,
    marginTop: 10,    
    alignSelf: "center",
    marginBottom: 20
    // justifyContent: "center"
  },
  childText: {
    color: "rgba(255,255,255,0.7)",
    fontSize: 15,
    lineHeight: 20,
    marginTop: 10,    
    alignSelf: "center",
    justifyContent: "center"
  },
  rect5: {
    width: 134,
    height: 20,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  icon4: {
    color: "#798894",
    fontSize: 15
  },
  TransactionsText: {
    color: "#798894",
    fontSize: 13,
    lineHeight: 20
  },
  rect4: {
    width: 163,
    height: 20,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  icon3: {
    color: "#798894",
    fontSize: 15
  },
  text2: {
    color: "#798894",
    fontSize: 16,
    lineHeight: 20
  },
  rect5Row: {
    // flexDirection: "row",
    alignItems: "center",
    marginTop: 10,
    marginBottom: 5
    // marginLeft: 22,
    // marginRight: 49
  },
  rect7: {
    width: 92,
    height: 21,
    // flexDirection: "row",
    justifyContent: "space-around",
    alignSelf: "center",
    marginBottom: 4
  },
  text6: {
    color: "rgba(255,255,255,1)",
    fontSize: 14,
    lineHeight: 20
  },
  visitors: {
    color: "#798894",
    fontSize: 14,
    lineHeight: 20
  },
  rect6: {
    width: 92,
    height: 21,
    flexDirection: "row",
    justifyContent: "space-between",
    marginLeft: 27,
    marginTop: 6
  },
  text4: {
    color: "rgba(255,255,255,1)",
    fontSize: 14,
    lineHeight: 20
  },
  likes: {
    color: "#798894",
    fontSize: 14,
    lineHeight: 20,
    alignSelf: "center",
    textAlign: "left"
  },
  rect3: {
    height: 35,
    flex: 1,
    width: "100%",
    backgroundColor: "rgba(241, 98, 35, 1)",
    borderRadius: 100,
    justifyContent: "center",
    marginTop: 5,
    marginLeft: 10,
    marginRight: 20,
    alignSelf: "center",
  },
  actionButton: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: "rgba(241, 98, 35, 1)",
    borderRadius: 100,
    marginTop: 5,
    marginLeft: 5,
    marginRight: 20,
    height: 35,
    alignItems: "center"
  },
  text: {
    color: "#fff",
    fontSize: 14,
    lineHeight: 20,
    alignSelf: "center"
  },
  rect7Row: {
    height: 30,
    flexDirection: "row",
    marginTop: 12,
    marginLeft: 20,
    marginRight: 20
  },
  scrollArea: {
    backgroundColor: "rgba(255,255,255,1)",
    width: "100%",
    height: "10%",
    flex: 1
  },
  scrollArea_contentContainerStyle: {
    // height: 1002,
    flex:1,
    width: 375
  },
  rect8: {
    height: 51,
    flexDirection: "row",
    justifyContent: "space-around"
  },
  divider: {
    top: 50,
    left: 0,
    height: 1,
    position: "absolute",
    right: 0
  },
  rect9: {
    flex: 0.21,
    borderColor: "#1da1f2",
    borderWidth: 0,
    borderBottomWidth: 4
  },
  myPosts: {
    color: "#1da1f2",
    fontSize: 18,
    lineHeight: 20,
    marginTop: 15,
    alignSelf: "center"
  },
  icon6: {
    top: 0,
    left: 47,
    position: "absolute",
    color: "rgba(128,128,128,1)",
    fontSize: 40,
    height: 43,
    width: 40
  },
  icon7: {
    top: 0,
    left: 160,
    position: "absolute",
    color: "rgba(28,33,82,1)",
    fontSize: 40,
    height: 43,
    width: 40
  },
  rect11: {
    flex: 0.15
  },
  icon8: {
    top: 0,
    left: 262,
    position: "absolute",
    color: "rgba(28,33,82,1)",
    fontSize: 40,
    height: 43,
    width: 40
  },
  rect12: {
    flex: 0.16
  },
  tweet2: {
    width: 360,
    height: 149
  },
  divider3: {
    height: 1,
    marginTop: 178
  },
  tweet: {
    width: 360,
    height: 149,
    marginTop: 1
  }
});

export default Transactions;
