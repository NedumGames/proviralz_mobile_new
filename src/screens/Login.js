import React, { useState, Component, useEffect } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  Text,
  Alert,
  Linking,
  TextInput,
  ToastAndroid,
  TouchableOpacity,
  Image
} from "react-native";
import Divider from "../components/Divider";
import Loading from "../components/Loading";
import AsyncStorage from '@react-native-community/async-storage';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-community/google-signin';

GoogleSignin.configure({
  // scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
  webClientId: '824493360358-crqnfck96tmme0b41elqlc3594f4v5ul.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
  offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
  // hostedDomain: '', // specifies a hosted domain restriction
  // loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
  forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
  // accountName: 'project-824493360358', // [Android] specifies an account name on the device that should be used
  // iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
});


function Login(props) {

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);

  let details = {}

  useEffect(() => {
    AsyncStorage.getItem('userdetails', (err, _data) => {
      var _res = JSON.parse(_data)
      if (_res && _res.user) {
        props.navigation.navigate("Home")
        return;
      }
    })

    return;
  })


  const login = () => {
    let _data = {
      "email": username,
      "password": password
    }
    setLoading(true)

    fetch("https://proviralzmobileapp.proviralz.com/api/login", {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify(_data) // body data type must match "Content-Type" header
    })
      .then(res => res.json())
      .then((result) => {
        setLoading(false)
        if (!result.success) {
          Alert.alert(
            'Login not successful!',
            `Hi!, ${result.message}`,
            [
              {
                text: "forget password",
                onPress: () => props.navigation.push("ForgetPassword")
              },
              {
                text: 'Try again',
              }
            ],
            { cancelable: false }
          );
          return;
        } else {

          Alert.alert(
            'Login Successful!',
            'Hi! Your login details matched successfully, click continue to proceed.',
            [
              {
                text: 'continue',
                onPress: async () => {
                  await AsyncStorage.setItem('userdetails', JSON.stringify(result))
                  await AsyncStorage.setItem('userid', result.user.id.toString())
                  props.navigation.navigate("Home")
                }
              }
            ],
            { cancelable: false }
          );
        }
      })
      .catch((e) => {
        setLoading(false)
      })
  }

  const nextPage = async (result) => {
    await AsyncStorage.setItem('userdetails', JSON.stringify(result))
    await AsyncStorage.setItem('userid', result.user.id + "")
    props.navigation.navigate("Home")
  }

  const _googleSignin = async () => {
    if (!statusCodes.SIGN_IN_REQUIRED) {      
      await GoogleSignin.revokeAccess();
    }
    await GoogleSignin.signOut()

    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      setLoading(true)

      const data = {
        email: userInfo.user.email,
        name: userInfo.user.name,
        photo: userInfo.user.photo,
        password: userInfo.user.id,
        type: 1
      }

      fetch("https://proviralzmobileapp.proviralz.com/api/googlelogin", {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
        body: JSON.stringify(data) // body data type must match "Content-Type" header
      })
        .then(res => res.json())
        .then((result) => {
          setLoading(false)
          if (!result.success) {
            ToastAndroid.showWithGravity(
              "login not successful, try again.",
              ToastAndroid.SHORT,
              ToastAndroid.CENTER
            )
            return;
          } else {
            nextPage(result)
          }
        })
        .catch((e) => {
          setLoading(false)
        })

    } catch (error) {
      setLoading(false)
      await GoogleSignin.signOut()
      ToastAndroid.showWithGravity(
        "login Error, try again.",
        ToastAndroid.SHORT,
        ToastAndroid.CENTER
      )
    }
    // setLoading(false)
  }

  return (
    <View style={styles.rect}>
      <Loading
        load={loading}
      />
      <StatusBar hidden />
      <View style={styles.rect3}>
        <Image
          source={require("../assets/images/proviralz_logo.jpg")}
          resizeMode="contain"
          style={styles.logo}
        ></Image>
        <Text style={styles.proviralz}>Login</Text>
      </View>
      <Text style={styles.logInToProviralz}>Use you phone number or email address to login, having any dificulty, use the forgot password link below.</Text>

      <TextInput
        placeholder="Phone/Email "
        onChange={value => {
          setUsername(value.nativeEvent.text)
        }}
        clearButtonMode="always"
        placeholderTextColor="rgba(0,0,0,0.5)"
        style={styles.phoneInput}
      ></TextInput>
      <TextInput
        placeholder="Password"
        onChange={value => {
          setPassword(value.nativeEvent.text)
        }}
        clearButtonMode="always"
        placeholderTextColor="rgba(0,0,0,0.5)"
        secureTextEntry={true}
        style={styles.phoneInput}
      ></TextInput>

      <TouchableOpacity
        onPress={() => {
          Linking.openURL("https://mobile.proviralz.com/tos.php").catch((err) => console.error('An error occurred', err));
        }}
      >
        <View
          style={styles.agreement}
        >
          <Text style={styles.attribute}>By continuing, you accept our</Text>
          <Text style={styles.attribute2}> Terms of service & Privacy Policy.</Text>
        </View>
      </TouchableOpacity>


      <View style={styles.materialButtonViolet2Row}>
        <TouchableOpacity
          onPress={() => {
            setLoading(true)
            login()
          }}
          style={styles.registerbutton}>
          <Text style={styles.register}>Signin</Text>
        </TouchableOpacity>
        <Text style={styles.or}>Or</Text>
        <GoogleSigninButton
          style={styles.googlebutton}
          size={GoogleSigninButton.Size.Wide}
          color={GoogleSigninButton.Color.Light}
          onPress={() => _googleSignin()}
          disabled={loading}
        />
      </View>
      <TouchableOpacity
        style={{
          marginTop: 30
        }}
        onPress={() => props.navigation.push("ForgetPassword")}
      >
        <Text style={styles.text5}>Forgotten your password?</Text>
      </TouchableOpacity>


    </View>
  );
}

const styles = StyleSheet.create({
  rect: {
    flex: 1,
    backgroundColor: "rgba(255,255,255,1)"
  },
  attribute: {
    color: "#000",
    fontSize: 10,
  },
  attribute2: {
    color: "rgba(241, 98, 35, 1)",
    fontSize: 10,
    textDecorationLine: "underline",
    textDecorationColor: "rgba(241, 98, 35, 1)",
  },
  agreement: {
    textAlign: "center",
    fontSize: 11,
    marginBottom: 0,
    width: "75%",
    alignSelf: "center",
    marginTop: 50,
    flexDirection: "row"
  },
  logo: {
    width: 30,
    height: 30,
    marginLeft: 20,
    marginBottom: 0,
    alignSelf: "center"
  },
  register: {
    color: "rgba(86,2,4,1)",
    fontSize: 15,
    lineHeight: 20,
    marginLeft: 20,
    marginRight: 20,
    alignSelf: "center"
  },
  or: {
    color: "rgba(0,0,0,1)",
    fontSize: 18,
    lineHeight: 20,
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
  },
  rect3: {
    height: 50,
    flexDirection: "row",
    backgroundColor: "#000",
    marginTop: 0,
  },
  registerbutton: {
    height: 50,
    width: 100,
    backgroundColor: "rgba(241, 98, 35, 1)",
    borderRadius: 100,
    justifyContent: "center",
    marginTop: 0,
    alignSelf: "center",
  },
  googlebutton: {
    width: 150,
    height: 50,
    // backgroundColor: "rgba(239,17,25,0.7)",
    borderRadius: 5,
    justifyContent: "center",
  },
  phoneInput: {
    width: "90%",
    height: 40,
    color: "rgba(0,0,0,0.7)",
    borderRadius: 5,
    fontSize: 14,
    borderColor: "#ddd",
    borderWidth: 1,
    backgroundColor: "#e4e7e8",
    lineHeight: 20,
    marginTop: 10,
    marginLeft: 20,
    marginRight: 20,
    padding: 5
  },
  materialButtonViolet2Row: {
    flexDirection: "row",
    marginRight: 20,
    marginLeft: 20,
    marginTop: 20
  },
  proviralz: {
    color: "rgba(255,255,255,0.6)",
    fontSize: 16,
    alignSelf: "flex-start",
    marginTop: 15,
    textAlignVertical: "center",
    marginLeft: 20,
  },
  logInToProviralz: {
    color: "rgba(0,0,0,0.7)",
    fontSize: 12,
    lineHeight: 20,
    marginTop: 30,
    width: "75%",
    marginBottom: 100,
    textAlign: "center",
    alignSelf: "center"
  },
  termAndCondition: {
    flexDirection: "row",
    alignSelf: "center",
    color: "rgba(0,0,0,0.7)",
    fontSize: 11,
    lineHeight: 20,
    marginTop: 50,
    width: "75%",
    marginBottom: 100,
    textAlign: "center",
    alignSelf: "center"
  },
  loremIpsum: {
    top: 0,
    left: 108,
    position: "absolute",
    fontFamily: "roboto-regular",
    color: "#121212"
  },
  logInToProviralzStack: {
    width: 258,
    height: 50,
    marginTop: 147,
    marginLeft: 33
  },
  google: {
    color: "#ffffff",
    fontSize: 12,
    lineHeight: 20,
    marginLeft: 20,
    marginRight: 20,
    alignSelf: "center"
  },
  googlebutton: {
    width: 150,
    height: 50,
    // backgroundColor: "rgba(239,17,25,1)",
    borderRadius: 5,
    justifyContent: "center",
  },
  rect2: {
    height: 70,
    backgroundColor: "rgba(86,2,4,1)",
    marginTop: 0
  },
  proviralz1: {
    color: "rgba(255,255,255,0.6)",
    fontSize: 24,
    alignSelf: "flex-start",
    marginTop: 20,
    marginLeft: 20,
  },
  text5: {
    color: "rgba(0,0,0,1)",
    fontSize: 11,
    lineHeight: 20,
    marginTop: 5,
    alignSelf: "center"
  },
  textInput3: {
    width: 312,
    height: 42,
    color: "rgba(0,0,0,1)",
    borderColor: "rgba(0,0,0,1)",
    borderWidth: 0,
    borderBottomWidth: 2,
    fontSize: 15,
    lineHeight: 20,
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
  },
  textInput4: {
    width: 312,
    height: 42,
    color: "rgba(0,0,0,1)",
    borderColor: "rgba(0,0,0,1)",
    borderWidth: 0,
    borderBottomWidth: 2,
    fontSize: 15,
    lineHeight: 20,
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
  },
  or2: {
    color: "rgba(0,0,0,1)",
    fontSize: 18,
    lineHeight: 20,
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
  },
  materialButtonDanger: {
    height: 48,
    width: 150,
    marginTop: 0,
    marginLeft: 0
  },
  logInToProviralzStackColumn: {
    marginTop: -5,
    marginLeft: 1,
    marginRight: -1
  },
  logInToProviralzStackColumnFiller: {
    flex: 1
  },
  rect4: {
    height: 42,
    flexDirection: "row",
    marginTop: 20,
    marginLeft: 20,
    marginRight: 10
  },
  divider: {
    width: 360,
    height: 1
  },
  button2: {
    width: 100,
    height: 50,
    backgroundColor: "rgba(0,0,0,0)",
    borderColor: "rgba(86,2,4,1)",
    borderWidth: 2,
    borderRadius: 100,
    justifyContent: "center",
  },
  text6: {
    color: "rgba(86,2,4,1)",
    fontSize: 15,
    lineHeight: 20,
    marginLeft: 20,
    marginRight: 20,
    alignSelf: "center"
  }
});

export default Login;
