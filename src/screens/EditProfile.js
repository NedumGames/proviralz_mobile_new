import React, { useState, Component } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  TextInput,
  Text,
  Image,
  Alert,
  // AsyncStorage,
  ScrollView,
  TouchableOpacity
} from "react-native";
import Header from "../components/Header";
import ImagePicker from 'react-native-image-picker';
import DropDownPicker from 'react-native-dropdown-picker';
import ToolBar from "../components/ToolBar";
import Loading from "../components/Loading";
import AsyncStorage from '@react-native-community/async-storage';




class EditProfile extends Component {

  state = {
    "background_image": null,
    "bank_account_name": null,
    "bank_account_number": null,
    "bank_name": null,
    "dob": null,
    "earnings": "0",
    "email": "",
    "gender": "",
    "id": 0,
    "location": null,
    "name": "",
    "phone": "",
    "profession": null,
    "profile_image": "",
    "username": "",
    "verified": ""
  }

  cloud = 'proviral'
  preset = "f0ohodol";
  upload_url = `https://api.cloudinary.com/v1_1/${this.cloud}/image/upload`
  video_url = `https://api.cloudinary.com/v1_1/${this.cloud}/video/upload`


  options = {
    title: 'Select Avatar',
    customButtons: [{ name: 'fb', title: 'Choose Photo' }],
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };

  props = []

  constructor(prop) {
    super(prop)
    this.props = prop

    this.state = this.props.navigation.state.params.user
  }

  getLibrary = () => {
    // Open Image Library:
    this.setState({ loading: true })
    // Open Image Library:
    ImagePicker.launchImageLibrary(this.options, (response) => {
      if (response.didCancel) {
        this.setState({ loading: false })
        return;
      }
      this.setState({
        pic: `file://${response.path}`,
        tempimageFile: response
      })
      this.setState({ disabled: false })
      // if (response) {
      let xhr = new XMLHttpRequest();
      xhr.open('POST', this.upload_url);
      xhr.onreadystatechange = () => {
        try {
          this.setState({ loading: false })
          var json_ = {}
          json_ = JSON.parse(xhr.response)
          this.setState({
            imageFile: json_.secure_url,
          });

        } catch (e) {
          this.setState({ loading: false })
        }
      };

      let formdata = new FormData();
      formdata.append('file', "data:image/jpeg;base64," + response.data);
      formdata.append('upload_preset', this.preset);
      // formdata.append('cloud_name', cloud);
      xhr.send(formdata);
      // }
    });

  }

  getCamera = () => {
    this.setState({ loading: true })
    // Launch Camera:
    ImagePicker.launchCamera(this.options, (response) => {
      if (response.didCancel) {
        this.setState({ loading: false })
        return;
      }
      this.setState({
        pic: `file://${response.path}`,
        tempimageFile: response
      })
      this.setState({ disabled: false })
      // if (response) {
      let xhr = new XMLHttpRequest();
      xhr.open('POST', this.upload_url);
      xhr.onreadystatechange = () => {
        try {
          this.setState({ loading: false })
          var json_ = {}
          json_ = JSON.parse(xhr.response)
          this.setState({
            imageFile: json_.secure_url,
          });

        } catch (e) {
          this.setState({ loading: false })
        }
      };

      let formdata = new FormData();
      formdata.append('file', "data:image/jpeg;base64," + response.data);
      formdata.append('upload_preset', this.preset);
      // formdata.append('cloud_name', cloud);
      xhr.send(formdata);
      // }
    });
  }

  updateProfile = async () => {
    let data = {}

    if (this.state.name && this.state.name.length < 5) {
      this.errorMessage("")
      return;
    } else { data.name = this.state.name }
    if (this.state.profile_image && this.state.profile_image.length > 5) {
      data.profile_image = this.state.profile_image
    }
    if (this.state.username.match(' ')) {
      this.errorMessage("Please no space between username.")
      return;
    }
    if (this.state.username && this.state.username.length < 5) {
      this.errorMessage("")
      return;
    } else { data.username = this.state.username }
    if (this.state.gender === "other") {
      this.errorMessage("Please Update your gender.")
      return;
    }
    if (this.state.gender && this.state.gender.length < 4) {
      this.errorMessage("")
      return;
    } else { data.gender = this.state.gender }
    if (this.state.phone && this.state.phone.length < 5) {
      this.errorMessage("")
      return;
    } else { data.phone = this.state.phone }
    if (this.state.email && this.state.email.length < 5) {
      this.errorMessage("")
      return;
    } else { data.email = this.state.email }
    if (this.state.bank_name && this.state.bank_name.length > 5) {
      data.bank_name = this.state.bank_name
    }
    if (this.state.bank_account_name && this.state.bank_account_name.length > 5) {
      data.bank_account_name = this.state.bank_account_name
    }
    if (this.state.bank_account_number && this.state.bank_account_number.length > 5) {
      data.bank_account_number = this.state.bank_account_number
    }
    if (this.state.oldpassword > 5 && this.state.newpasssword.length > 5 && this.state.confirmPassword > 5) {
      if (this.state.newpassword.equals(this.state.confirmPassword)) {
        data.password = this.state.newpassword
      }
    }

    this.setState({ loading: true, disabled: true })

    fetch("https://proviralzmobileapp.proviralz.com/api/updateuser", {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        "authorization": `Bearer ${this.props.navigation.state.params.user.api_token}`
      },
      body: JSON.stringify(data) // body data type must match "Content-Type" header
    })
      .then(res => res.json())
      .then(async result => {
        this.setState({ loading: false, disabled: false })
        if (!result.success) {
          Alert.alert(
            'Update Error.',
            `Hi!, ${result.message}`,
            [
              {
                text: 'Ok',
              }
            ],
            { cancelable: false }
          );
          return;
        } else {
          Alert.alert(
            'Update Successful!',
            `Hi!, Your user details have been updated successfully.`,
            [
              {
                text: 'Ok',
                onPress: () => {
                  this.props.navigation.push("Profile")
                }
              }
            ],
            { cancelable: false }

          );

        }
      })
      .catch((e) => {
        this.setState({ loading: false, disabled: false })
        Alert.alert(
          'Update Error!',
          `Hi!, Update was not successful, try again.`,
          [
            {
              text: 'Ok',
            }
          ],
          { cancelable: false }

        );
        return
      })
  }

  errorMessage = (message) => {
    if (message.length > 1 && message != "error") {
      Alert.alert(
        'Update Error',
        `Hi! ${message}.`,
        [
          {
            text: 'Okay',
          }
        ],
        { cancelable: false }
      );
    } else {
      Alert.alert(
        'Update Error',
        `Hi! No input box should be left empty.`,
        [
          {
            text: 'Okay',
          }
        ],
        { cancelable: false }
      );
    }
  }

  render() {
    return (
      <View style={styles.rect}>
        <StatusBar hidden />
        <Header navigation={this.props.navigation} page="Edit Profile" />
        <Loading load={this.state.loading} />
        <ScrollView
          horizontal={false}
          style={{ paddingBottom: 50, marginBottom: 30 }}
        >
          <View style={styles.textInputColumn}>

            <Text style={styles.text4}>Update your personal details here.</Text>
            {/* <Text style={styles.label}>Full name</Text>     */}
            {this.state.pic && this.state.pic.length > 0 ?
              <Image
                source={{ uri: this.state.pic }}
                resizeMode="cover"
                style={{
                  width: 100,
                  alignSelf: "center",
                  height: 100
                }}
              ></Image>
              :
              <Image
                source={{ uri: this.state.profile_image }}
                resizeMode="contain"
                style={styles.image4}
              ></Image>
            }

            <Text style={{
              color: "#000",
              fontSize: 12,
              alignSelf: "center",
              marginTop: 5,
              textAlign: "center",
            }}>{this.state.tempimageFile && this.state.tempimageFile.fileName ? this.state.tempimageFile.fileName : "Edit profile image"}</Text>

            <View style={{
              flexDirection: "row",
              marginBottom: 20,
              marginTop: 20,
              marginLeft: 20,
              marginRight: 20
            }}>
              <TouchableOpacity
                disabled={this.state.loading}
                onPress={() =>
                  this.getCamera()
                }
                style={styles.camerabutton}>
                <Text style={styles.google}>Use Camera</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.setState({ disabled: true })
                  this.getLibrary()
                }
                }
                disabled={this.state.loading}
                style={styles.camerabutton}>
                <Text style={styles.google}>From Gallery</Text>
              </TouchableOpacity>
            </View>
            <TextInput
              placeholder="Full Name"
              placeholderTextColor="rgba(0,0,0,0.5)"
              textBreakStrategy="highQuality"
              clearButtonMode="always"
              style={styles.textInput}
              onChange={value => {
                this.setState({
                  name: value.nativeEvent.text
                })
              }}
            >{this.state.name ? this.state.name : ""}</TextInput>
            <View style={styles.materialButtonViolet1Row}>
              <TextInput
                placeholder="Username"
                clearButtonMode="always"
                placeholderTextColor="rgba(0,0,0,0.5)"
                style={styles.emailInput}
                onChange={value => {
                  this.setState({
                    username: value.nativeEvent.text
                  })
                }}
              >{this.state.username ? this.state.username : ""}</TextInput>
              <DropDownPicker
                items={[
                  { label: 'Male', value: 'male' },
                  { label: 'Female', value: 'female' },
                ]}
                placeholder={this.state.gender ? this.state.gender : "Gender"}
                defaultIndex={0}
                style={{
                  color: "rgba(0,0,0,0.7)",
                  borderRadius: 5,
                  fontSize: 14,
                  borderColor: "#ddd",
                  borderWidth: 1,
                  backgroundColor: "#e4e7e8",
                  lineHeight: 20,
                  marginTop: 10,
                  marginLeft: 5,
                  marginRight: 0,
                  flex: 1,
                  padding: 50
                }}
                containerStyle={{
                  width: "50%",
                  height: 50
                }}
                onChangeItem={item => {
                  this.setState({
                    gender: item.value
                  })
                }}
              />
            </View>
            <View style={styles.materialButtonViolet1Row}>
              <TextInput
                placeholder="Email Address"
                clearButtonMode="always"
                placeholderTextColor="rgba(0,0,0,0.5)"
                style={styles.emailInput}
                onChange={value => {
                  this.setState({
                    email: value.nativeEvent.text
                  })
                }}
              >{this.state.email ? this.state.email : ""}</TextInput>
              <TextInput
                placeholder="Phone Number"
                clearButtonMode="always"
                placeholderTextColor="rgba(0,0,0,0.5)"
                style={styles.phoneInput}
                onChange={value => {
                  this.setState({
                    phone: value.nativeEvent.text
                  })
                }}
              >{this.state.phone ? this.state.phone : ""}</TextInput>
            </View>
            {/* <Text style={{
              color: "#000",
              fontSize: 12,
              alignSelf: "center",
              textAlign: "center",
              marginTop: 10
            }}>Update your bank account details</Text>
            <View style={styles.materialButtonViolet1Row}>
              <TextInput
                placeholder="Bank Name"
                clearButtonMode="always"
                placeholderTextColor="rgba(0,0,0,0.5)"
                style={styles.emailInput}
                onChange={value => {
                  this.setState({ bank_name: value.nativeEvent.text })
                }}
              >{this.state.bank_name ? this.state.bank_name : ""}</TextInput>
              <TextInput
                placeholder="Bank account name"
                clearButtonMode="always"
                placeholderTextColor="rgba(0,0,0,0.5)"
                style={styles.phoneInput}
                onChange={value => {
                  this.setState({ bank_account_name: value.nativeEvent.text })
                }}
              >{this.state.bank_account_name ? this.state.bank_account_name : ""}</TextInput>
            </View>
            <View style={styles.materialButtonViolet1Row}>
              <TextInput
                placeholder="Bank account number"
                clearButtonMode="always"
                placeholderTextColor="rgba(0,0,0,0.5)"
                style={styles.emailInput}
                onChange={value => {
                  this.setState({ bank_account_number: value.nativeEvent.text })
                }}
              >{this.state.bank_account_number ? this.state.bank_account_number : ""}</TextInput>
            </View> */}
            <Text style={{
              color: "#000",
              fontSize: 12,
              alignSelf: "center",
              textAlign: "center",
              marginTop: 10
            }}>Update your password</Text>
            <View style={styles.materialButtonViolet1Row}>
              <TextInput
                placeholder="old Password"
                clearButtonMode="always"
                placeholderTextColor="rgba(0,0,0,0.5)"
                secureTextEntry={true}
                style={styles.passwordInput}
                onChange={value => {
                  this.setState({ oldpassword: value.nativeEvent.text })
                }}
              ></TextInput>
            </View>
            <View style={styles.materialButtonViolet1Row}>
              <TextInput
                placeholder="New Password"
                clearButtonMode="always"
                placeholderTextColor="rgba(0,0,0,0.5)"
                secureTextEntry={true}
                style={styles.passwordInput2}
                onChange={value => {
                  this.setState({ newpasssword: value.nativeEvent.text })
                }}
              ></TextInput>

              <TextInput
                placeholder="Confirm new Password"
                clearButtonMode="always"
                placeholderTextColor="rgba(0,0,0,0.5)"
                secureTextEntry={true}
                style={styles.passwordInput2}
                onChange={value => {
                  this.setState({ confirmPassword: value.nativeEvent.text })
                }}
              ></TextInput>
            </View>

            {/* <Text style={styles.agreement}>By continuing, you accept our Terms or service & Privacy Policy.</Text> */}
            <View style={styles.materialButtonViolet2Row}>
              <TouchableOpacity
                onPress={() => {
                  this.updateProfile()
                }}
                disabled={this.state.disabled}
                style={styles.registerbutton}>
                <Text style={styles.register}>Update Profile</Text>
              </TouchableOpacity>
              {/* <Text style={styles.or}>Or</Text>
              <TouchableOpacity
                onPress={() => props.navigation.navigate("Profile")}
                style={styles.googlebutton}>
                <Text style={styles.google}>SignUp with Google</Text>
              </TouchableOpacity> */}
            </View>
          </View>
        </ScrollView>
        <ToolBar navigation={this.props.navigation} active={4} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  rect: {
    flex: 1,
    backgroundColor: "#fff"
  },
  image3: {
    width: 15,
    // height: 35,
    marginLeft: 10,
    tintColor: "#fff",
    height: 15
  },
  image4: {
    width: 150,
    alignSelf: "center",
    // tintColor: "#666",
    height: 150,
  },
  logo: {
    width: 30,
    height: 30,
    marginLeft: 20,
    marginBottom: 0,
    alignSelf: "center"
  },
  agreement: {
    textAlign: "center",
    fontSize: 11,
    marginBottom: 10,
    width: "75%",
    alignSelf: "center",
    marginTop: 70
  },
  textInput: {
    width: "90%",
    height: 40,
    color: "rgba(0,0,0,0.7)",
    borderRadius: 5,
    fontSize: 14,
    borderColor: "#ddd",
    borderWidth: 1,
    backgroundColor: "#e4e7e8",
    lineHeight: 20,
    marginTop: 10,
    marginLeft: 20,
    marginRight: 20,
    padding: 5
  },
  label: {
    fontSize: 12,
    marginLeft: 20,
    color: "rgba(241, 98, 35, 1)"
  },
  emailInput: {
    width: "49.5%",
    height: 40,
    color: "rgba(0,0,0,0.7)",
    borderRadius: 5,
    fontSize: 14,
    borderColor: "#ddd",
    borderWidth: 1,
    backgroundColor: "#e4e7e8",
    lineHeight: 20,
    marginTop: 10,
    marginLeft: 0,
    marginRight: 5,
    padding: 5
  },
  phoneInput: {
    width: "49.5%",
    height: 40,
    color: "rgba(0,0,0,0.7)",
    borderRadius: 5,
    fontSize: 14,
    borderColor: "#ddd",
    borderWidth: 1,
    backgroundColor: "#e4e7e8",
    lineHeight: 20,
    marginTop: 10,
    marginLeft: 5,
    marginRight: 0,
    padding: 5
  },
  passwordInput: {
    width: "49.5%",
    height: 40,
    color: "rgba(0,0,0,0.7)",
    borderRadius: 5,
    fontSize: 14,
    borderColor: "#ddd",
    borderWidth: 1,
    backgroundColor: "#e4e7e8",
    lineHeight: 20,
    marginTop: 10,
    marginLeft: 0,
    marginRight: 5,
    padding: 5,
  },
  passwordInput2: {
    width: "49.5%",
    height: 40,
    flex: 1,
    color: "rgba(0,0,0,0.7)",
    borderRadius: 5,
    fontSize: 14,
    borderColor: "#ddd",
    borderWidth: 1,
    backgroundColor: "#e4e7e8",
    lineHeight: 20,
    marginTop: 10,
    marginLeft: 5,
    marginRight: 0,
    padding: 5
  },
  text4: {
    color: "rgba(0,0,0,0.7)",
    fontSize: 12,
    lineHeight: 20,
    marginTop: 30,
    width: "75%",
    marginBottom: 30,
    textAlign: "center",
    alignSelf: "center"
  },
  rect3: {
    height: 50,
    flexDirection: "row",
    backgroundColor: "#000",
    marginTop: 0
  },
  proviralz: {
    color: "rgba(255,255,255,0.6)",
    fontSize: 16,
    alignSelf: "flex-start",
    marginTop: 15,
    textAlignVertical: "center",
    marginLeft: 20,
  },
  textInput3: {
    width: 312,
    height: 42,
    color: "rgba(0,0,0,1)",
    borderColor: "rgba(0,0,0,1)",
    borderWidth: 0,
    borderBottomWidth: 2,
    fontSize: 15,
    lineHeight: 20,
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
  },
  textInput4: {
    width: 312,
    height: 42,
    color: "rgba(0,0,0,1)",
    borderColor: "rgba(0,0,0,1)",
    borderWidth: 0,
    borderBottomWidth: 2,
    fontSize: 15,
    lineHeight: 20,
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20
  },
  textInput5: {
    width: 312,
    height: 42,
    color: "rgba(0,0,0,1)",
    borderColor: "rgba(0,0,0,1)",
    borderWidth: 0,
    borderBottomWidth: 2,
    fontSize: 15,
    lineHeight: 20,
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20
  },
  textInput6: {
    width: 312,
    height: 42,
    color: "rgba(0,0,0,1)",
    borderColor: "rgba(0,0,0,1)",
    borderWidth: 0,
    borderBottomWidth: 2,
    fontSize: 15,
    lineHeight: 20,
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20
  },
  materialButtonViolet1: {
    height: 36,
    width: 168,
    marginTop: 5
  },
  textInput7: {
    width: 129,
    height: 42,
    color: "rgba(0,0,0,1)",
    borderColor: "rgba(0,0,0,1)",
    borderWidth: 0,
    borderBottomWidth: 2,
    fontSize: 15,
    lineHeight: 20,
    marginLeft: 14,
    marginTop: 0
  },
  materialButtonViolet1Row: {
    flexDirection: "row",
    marginRight: 20,
    marginLeft: 20,
  },
  materialButtonViolet2Row: {
    flexDirection: "row",
    marginRight: 20,
    marginLeft: 20,
    marginTop: 50
  },
  or: {
    color: "rgba(0,0,0,1)",
    fontSize: 18,
    lineHeight: 20,
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
  },
  materialButtonDanger1: {
    height: 48,
    width: 182,
    marginTop: 17,
    marginLeft: 98
  },
  textInputColumn: {
    marginLeft: -2
  },
  textInputColumnFiller: {
    flex: 1
  },
  rect2: {
    height: 71,
    shadowOpacity: 1,
    marginBottom: 29,
    marginTop: 20
  },
  divider: {
    width: 360,
    height: 1
  },
  registerbutton: {
    height: 50,
    backgroundColor: "rgba(241, 98, 35, 1)",
    borderRadius: 100,
    justifyContent: "center",
    marginBottom: 50,
    alignSelf: "center",
  },
  camerabutton: {
    height: 50,
    flex: 1,
    backgroundColor: "rgba(241, 98, 35, 1)",
    borderRadius: 100,
    justifyContent: "center",
    margin: 10,
    alignSelf: "center",
  },
  googlebutton: {
    width: 150,
    height: 50,
    backgroundColor: "rgba(239,17,25,1)",
    borderRadius: 5,
    justifyContent: "center",
  },
  register: {
    color: "rgba(86,2,4,1)",
    fontSize: 15,
    lineHeight: 20,
    marginLeft: 20,
    marginRight: 20,
    alignSelf: "center"
  },
  google: {
    color: "#ffffff",
    fontSize: 11,
    lineHeight: 20,
    marginLeft: 20,
    marginRight: 20,
    alignSelf: "center"
  }
});

export default EditProfile;