import React, { Component, useEffect, useState } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  Image,
  ImageBackground,
  TouchableOpacity,
  TextInput,
  ToastAndroid,
  ActivityIndicator,
  Alert,
  Text,
  ScrollView,
  TouchableOpacityBase
} from "react-native";
import DropDownPicker from 'react-native-dropdown-picker';
import { Center } from "@builderx/utils";
import Divider from "../components/Divider";
import ToolBar from "../components/ToolBar";
import Header from "../components/Header";
import Uploads from "../components/Uploads";
import DownloadList from "../components/DownloadList";
import Loading from "../components/Loading";
import AsyncStorage from '@react-native-community/async-storage';


function Downloads(props) {

  const [loading, setLoading] = useState(false);
  const [networkerror, setErrorNetwork] = useState(0);
  const [music, setMusic] = useState([]);

  const pullDownloads = async () => {
    await AsyncStorage.getItem("userdetails", async (err, result) => {
      var _res = JSON.parse(result)
      setLoading(true)
      await fetch("https://proviralzmobileapp.proviralz.com/api/viewdownloads/" + _res.user.id, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          "authorization": `Bearer ${_res.token}`
        },
        // body: JSON.stringify(_data) // body data type must match "Content-Type" header
      })
        .then(res => res.json())
        .then((result) => {
          console.log(result, "result")
          setLoading(false)
          if (!result.success) {
            ToastAndroid.showWithGravity(
              `Hi!, ${result.message}`,
              ToastAndroid.LONG,
              ToastAndroid.CENTER
            )
            return;
          } else {
            setMusic(result.downloads)
          }
        })
        .catch((e) => {
          setLoading(false)
          ToastAndroid.show(
            "Hi!, there is an issue retrieving your downloads.",
            ToastAndroid.LONG
          )
        })
    })
  }

  const deletedownloads = async (id) => {
    const userDetails = await AsyncStorage.getItem("userdetails", (err, result) => {
      var _res = JSON.parse(result)
      setLoading(true)
      fetch("https://proviralzmobileapp.proviralz.com/api/deletedownloads/" + _res.user.id + "/" + id, {
        method: 'DELETE', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          "authorization": `Bearer ${_res.token}`
        },
        // body: JSON.stringify(_data) // body data type must match "Content-Type" header
      })
        .then(res => res.json())
        .then((result) => {
          setLoading(false)
          if (!result.success) {
            ToastAndroid.showWithGravity(
              `Hi!, ${result.message}`,
              ToastAndroid.LONG,
              ToastAndroid.CENTER
            )
            return;
          }
          pullDownloads()
        })
        .catch((e) => {
          setLoading(false)
          ToastAndroid.show(
            "Hi!, there is an issue deleting this download, try again.",
            ToastAndroid.LONG
          )
        })
    })
  }

  const showHome = (props) => {
    console.log()
    if (music.length < 1)
      return
    const result = []
    music != null && music.forEach(music_ => {
      console.log(music_)
      result.push(
        <DownloadList
          deletedownloads={deletedownloads}
          navigation={props.navigation}
          musicImage={music_ && music_.cover}
          title={music_ && music_.title}
          artist={music_ && music_.artist}
          audio={music_ && music_.audio}
          video={music_ && music_.video}
          genre={music_ && music_.genre}
          id={music_ && music_.id}

        />
      )
    });

    if (music.length < 1) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: "center"
          }}
        >
          <Text
            style={{
              width: 200,
              color: "rgba(0,0,0,0.3)",
              fontSize: 13,
              textAlign: "center",
              marginBottom: 20,
              lineHeight: 25
            }}
          >No downloads available at the moment, visit the home page and download some songs.</Text>
          <TouchableOpacity
            style={{
              borderWidth: 1,
              borderRadius: 5,
              borderColor: "#ddd",
              padding: 10
            }}
            onPress={() => props.navigation.navigate("Home")}
          >
            <Text
              style={{
                color: "#666",
                fontSize: 14
              }}
            >View Songs</Text>
          </TouchableOpacity>
        </View>
      )
    } else
      return (
        <ScrollView
          horizontal={false}
          contentContainerStyle={styles.scrollArea_contentContainerStyle}
        >
          {result}
        </ScrollView>
      );
  }

  useEffect(() => {
    pullDownloads()
  }, [])

  return (
    <View style={{ flex: 1 }}>
      <Header navigation={props.navigation} page="My Downloads" />      
      {music.length < 1 && !loading ?
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: "center"
          }}
        >
          <Text
            style={{
              width: 200,
              color: "rgba(0,0,0,0.3)",
              fontSize: 13,
              textAlign: "center",
              marginBottom: 20,
              lineHeight: 25
            }}
          >No downloads available at the moment, visit the home page and download some songs.</Text>
          <TouchableOpacity
            style={{
              borderWidth: 1,
              borderRadius: 5,
              borderColor: "#ddd",
              padding: 10
            }}
            onPress={() => props.navigation.push("Home")}
          >
            <Text
              style={{
                color: "#666",
                fontSize: 14
              }}
            >View Songs</Text>
          </TouchableOpacity>
        </View>
        :
        <View style={{
          flex: 1
        }}>{showHome(props)}</View>
      }
      {loading ?
        <ActivityIndicator size="large" color="#000" style={{ marginTop: 100, alignSelf: "center" }} />
        : null}
      <View style={{height: 100}} />
      <ToolBar navigation={props.navigation} active={4} />
    </View>
  );
}

const styles = StyleSheet.create({
  likeimage: {
    width: 15,
    height: 15,
    marginTop: 0,
    tintColor: "rgba(241, 98, 35, 1)",
    alignSelf: "flex-end"
  },
  commentimage: {
    width: 15,
    height: 15,
    marginTop: 0,
    tintColor: "#05cd51",
    alignSelf: "flex-end"
  },
  statimage: {
    width: 50,
    height: 50,
    marginTop: 0,
    marginRight: 10,
    // tintColor: "rgba(241, 98, 35, 1)",
    alignSelf: "flex-end"
  },
  musicimage: {
    width: 50,
    height: 50,
    borderRadius: 100,
    marginTop: 5,
    marginLeft: 10,
    // tintColor: "rgba(241, 98, 35, 1)",
    alignSelf: "center"
  },
  statimage2: {
    width: 15,
    height: 15,
    marginTop: 20,
    tintColor: "rgba(241, 98, 35, 1)",
    alignSelf: "center"
  },
  capsules: {
    // borderWidth: 1,
    // borderTopColor: "transparent",
    // borderLeftColor: "transparent",
    // borderRightColor: "transparent",
    // borderBottomColor: "#ddd",
    flexDirection: "row",
    width: "90%",
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10
  },
  divider: {
    height: 1,
    backgroundColor: "#000",
    alignSelf: "stretch"
  },
});

export default Downloads;