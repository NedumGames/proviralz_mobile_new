import React, { useEffect, Component } from 'react';
import {
  StyleSheet,
  Alert,
  View,
  TouchableOpacity,
  Image,
  ImageBackground,
} from 'react-native';

import { fonts, colors } from '../styles';
import { Text } from '../components/StyledText';
import AppIntroSlider from 'react-native-app-intro-slider';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class Walkthrough extends Component {

  constructor(props) {
    super(props)
  }

  _move = () => {
    this.props.navigation.push("Register")
  };

  _renderItem = ({ item }) => {
    var img = { uri: item.image }
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: item.backgroundColor,
          alignItems: 'center',
          justifyContent: 'space-around',
          paddingBottom: 100
        }}>
        <Text style={{
          color: item.textColor,
          fontSize: 20,
          fontFamily: fonts.primaryBold
        }}>{item.title}</Text>
        {/* <Image style={{
          height: 100,
          width: 200,
          tintColor: item.textColor
        }}  */}
        <Text>
          <Icon
            size={150}
            color={item.textColor}
            name={item.icon}
          />
        </Text>
        {/* resizeMode="contain"
        source={item.image} /> */}
        <Text style={{
          color: item.textColor,
          textAlign: "center",
          fontFamily: fonts.primaryLight,
          fontSize: 15
        }}>{item.text}</Text>
      </View>
    );
  };

  _renderNextButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Icon
          name="arrow-right"
          color="rgba(255, 255, 255, .9)"
          size={24}
        />
      </View>
    );
  };
  _renderDoneButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Icon
          name="check"
          color="rgba(255, 255, 255, .9)"
          size={24}
        />
      </View>
    );
  };

  _renderSkipButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Text style={{
          color: colors.white
        }}>Skip</Text>
      </View>
    );
  };

  render() {
    return (
      <AppIntroSlider
        data={slides}
        renderItem={this._renderItem}
        onDone={this._move}
        showSkipButton={true}
        onSkip={this._move}
        // renderSkipButton={this._renderSkipButton}
        renderDoneButton={this._renderDoneButton}
        renderNextButton={this._renderNextButton}
      />
    );
  }
}

const slides = [
  {
    key: 's1',
    text: 'Here is a walkthrough explaining in details what 9JaGo is all about.',
    title: 'What is 9JaGo all about.',
    icon: "tablet",
    backgroundColor: colors.primary,
    textColor: colors.white
  },
  {
    key: 's2',
    title: 'Create a cause',
    text: 'Upto 25% off on Domestic Flights',
    icon: "podcast",
    backgroundColor: colors.white,
    textColor: colors.primary
  },
  {
    key: 's3',
    title: 'Sponsor a cause',
    text: 'Enjoy Great offers on our all services',
    icon: "handshake-o",
    backgroundColor: colors.primary,
    textColor: colors.white
  },
  {
    key: 's4',
    title: 'Run different causes',
    text: ' Best Deals on all our services',
    icon: "list",
    backgroundColor: colors.white,
    textColor: colors.primary
  },
  {
    key: 's5',
    title: 'Award causes',
    text: 'Enjoy Travelling on Bus with flat 100% off',
    icon: "trophy",
    backgroundColor: colors.primary,
    textColor: colors.white
  }
];

const styles = StyleSheet.create({
  buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: colors.primary,//'rgba(0, 0, 0, .2)',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },

  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  buttonsContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center",
  },
  nerdImage: {
    flex: 1,
    alignContent: "center",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
  },
  bgImage: {
    flex: 1,
    marginHorizontal: -20,
    width: "100%"
  },
  section: {
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sectionLarge: {
    flex: 2,
    justifyContent: 'space-around',
  },
  sectionHeader: {
    marginBottom: 8,
  },
  priceContainer: {
    alignItems: 'center',
  },
  description: {
    padding: 15,
    lineHeight: 25,
  },
  titleDescription: {
    color: '#19e7f7',
    textAlign: 'center',
    fontFamily: fonts.primaryRegular,
    fontSize: 15,
  },
  title: {
    marginTop: 30,
  },
  price: {
    marginBottom: 5,
  },
  priceLink: {
    borderBottomWidth: 1,
    borderBottomColor: colors.primary,
  },
});
