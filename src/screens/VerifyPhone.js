import React, { useState, Component } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  Image,
  ImageBackground,
  Platform,
  TouchableOpacity,
  TextInput,
  Text,
  ScrollView,
  TouchableOpacityBase
} from "react-native";
import ToolBar from "../components/ToolBar";
import Header from "../components/Header";

function VerifyPhone(){
    return (
        <View>
            <Header navigation={props.navigation} page="Phone verification" />
        </View>
    )
}

export default VerifyPhone;