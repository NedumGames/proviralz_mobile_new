import React, { useState, Component } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  TextInput,
  Alert,
  Text,
  Image,
  ToastAndroid,
  Linking,
  TouchableOpacity
} from "react-native";
import Divider from "../components/Divider";
import Modal from 'react-native-modal';
import DropDownPicker from 'react-native-dropdown-picker';
import Loading from "../components/Loading";
import AsyncStorage from '@react-native-community/async-storage';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-community/google-signin';

GoogleSignin.configure({
  // scopes: ['https://www.googleapis.com/auth/drive.readonly'],  // what API you want to access on behalf of the user, default is email and profile
  webClientId: '824493360358-crqnfck96tmme0b41elqlc3594f4v5ul.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
  offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
  // hostedDomain: '', // specifies a hosted domain restriction
  // loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
  forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
  // accountName: 'project-824493360358', // [Android] specifies an account name on the device that should be used
  // iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
});

class Signup extends Component {

  state = {
    showModal: false,
    pin: 0,
    loading: false,
    fullname: "",
    username: "",
    gender: "",
    email: "",
    phone: "",
    password: "",
    confirmPassword: "",
    token: ""
  }

  constructor(prop) {
    super(prop)
    this.props = prop


    AsyncStorage.getItem('userdetails', (err, _data) => {
      var _res = JSON.parse(_data)
      if (_res && _res.user) {
        this.props.navigation.navigate("Home")
        return;
      }
    })
  }

  componentDidMount() {

  }

  promptError = () => {
    this.setState({ showModal: true })
  }

  sendErrorMessage = (message) => {
    Alert.alert(
      'Validation Error',
      `Hi! ${message}.`,
      [
        {
          text: 'Okay',
        }
      ],
      { cancelable: false }
    );
  }

  sendpin = () => {

    if (this.state.fullname.length < 5) {
      this.sendErrorMessage("Full name should be atleast 10 digits.")
      return;
    }
    if (this.state.username.length < 5) {
      this.sendErrorMessage("Username should be atleast 8 digits.")
      return;
    }
    if (this.state.gender.length < 4) {
      this.sendErrorMessage("Your gender is required.")
      return;
    }
    if (this.state.email.length < 5) {
      this.sendErrorMessage("Email address is required.")
      return;
    }
    if (this.state.phone.length < 10) {
      this.sendErrorMessage("Phone number is required.")
    }
    if (this.state.password.length < 8) {
      this.sendErrorMessage("Password address is required, and must be above 8 characters.")
      return;
    }
    if (this.state.password !== this.state.confirmPassword) {
      this.sendErrorMessage("Your passwords don't match.")
      return;
    }

    this.setState({ loading: true })

    var a = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    var n;
    var r = [];
    for (n = 1; n <= 7; ++n) {
      var i = Math.floor((Math.random() * (9 - n)) + 1);
      r.push(a[i]);
      a[i] = a[9 - n];
    }
    var s = "";
    for (i = 0; i < 7; i++) {
      s += r[i] + "";
    }

    this.setState({ pin: s })

    fetch(`https://proviralzmobileapp.proviralz.com/api/sendpin/${this.state.fullname}/${this.state.email}/${this.state.phone}/${s}`, {
      method: 'GET', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      // body: JSON.stringify(signdata) // body data type must match "Content-Type" header
    })
      .then(res => res.json())
      .then(result => {
        this.setState({ loading: false })
        if (!result.success) {
          if (result.message != undefined) {
            Alert.alert(
              'Registration Error',
              `Hi! ${result.message}`,
              [
                {
                  text: 'Okay',
                }
              ],
              { cancelable: false }
            );
          } else {
            Alert.alert(
              'Registration Error',
              'Hi! there is an issue with your details, please check them and try again.',
              [
                {
                  text: 'Okay',
                }
              ],
              { cancelable: false }
            );
          }
          return;
        } else {

          this.setState({ showModal: true })
        }
      })
      .catch(e => { })
  }

  finalsignup = () => {
    this.setState({ loading: true })

    let signupdata = {
      "name": this.state.fullname,
      "phone": this.state.phone,
      "email": this.state.email,
      "password": this.state.password,
      "username": this.state.username,
      "gender": this.state.gender,
      "type": 1
    }

    fetch("https://proviralzmobileapp.proviralz.com/api/register", {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify(signupdata) // body data type must match "Content-Type" header
    })
      .then(res => res.json())
      .then((result) => {
        this.setState({ loading: false })
        if (!result.success) {
          if (result.message != undefined) {
            ToastAndroid.showWithGravity(
              "Registration not successful, try again.",
              ToastAndroid.SHORT,
              ToastAndroid.CENTER
            )
          } else {
            ToastAndroid.showWithGravity(
              "There is an issue with your details, please check them and try again.",
              ToastAndroid.SHORT,
              ToastAndroid.CENTER
            )
          }
          return;
        } else {
          ToastAndroid.showWithGravity(
            "Your registration was successful!.",
            ToastAndroid.SHORT,
            ToastAndroid.CENTER
          )
          props.navigation.navigate("Login")
        }
      })
      .catch(e => {
        this.setState({ loading: false })
        ToastAndroid.showWithGravity(
          "There is an issue completing your registration this time.",
          ToastAndroid.SHORT,
          ToastAndroid.CENTER
        )
      })
  }

  _googleSignUp = async () => {
    if (!statusCodes.SIGN_IN_REQUIRED) {
      GoogleSignin.revokeAccess();
    }
    GoogleSignin.signOut()

    try {
      GoogleSignin.hasPlayServices();
      const userInfo = GoogleSignin.signIn();

      if (userInfo.idToken != undefined) {
        this.setState({ loading: true });
        this.runSignin(userInfo)
      } 
    } catch (error) {
      this.setState({ loading: false })
      GoogleSignin.signOut()
      ToastAndroid.showWithGravity(
        "Registration Error, try again, "+error,
        ToastAndroid.SHORT,
        ToastAndroid.CENTER
      )
    }
  }

  runSignin(userInfo) {
    let data = {
      "email": userInfo.user.email,
      "name": userInfo.user.name,
      "photo": userInfo.user.photo,
      "password": userInfo.user.id,
      "type": 1
    }

    fetch("https://proviralzmobileapp.proviralz.com/api/googleregister", {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify(data) // body data type must match "Content-Type" header
    })
      .then(res => res.json())
      .then(result => {
        this.setState({ loading: false })
        if (!result.success) {
          ToastAndroid.showWithGravity(
            "Your registration failed, "+result.message,
            ToastAndroid.SHORT,
            ToastAndroid.CENTER
          )
          return;
        } else {
          ToastAndroid.showWithGravity(
            "Your registration was successful!.",
            ToastAndroid.SHORT,
            ToastAndroid.CENTER
          )
          props.navigation.navigate("Login")
        }
      })
      .catch((e) => {
        this.setState({ loading: false })
        ToastAndroid.showWithGravity(
          "Your registration failed!",
          ToastAndroid.SHORT,
          ToastAndroid.CENTER
        )
      })
  }

  render() {
    return (
      <View style={styles.rect} >
        <Loading
          load={this.state.loading}
        />
        <Modal
          isVisible={this.state.showModal}
          animationIn="slideInUp"
          animationInTiming={500}
          animationOut="slideOutDown"
          animationOutTiming={1000}
        // onBackButtonPress={() => {
        //   onToggleHideModal()
        // }}
        >
          <View
            style={{
              backgroundColor: "#fff",
              borderRadius: 5,
              margin: 20,
              padding: 20,
              flexDirection: "column"
            }}
          >
            <Text
              style={{
                color: "#000",
                textAlign: "center",
              }}
            >A 7 pin code was sent to you, enter pin in the box below.</Text>
            <TextInput

              style={{
                marginLeft: 20,
                marginRight: 20,
                marginBottom: 20,
                borderColor: "#000",
                borderTopWidth: 0,
                borderLeftWidth: 0,
                borderRightWidth: 0,
                borderRadius: 5,
                borderBottomWidth: 2
              }}
              onChange={value => {
                this.setState({ token: value.nativeEvent.text })
              }}
            >

            </TextInput>

            <View
              style={{
                flexDirection: "row"
              }}
            >
              <TouchableOpacity
                style={{
                  flex: 1
                }}
                onPress={() => this.setState({ showModal: false })}
              >
                <Text
                  style={{
                    color: "skyblue",
                    fontSize: 12,
                    textAlign: "center",
                    paddingLeft: 5,
                    paddingRight: 5,
                    paddingTop: 5,
                    paddingBottom: 5,
                  }}
                >Back</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  flex: 1
                }}
                onPress={() => {
                  this.sendpin()
                }}
              >
                <Text
                  style={{
                    color: "green",
                    fontSize: 11,
                    textAlign: "center",
                    paddingLeft: 5,
                    paddingRight: 5,
                    paddingTop: 5,
                    paddingBottom: 5,
                  }}
                >Re-send Pin</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  flex: 1,
                }}
                onPress={() => {
                  if (this.state.pin !== this.state.token) {
                    this.sendErrorMessage("Wrong pin code, try re-sending or check your phone number.")
                    return;
                  }
                  this.finalsignup()
                }}
              >
                <Text
                  style={{
                    color: "#fff",
                    backgroundColor: "rgba(241, 98, 35, 1)",
                    paddingLeft: 5,
                    paddingRight: 5,
                    paddingTop: 5,
                    paddingBottom: 5,
                    borderRadius: 5,
                    fontSize: 12,
                    textAlign: "center"
                  }}
                >Submit</Text>
              </TouchableOpacity>
            </View>

          </View>
        </Modal>
        <StatusBar hidden />
        <View style={styles.textInputColumn}>
          <View style={styles.rect3}>
            <Image
              source={require("../assets/images/proviralz_logo.jpg")}
              resizeMode="contain"
              style={styles.logo}
            ></Image>
            <Text style={styles.proviralz}>Create your account</Text>
          </View>
          <Text style={styles.text4}>Let's set things rolling, please fill out these basic details to get started.</Text>
          {/* <Text style={styles.label}>Full name</Text>     */}
          <TextInput
            placeholder="Full Name"
            placeholderTextColor="rgba(0,0,0,0.5)"
            textBreakStrategy="highQuality"
            clearButtonMode="always"
            style={styles.textInput}
            onChange={value => {
              this.setState({ fullname: value.nativeEvent.text })
            }}
          ></TextInput>
          <View style={styles.materialButtonViolet1Row}>
            <TextInput
              placeholder="Username"
              clearButtonMode="always"
              placeholderTextColor="rgba(0,0,0,0.5)"
              style={styles.emailInput}
              onChange={value => {
                this.setState({ username: value.nativeEvent.text })
              }}
            ></TextInput>
            <DropDownPicker
              items={[
                { label: 'Male', value: 'male' },
                { label: 'Female', value: 'female' },
              ]}
              placeholder="Gender"
              defaultIndex={0}
              style={{
                color: "rgba(0,0,0,0.7)",
                borderRadius: 5,
                fontSize: 14,
                borderColor: "#ddd",
                borderWidth: 1,
                backgroundColor: "#e4e7e8",
                lineHeight: 20,
                marginTop: 10,
                marginLeft: 5,
                marginRight: 0,
                flex: 1,
                padding: 50
              }}
              containerStyle={{
                width: "50%",
                height: 50
              }}
              onChangeItem={item => {
                this.setState({ gender: item.value })
              }}
            />
          </View>
          <View style={styles.materialButtonViolet1Row}>
            <TextInput
              placeholder="Email Address"
              clearButtonMode="always"
              placeholderTextColor="rgba(0,0,0,0.5)"
              style={styles.emailInput}
              onChange={value => {
                this.setState({ email: value.nativeEvent.text })
              }}
            ></TextInput>
            <TextInput
              placeholder="Phone Number"
              clearButtonMode="always"
              placeholderTextColor="rgba(0,0,0,0.5)"
              style={styles.phoneInput}
              onChange={value => {
                this.setState({ phone: value.nativeEvent.text })
              }}
            ></TextInput>
          </View>
          <View style={styles.materialButtonViolet1Row}>
            <TextInput
              placeholder="Password"
              clearButtonMode="always"
              placeholderTextColor="rgba(0,0,0,0.5)"
              secureTextEntry={true}
              style={styles.passwordInput}
              onChange={value => {
                this.setState({ password: value.nativeEvent.text })
              }}
            ></TextInput>
            <TextInput
              placeholder="Confirm Password"
              clearButtonMode="always"
              placeholderTextColor="rgba(0,0,0,0.5)"
              secureTextEntry={true}
              style={styles.passwordInput2}
              onChange={value => {
                this.setState({ confirmPassword: value.nativeEvent.text })
              }}
            ></TextInput>
          </View>

          <TouchableOpacity
            onPress={() => {
              Linking.openURL("https://mobile.proviralz.com/tos.php").catch((err) => console.error('An error occurred', err));
            }}
          >
            <View
              style={styles.agreement}
            >
              <Text style={styles.attribute}>By continuing, you accept our</Text>
              <Text style={styles.attribute2}> Terms of service & Privacy Policy.</Text>
            </View>
          </TouchableOpacity>

          <View style={styles.materialButtonViolet2Row}>
            <TouchableOpacity
              onPress={() => {
                this.sendpin()
              }}
              style={styles.registerbutton}>
              <Text style={styles.register}>Register</Text>
            </TouchableOpacity>
            <Text style={styles.or}>Or</Text>
            <GoogleSigninButton
              style={styles.googlebutton}
              size={GoogleSigninButton.Size.Wide}
              color={GoogleSigninButton.Color.Light}
              onPress={() => this._googleSignUp()}
              disabled={this.state.loading}
            />

          </View>
        </View>
        <View style={styles.textInputColumnFiller}></View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  rect: {
    flex: 1,
    backgroundColor: "#fff"
  },
  logo: {
    width: 30,
    height: 30,
    marginLeft: 20,
    marginBottom: 0,
    alignSelf: "center"
  },
  agreement: {
    textAlign: "center",
    fontSize: 11,
    marginBottom: 20,
    width: "75%",
    alignSelf: "center",
    marginTop: 50,
    flexDirection: "row"
  },
  termAndCondition: {
    flexDirection: "row",
    alignSelf: "center",
    color: "rgba(0,0,0,0.7)",
    fontSize: 11,
    lineHeight: 20,
    marginTop: 50,
    width: "75%",
    marginBottom: 100,
    textAlign: "center",
    alignSelf: "center"
  },
  attribute: {
    color: "#000",
    fontSize: 10,
  },
  attribute2: {
    color: "rgba(241, 98, 35, 1)",
    fontSize: 10,
    textDecorationLine: "underline",
    textDecorationColor: "rgba(241, 98, 35, 1)",
  },
  textInput: {
    width: "90%",
    height: 40,
    color: "rgba(0,0,0,0.7)",
    borderRadius: 5,
    fontSize: 14,
    borderColor: "#ddd",
    borderWidth: 1,
    backgroundColor: "#e4e7e8",
    lineHeight: 20,
    marginTop: 10,
    marginLeft: 20,
    marginRight: 20,
    padding: 5
  },
  label: {
    fontSize: 12,
    marginLeft: 20,
    color: "rgba(241, 98, 35, 1)"
  },
  emailInput: {
    width: "49.5%",
    height: 40,
    color: "rgba(0,0,0,0.7)",
    borderRadius: 5,
    fontSize: 14,
    borderColor: "#ddd",
    flex: 1,
    borderWidth: 1,
    backgroundColor: "#e4e7e8",
    lineHeight: 20,
    marginTop: 10,
    marginLeft: 0,
    marginRight: 5,
    padding: 5
  },
  phoneInput: {
    width: "49.5%",
    height: 40,
    color: "rgba(0,0,0,0.7)",
    borderRadius: 5,
    fontSize: 14,
    borderColor: "#ddd",
    borderWidth: 1,
    backgroundColor: "#e4e7e8",
    lineHeight: 20,
    marginTop: 10,
    marginLeft: 5,
    flex: 1,
    marginRight: 0,
    padding: 5
  },
  passwordInput: {
    width: "49.5%",
    height: 40,
    color: "rgba(0,0,0,0.7)",
    borderRadius: 5,
    fontSize: 14,
    borderColor: "#ddd",
    borderWidth: 1,
    flex: 1,
    backgroundColor: "#e4e7e8",
    lineHeight: 20,
    marginTop: 10,
    marginLeft: 0,
    marginRight: 5,
    padding: 5
  },
  passwordInput2: {
    width: "49.5%",
    flex: 1,
    height: 40,
    color: "rgba(0,0,0,0.7)",
    borderRadius: 5,
    fontSize: 14,
    borderColor: "#ddd",
    borderWidth: 1,
    backgroundColor: "#e4e7e8",
    lineHeight: 20,
    marginTop: 10,
    marginLeft: 5,
    marginRight: 0,
    padding: 5
  },
  text4: {
    color: "rgba(0,0,0,0.7)",
    fontSize: 12,
    lineHeight: 20,
    marginTop: 30,
    width: "75%",
    marginBottom: 100,
    textAlign: "center",
    alignSelf: "center"
  },
  rect3: {
    height: 50,
    flexDirection: "row",
    backgroundColor: "#000",
    marginTop: 0
  },
  proviralz: {
    color: "rgba(255,255,255,0.6)",
    fontSize: 16,
    alignSelf: "flex-start",
    marginTop: 15,
    textAlignVertical: "center",
    marginLeft: 20,
  },
  textInput3: {
    width: 312,
    height: 42,
    color: "rgba(0,0,0,1)",
    borderColor: "rgba(0,0,0,1)",
    borderWidth: 0,
    borderBottomWidth: 2,
    fontSize: 15,
    lineHeight: 20,
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
  },
  textInput4: {
    width: 312,
    height: 42,
    color: "rgba(0,0,0,1)",
    borderColor: "rgba(0,0,0,1)",
    borderWidth: 0,
    borderBottomWidth: 2,
    fontSize: 15,
    lineHeight: 20,
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20
  },
  textInput5: {
    width: 312,
    height: 42,
    color: "rgba(0,0,0,1)",
    borderColor: "rgba(0,0,0,1)",
    borderWidth: 0,
    borderBottomWidth: 2,
    fontSize: 15,
    lineHeight: 20,
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20
  },
  textInput6: {
    width: 312,
    height: 42,
    color: "rgba(0,0,0,1)",
    borderColor: "rgba(0,0,0,1)",
    borderWidth: 0,
    borderBottomWidth: 2,
    fontSize: 15,
    lineHeight: 20,
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20
  },
  materialButtonViolet1: {
    height: 36,
    width: 168,
    marginTop: 5
  },
  textInput7: {
    width: 129,
    height: 42,
    color: "rgba(0,0,0,1)",
    borderColor: "rgba(0,0,0,1)",
    borderWidth: 0,
    borderBottomWidth: 2,
    fontSize: 15,
    lineHeight: 20,
    marginLeft: 14,
    marginTop: 0
  },
  materialButtonViolet1Row: {
    flexDirection: "row",
    marginRight: 20,
    marginLeft: 20,
  },
  materialButtonViolet2Row: {
    flexDirection: "row",
    marginRight: 20,
    marginLeft: 20,
  },
  or: {
    color: "rgba(0,0,0,1)",
    fontSize: 18,
    lineHeight: 20,
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
  },
  materialButtonDanger1: {
    height: 48,
    width: 182,
    marginTop: 17,
    marginLeft: 98
  },
  textInputColumn: {
    marginLeft: -2
  },
  textInputColumnFiller: {
    flex: 1
  },
  rect2: {
    height: 71,
    shadowOpacity: 1,
    marginBottom: 29,
    marginTop: 20
  },
  divider: {
    width: 360,
    height: 1
  },
  registerbutton: {
    height: 50,
    backgroundColor: "rgba(241, 98, 35, 1)",
    borderRadius: 100,
    justifyContent: "center",
    marginTop: 0,
    alignSelf: "center",
  },
  googlebutton: {
    width: 150,
    height: 50,
    // backgroundColor: "rgba(239,17,25,0.7)",
    padding: 10,
    borderRadius: 10,
    justifyContent: "center",
  },
  register: {
    color: "rgba(86,2,4,1)",
    fontSize: 15,
    lineHeight: 20,
    marginLeft: 20,
    marginRight: 20,
    alignSelf: "center"
  },
  google: {
    color: "#ffffff",
    fontSize: 12,
    lineHeight: 20,
    marginLeft: 20,
    marginRight: 20,
    alignSelf: "center"
  }
});

export default Signup;
