import React, { useState, Component } from "react";
import {
  View,
  ScrollView,
  ImageBackground,
  Image,
  FlatList,
  TouchableOpacity,
  Alert,
  Text,
  TextInput,
  ActivityIndicator,
  Platform,
  TouchableWithoutFeedback,
  // AsyncStorage,
} from "react-native";
import ToolBar from "../components/ToolBar";
import Header from "../components/Header";
import MusicList from "../components/MusicList";
import { Card } from '../components/Card';
import { Title } from '../components/Title';
import AsyncStorage from '@react-native-community/async-storage';
import { Searchbar } from "react-native-paper";
import AdVert from "../components/AdVert";



class Home extends Component {

  state = {
    resultset: [],
    musicData: [],
    musicCache: [],
    result: [],
    token: "",
    loading: false,
    searchQuery: ""
  }

  props = []

  constructor(props) {
    super(props)
    this.props = props
    this.state = {
      musicData: [],
      musicCache: []
    }
  }

  componentDidMount() {
    AsyncStorage.getItem("userdetails", (err, result) => {
      var _res = JSON.parse(result)

      if (_res == null) {
        this.props.navigation.navigate("Login")
        return;
      }
      this.setState({ token: _res.token })
      this.fetchMusic()
    });

    this.loadMusic()
  }

  loadMusic = async () => {
    this.setState({
      loading: true
    })
    try {
      await fetch("http://proviralzmobileapp.proviralz.com/api/getallpost", {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'authorization': `Bearer ${this.state.token}`
        },
        // body: JSON.stringify(signupdata) // body data type must match "Content-Type" header
      })
        .then(res => res.json())
        .then(result => {

          this.setState({
            musicData: result.data,
            musicCache: result.data,
            loading: false,
            searchQuery: ""
          })
        })
    } catch (error) {
      this.setState({
        loading: false
      })
    }
  }

  fetchMusic = () => {
    this.loadMusic()
  }

  filterSongs = (song) => {
    let arr = []
    this.state.musicData.forEach(music => {
      if (music.genre.toLowerCase() === song.toLowerCase()) {
        arr.push(music)
      }
    })
    if (arr.length < 1) {
      this.fetchMusic()
      Alert.alert("No Result found!",
        `No ${song} song available at the moment, try again later.`,
        [{
          text: "Ok",
        }],
        { cancelable: false }
      )
      this.fetchMusic()
      return
    }
    this.setState({ result: arr, loading: false, musicData: arr })
  }

  _renderItem = ({ item, idx }) => {
    return (
      <MusicList
        key={idx}
        musicImage={item.cover ? item.cover : null}
        artist={item.artist ? item.artist : null}
        title={item.title ? item.title : null}
        audio={item.audio ? item.audio : null}
        video={item.video ? item.video : null}
        genre={item.genre ? item.genre : null}
        id={item.id ? item.id : null}
        navigation={this.props.navigation}
        usertoken={this.state.token} />
    )
  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1, backgroundColor: "rgba(0,0,0,0.1)", width: "100%"
        }}
      />
    )
  }

  onChangeSearch = (query) => {
    this.setState({ searchQuery: query })
  }

  searchSongs = () => {
    let arr = []

    this.state.musicCache.forEach(music => {
      if (music.title.toLowerCase() === this.state.searchQuery.toLowerCase() || music.artist.toLowerCase() === this.state.searchQuery.toLowerCase()) {
        arr.push(music)
      }
    })
    if (arr.length < 1) {
      this.fetchMusic()
      Alert.alert("Not found error!",
        `This song can not be found, try again later.`,
        [{
          text: "Ok",
        }],
        { cancelable: false }
      )
      this.fetchMusic()
      return
    }
    this.setState({ result: arr, loading: false, musicData: arr })
  }

  cancleSearch() {
    this.setState({ musicData: this.state.musicCache });
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <Header navigation={this.props.navigation} page="Home" />
        {/* {this._genreBox()} */}
        <View style={{ margin: 20, marginBottom: 0 }}>
          <Searchbar
            placeholder="Search"
            inputStyle={{ fontSize: 15 }}
            onSubmitEditing={() => this.searchSongs()}
            onChangeText={this.onChangeSearch}
            value={this.state.searchQuery}
          />
        </View>
        <View
          style={{
            flexDirection: "row"
          }}
        >
          <Text style={styles.headtext}>Latest Music Post</Text>
          <TouchableOpacity
            onPress={() => {
              this.fetchMusic()
            }}
          >
            <Image
              style={{
                marginRight: 20,
                marginLeft: 20,
                marginTop: 23,
                height: 15,
                width: 15,
                tintColor: "grey",
              }}
              source={require("../assets/images/refresh.png")}
              resizeMode="contain"
            ></Image>
          </TouchableOpacity>
        </View>
        <View style={{
          flex: 1,
        }}>
          {
            this.state.loading ?
              <View
                style={{
                  flex: 1, justifyContent: "center", alignItems: "center"
                }}
              >
                <ActivityIndicator size="large" animating color="rgba(241, 98, 35, 1)" />
              </View>
              :
              this.state.musicData.length > 0 ?
                <View style={{ paddingBottom: 60 }}>
                  <FlatList
                    maxToRenderPerBatch="1"
                    data={this.state.musicData}
                    renderItem={this._renderItem}
                    keyExtractor={item => item.id.toString()}
                  />
                </View>
                :
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: "center"
                  }}
                >
                  <Text
                    style={{
                      width: 200,
                      color: "rgba(0,0,0,0.3)",
                      fontSize: 13,
                      textAlign: "center",
                      marginBottom: 20,
                      lineHeight: 25
                    }}
                  >No post available at the moment be the first to make a post.</Text>
                  <TouchableOpacity
                    style={{
                      borderWidth: 1,
                      borderRadius: 5,
                      borderColor: "#ddd",
                      padding: 10
                    }}
                    onPress={() => this.props.navigation.navigate("Upload")}
                  >
                    <Text
                      style={{
                        color: "#666",
                        fontSize: 14
                      }}
                    >Make post</Text>
                  </TouchableOpacity>
                </View>
          }
        </View>
        <ToolBar navigation={this.props.navigation} active={1} />
      </View>
    );
  }

  _genreBox = () => {
    return (
      <View
        style={styles.searchBox}
      >
        <Text style={styles.genre}>Genres</Text>
        <ScrollView
          horizontal={true}
          contentContainerStyle={styles.scrollArea_contentContainerStyle}
        // onScroll={() => this.loadMusic()}
        >
          <TouchableOpacity
            style={styles.rect3}
            onPress={() => this.filterSongs("Hip-hop")}
          >
            <Text style={styles.text}>Hip-Hop</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.rect3}
            onPress={() => this.filterSongs("Raggae")}
          >
            <Text style={styles.text}>Raggea</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.rect3}
            onPress={() => this.filterSongs("Rap")}
          >
            <Text style={styles.text}>Rap</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.rect3}
            onPress={() => this.filterSongs("soul")}
          >
            <Text style={styles.text}>Soul</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.rect3}
            onPress={() => this.filterSongs("Afro Beats")}
          >
            <Text style={styles.text}>Afro Beats</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.rect3}
            onPress={() => this.filterSongs("High-life")}
          >
            <Text style={styles.text}>High-Life</Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    )
  }

}

styles = {
  mainContainer: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#fff"
  },
  text: {
    color: "#fff",
    fontSize: 12,
    // lineHeight: 20,
    alignSelf: "center"
  },
  headtext: {
    color: "#666",
    fontSize: 15,
    // flex: 1,
    marginLeft: 20,
    marginTop: 20,
    marginBottom: 15,
    fontWeight: "bold",
    fontStyle: "italic"
  },
  genre: {
    color: "#666",
    fontSize: 15,
    // lineHeight: 20,
    // alignSelf: "flex-start",
    marginLeft: 20,
    marginTop: 20,
    marginBottom: 0,
    fontWeight: "bold",
    fontStyle: "italic"
  },
  searchBox: {
    width: "100%",
    // height: 100,
    borderWidth: 1,
    flowDirection: "row",
    borderTopColor: "transparent",
    borderLeftColor: "transparent",
    borderRightColor: "transparent",
    borderBottomColor: "#ddd",
  },
  rect3: {
    height: 35,
    // flex: 1,
    // width: 
    padding: 10,
    backgroundColor: "rgba(241, 98, 35, 1)",
    borderRadius: 100,
    // justifyContent: "center",
    marginTop: 20,
    marginBottom: 20,
    marginLeft: 10,
    marginRight: 20,
    // alignSelf: "center",
  },
  scrollArea_contentContainerStyle: {
    // height: 500
  },
  scrollArea_Style: {
    height: 500
  },
  textInput: {
    width: "90%",
    color: "#666",
    height: 45,
    borderRadius: 100,
    justifyContent: "center",
    fontSize: 13,
    borderColor: "#ddd",
    borderWidth: 1,
    backgroundColor: "#fff",
    lineHeight: 20,
    marginTop: 0,
    marginLeft: 0,
    marginRight: 0,
    alignSelf: "center",
    padding: 15
  },
}



export default Home;