import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import MaterialCardWithTextOverImage1 from "./MaterialCardWithTextOverImage1";

function Tweet(props) {
  return (
    <View style={[styles.rect, props.style]}>
      <MaterialCardWithTextOverImage1
        style={styles.materialCardWithTextOverImage1}
      ></MaterialCardWithTextOverImage1>
    </View>
  );
}

const styles = StyleSheet.create({
  rect: {
    justifyContent: "center"
  },
  materialCardWithTextOverImage1: {
    height: 285,
    width: 360,
    alignSelf: "center"
  }
});

export default Tweet;
