import React, { Component } from "react";
import { StyleSheet, Share, View, Image, Text, TouchableOpacity } from "react-native";
import Divider from "../components/Divider";
import { List, Avatar } from "react-native-paper";
import * as base64 from "base-64";
import * as utf8 from "utf8";
import Icon from "react-native-vector-icons/FontAwesome";


function Uploads(props) {

    const data = props.musicData
    const show = []

    const editSong = (data) => {
        props.editsong(data)
    }

    const deleteSong = (data) => {
        props.deleteSong(data)
    }

    if (data) {

        for (let i = 0; i < data.length; i++) {
            show.push(
                <View key={i} style={styles.capsules}>
                    <View style={{ flex: 1, flexDirection: "column", justifyContent: "center", alignItems: "center" }} >
                        <Avatar.Image
                            style={{ alignSelf: "center" }}
                            size={50}
                            source={{ uri: data[i].cover }} />
                        <Text style={{ marginLeft: 0, marginTop: 5, marginBottom: 10, alignSelf: "center", fontSize: 13 }}>{data[i].title}</Text>
                    </View>
                    <View style={{ width: "60%", flexDirection: "row", justifyContent: "center", alignContent: "center", paddingTop: 15 }} >
                        <View style={{ flex: 1, marginTop: 0 }}>
                            {data[i].status == 1 ?
                                <Icon name="check-circle" size={25} color="#05cd51" />
                                :
                                <Icon name="hourglass" size={20} color="rgba(241, 98, 35, 1)" />
                            }
                        </View>
                        <View style={{ flex: 1, marginTop: 0 }}>
                            <TouchableOpacity
                                onPress={() => {
                                    props.navigation.push("MusicPlayer", {
                                        src: data[i].cover,
                                        title: data[i].title,
                                        artist: data[i].artist,
                                        audio: data[i].audio,
                                        video: data[i].video,
                                        genre: data[i].genre,
                                        id: data[i].id,
                                        token: props.usertoken,
                                        navigation: props.navigation
                                    })
                                }}
                            >
                                <Icon name="play" size={25} color="#rgba(241, 98, 35, 1)" />
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 1, marginTop: 0 }}>
                            <TouchableOpacity
                                onPress={() => {
                                    editSong(data[i])
                                }}
                            >
                                <Icon name="edit" size={25} color="#000" />
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 1, marginTop: 0 }}>
                            <TouchableOpacity
                                onPress={() => {
                                    deleteSong(data[i])
                                }}
                            >
                                <Icon name="trash" size={25} color="#000" />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            )
        }

        return (show);
    }

    return null
}

function AdminUploads(props) {

    const data = props.musicData
    const show = []

    const editSong = (data) => {
        props.editsong(data)
    }

    const _approveSong = (id) => {
        props.changestatus(id)
    }

    const _unapproveSong = (id) => {
        props.changestatusback(id)
    }

    if (data) {

        for (let i = 0; i < data.length; i++) {
            show.push(
                <View key={i} style={styles.capsules}>
                    <View style={{ flex: 1, flexDirection: "column", justifyContent: "center", alignItems: "center" }} >
                        <Avatar.Image
                            style={{ alignSelf: "center" }}
                            size={50}
                            source={{ uri: data[i].cover }} />
                        <Text style={{ marginLeft: 0, marginTop: 5, marginBottom: 10, alignSelf: "center", fontSize: 13 }}>{data[i].title}</Text>
                    </View>
                    <View style={{ flex: 1, height: 50 }}>
                        {/* <Text style={{ marginLeft: 20, marginTop: 5, color: "#666"}}>Status</Text> */}
                        {data[i].status == 1 ?
                            <Text style={{ marginLeft: 20, marginTop: 30, color: "#05cd51", fontSize: 11, alignSelf: "center" }}>Done</Text> :
                            <Text style={{ marginLeft: 20, marginTop: 20, color: "rgba(241, 98, 35, 1)", fontSize: 11, alignSelf: "center", justifyContent: "center" }}>Pending</Text>}
                    </View>
                    <View
                        style={{
                            flex: 1
                        }}
                    >
                        {data[i].status == 0 ?
                            <TouchableOpacity
                                onPress={() => {
                                    _approveSong(data[i].id)
                                }}
                            >
                                <Image
                                    source={require("../assets/images/heart.png")}
                                    resizeMode="contain"
                                    style={styles.statimage}
                                ></Image>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity
                                onPress={() => {
                                    _unapproveSong(data[i].id)
                                }}
                            >
                                <Image
                                    source={require("../assets/images/004-cardiogram.png")}
                                    resizeMode="contain"
                                    style={styles._statimage}
                                ></Image>
                            </TouchableOpacity>
                        }
                    </View>
                    <View style={{ flex: 1, marginTop: 0 }}>
                        <TouchableOpacity
                            onPress={() => {
                                props.navigation.navigate("MusicPlayer", {
                                    src: data[i].cover,
                                    title: data[i].title,
                                    artist: data[i].artist,
                                    audio: data[i].audio,
                                    video: data[i].video,
                                    genre: data[i].genre,
                                    id: data[i].id,
                                    token: props.usertoken,
                                    navigation: props.navigation
                                })
                            }}
                        >
                            <Image
                                source={require("../assets/images/play.png")}
                                resizeMode="contain"
                                style={styles.statimage}
                            ></Image>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, marginTop: 0 }}>
                        <TouchableOpacity
                            onPress={() => {
                                editSong(data[i])
                            }}
                        >
                            <Image
                                source={require("../assets/images/edit.png")}
                                resizeMode="cover"
                                style={styles.statimage}
                            ></Image>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, marginTop: 0 }}>
                        <TouchableOpacity
                            onPress={() => {

                            }}
                        >
                            <Image
                                source={require("../assets/images/delete.png")}
                                resizeMode="cover"
                                style={styles.statimage2}
                            ></Image>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }

        return (show);
    }

    return null
}

const styles = StyleSheet.create({
    ctrls: {
        marginTop: 5,
        marginLeft: 0,
        flex: 1,
        justifyContent: "center",
        alignContent: "center"
    },
    statimage: {
        width: 15,
        height: 15,
        marginTop: 20,
        tintColor: "rgba(241, 98, 35, 1)",
        alignSelf: "flex-end"
    },
    _statimage: {
        width: 15,
        height: 15,
        marginTop: 20,
        tintColor: "#05cd51",
        alignSelf: "flex-end"
    },
    musicimage: {
        width: 50,
        height: 50,
        borderRadius: 100,
        // marginTop: 10,
        justifyContent: "center",
        // marginLeft: 20,
        alignSelf: "center"
    },
    statimage2: {
        width: 15,
        height: 15,
        marginTop: 20,
        tintColor: "rgba(241, 98, 35, 1)",
        alignSelf: "center"
    },
    capsules: {
        borderWidth: 1,
        borderTopColor: "transparent",
        borderLeftColor: "transparent",
        borderRightColor: "transparent",
        borderBottomColor: "#ddd",
        flexDirection: "row",
        // width: "80%",
        alignSelf: "center",
        marginTop: 10,
        // marginLeft: 10,
        marginRight: 10,
        marginBottom: 10
    },
    // capsules: {
    //     flexDirection: "row",
    //     marginBottom: 10,
    //     width: "50%",
    //     marginRight: 10
    // },
    divider: {
        height: 1,
        backgroundColor: "#000",
        alignSelf: "stretch"
    },
});

export {
    Uploads,
    AdminUploads
};