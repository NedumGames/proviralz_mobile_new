import React, { useState, Component } from "react";
import { StyleSheet, TouchableOpacity, View, Image, Text, BackHandler } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
import AdVert from "./AdVert";
import SongLinking from "../components/DeepLinkingSong";
import {
    GoogleSignin,
    GoogleSigninButton,
    statusCodes,
  } from '@react-native-community/google-signin';

function Header(props) {

    // const [loading, setLoading] = useState(false);
    const playAds = new AdVert;

    const logout = async () => {
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut()
        AsyncStorage.removeItem("userdetails", (err, data) => {            
            props.navigation.navigate("Login")
        });
        // props.navigation.navigate("Login")
    }

    return (
        <View style={styles.rect3}>
            {/* <Loading load={loading} /> */}
            <SongLinking {...props} />
            <Image
                source={require("../assets/images/proviralz_logo.jpg")}
                resizeMode="contain"
                style={styles.logoz}
            ></Image>
            <Text style={styles.proviral}>{props.page}</Text>
            <TouchableOpacity
                onPress={() => {
                    // setLoading(true)
                    // logout()
                    playAds.showInterstitialAd();                    
                    logout()
                }}
            >
                <Image
                    source={require("../assets/images/logout.png")}
                    resizeMode="contain"
                    style={styles.logout}
                ></Image>
            </TouchableOpacity>

        </View>
    )
}

const styles = {
    logoz: {
        width: 30,
        height: 30,
        marginLeft: 20,
        marginTop: 15,
        marginBottom: 0,
        justifyContent: "flex-start",
        alignSelf: "flex-start",
    },
    logout: {
        width: 25,
        height: 25,
        marginLeft: 20,
        marginTop: 20,
        marginBottom: 15,
        marginRight: 20,
        alignSelf: "flex-end",
        tintColor: "grey"//"rgba(241, 98, 35, 1)"
    },
    proviral: {
        color: "rgba(255,255,255,0.6)",
        fontSize: 16,
        flex: 1,
        alignSelf: "flex-start",
        marginTop: 17,
        textAlignVertical: "center",
        marginLeft: 10,
    },
    rect3: {
        height: 55,
        backgroundColor: "#000",
        marginTop: 0,
        flexDirection: "row",
    },
    logo: {
        width: 30,
        height: 50,
        marginTop: 2,
        marginBottom: 0,
        marginLeft: 20,
        justifyContent: "center",
        // tintColor: "#fff"
    },
    proviralz: {
        // color: "rgba(241, 98, 35, 1)",
        color: "rgba(255,255,255,0.6)",
        fontSize: 16,
        alignSelf: "flex-start",
        marginTop: 17,
        textAlignVertical: "center",
        marginLeft: 10,
        // tintColor: "#fff"
    },
}

export default Header;