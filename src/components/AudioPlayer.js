import React, { useState } from "react";
import PropTypes from "prop-types";
import TrackPlayer, {
    useTrackPlayerProgress,
    usePlaybackState,
    useTrackPlayerEvents
} from "react-native-track-player";
import Video from 'react-native-video';
import {
    Image,
    StyleSheet,
    Alert,
    Text,
    TouchableOpacity,
    View,
    ViewPropTypes,
    Dimensions
} from "react-native";
import AsyncStorage from '@react-native-community/async-storage';

function ProgressBar() {
    const progress = useTrackPlayerProgress();
    var length_ = 0

    if (progress.position > 0) {
        const screenWidth = Math.round(Dimensions.get('window').width);
        length_ = (progress.position * screenWidth) / progress.duration
    }

    return (
        <View style={styles.progress}>
            <View style={{ width: `${(progress.position / progress.duration) * 100}%`, backgroundColor: "#fff", height: 1 }} />
            <View style={{ width: "100%", flowDirection: "row", height: 10, }}>
                <Text style={{ flex: 1, fontSize: 9, color: "#fff", alignSelf: "flex-start", marginRight: 20 }}>{parseFloat(progress.duration / 100).toFixed(2)}</Text>
                <Text style={{ flex: 1, fontSize: 9, color: "#fff", alignSelf: "flex-end", marginLeft: 20, marginTop: -10 }}>{parseFloat(progress.position / 100).toFixed(2)}</Text>
            </View>
        </View>
    );
}

function ControlButton({ title, onPress }) {
    return (
        <TouchableOpacity style={styles.controlButtonContainer} onPress={onPress}>
            <Text style={styles.controlButtonText}>{title}</Text>
        </TouchableOpacity>
    );
}

ControlButton.propTypes = {
    title: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired
};

export default function AudioPlayer(props) {

    const playbackState = usePlaybackState();
    const [trackTitle, setTrackTitle] = useState("");
    const [trackArtwork, setTrackArtwork] = useState();
    const [trackArtist, setTrackArtist] = useState("");
    const [liked, setLiked] = useState(true);
    useTrackPlayerEvents(["playback-track-changed"], async event => {
        if (event.type === TrackPlayer.TrackPlayerEvents.PLAYBACK_TRACK_CHANGED) {
            const track = await TrackPlayer.getTrack(event.nextTrack);
            const { title, artist, artwork } = track || {};
            setTrackTitle(title);
            setTrackArtist(artist);
            setTrackArtwork(artwork);
        }
    });

    const { postid, style, onNext, onPrevious, onTogglePlayback, token } = props;

    var middleButtonText = "Play";
    var played = false

    if (
        playbackState === TrackPlayer.STATE_PLAYING ||
        playbackState === TrackPlayer.STATE_BUFFERING
    ) {
        middleButtonText = "Pause";
        played = true
    }

    const likeTrack = () => {
        var _data = {
            "post_id": postid,
            "comment_id": 1
        }

        fetch("https://proviralzmobileapp.proviralz.com/api/createlike", {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                "Authorization": `Bearer ${token}`
            },
            body: JSON.stringify(_data) // body data type must match "Content-Type" header
        })
            .then(res => res.json())
            .then(result => {
                return;
            })
            .catch((e) => {
            })
    }

    const downloadTrack = () => {
        var userdetails = {}
        AsyncStorage.getItem('userdetails', (err, _data) => {
            userdetails = JSON.parse(_data)
            var _data = {
                "post_id": postid,
                "owner_id": userdetails.user.id
            }

            fetch("https://proviralzmobileapp.proviralz.com/api/createdownloads", {
                method: 'POST', // *GET, POST, PUT, DELETE, etc.
                mode: 'cors', // no-cors, *cors, same-origin
                cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                credentials: 'same-origin', // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    "Authorization": `Bearer ${token}`
                },
                body: JSON.stringify(_data) // body data type must match "Content-Type" header
            })
                .then(res => res.json())
                .then(result => {
                    if(result.message != "" || result.message != undefined){
                        Alert.alert(
                            'Download alert!',
                            `Hi! ${result.message != undefined ? result.message : "There was an issue adding this song to your download list, try again."}`,
                            [                                
                                {
                                    text: 'Okay'
                                }
                            ],
                            { cancelable: false }
                        );
                    }
                })
                .catch((e) => {
                })
        })

    }

    return (
        <View style={{
            flexDirection: "column",
            height: 60,
            marginTop: 10,
            marginBottom: 20,
            width: "100%",
            alignItems: "center"
        }}>
            <ProgressBar />
            <View style={{
                flexDirection: "row",
                height: 50,
                marginTop: 5,
                marginBottom: 20,
                width: "70%",
                alignSelf: "center"
            }}>
                <TouchableOpacity
                    onPress={() => {
                        likeTrack()
                        setLiked(!liked)
                    }
                    }
                    style={{ flex: 1, alignSelf: "center", flowDirection: "row" }}
                >
                    <Image
                        source={!liked ? require("../assets/images/activity.png") : require("../assets/images/heart.png")}
                        resizeMode="contain"
                        style={styles.otherButton}
                    ></Image>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => onTogglePlayback()}
                    style={{ flex: 1, alignSelf: "center", flowDirection: "row" }}
                >
                    <Image
                        source={played ?
                            require("../assets/images/pause.png") :
                            require("../assets/images/play.png")}
                        resizeMode="contain"
                        style={styles.playButton}
                    ></Image>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() =>
                        downloadTrack()
                    }
                    style={{ flex: 1, alignSelf: "center", flowDirection: "row" }}
                >
                    <Image
                        source={require("../assets/images/download.png")}
                        resizeMode="contain"
                        style={styles.otherButton}
                    ></Image>
                </TouchableOpacity>
            </View>
        </View>
    );
}

AudioPlayer.propTypes = {
    style: ViewPropTypes.style,
    onNext: PropTypes.func.isRequired,
    onPrevious: PropTypes.func.isRequired,
    onTogglePlayback: PropTypes.func.isRequired,
};

AudioPlayer.defaultProps = {
    style: {}
};

const styles = StyleSheet.create({
    card: {
        width: "80%",
        elevation: 1,
        borderRadius: 4,
        shadowRadius: 2,
        shadowOpacity: 0.1,
        alignItems: "center",
        shadowColor: "black",
        backgroundColor: "white",
        shadowOffset: { width: 0, height: 1 }
    },
    cover: {
        width: 140,
        height: 140,
        marginTop: 20,
        backgroundColor: "grey"
    },
    progress: {
        height: 2,
        width: "90%",
        marginTop: 5,
        marginBottom: 15,
        alignSelf: "center",
        marginLeft: 20,
        marginRight: 20,
        flexDirection: "column",
        backgroundColor: "rgba(0,0,0,0.5)"
    },
    title: {
        marginTop: 10
    },
    artist: {
        fontWeight: "bold"
    },
    controls: {
        marginVertical: 20,
        flexDirection: "row"
    },
    controlButtonContainer: {
        flex: 1
    },
    text_: {
        width: "70%",
        color: "#000",
        height: 50,
        borderRadius: 100,
        justifyContent: "center",
        fontSize: 13,
        borderColor: "#ddd",
        borderWidth: 1,
        backgroundColor: "#fff",
        lineHeight: 20,
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
        alignSelf: "center",
        padding: 10
    },
    comment: {
        marginTop: 20,
        paddingTop: 20,
        borderWidth: 1,
        borderBottomColor: "transparent",
        borderLeftColor: "transparent",
        borderRightColor: "transparent",
        borderTopColor: "#ddd",
    },
    musicimage: {
        width: "70%",
        height: 200,
        alignSelf: "center",
        marginBottom: 20,
        marginTop: 30,
        borderRadius: 5,
        borderWidth: 5,
        borderColor: "rgba(0,0,0,0.6)"
    },
    playButton: {
        // flex: 1,
        width: 50,
        height: 50,
        marginTop: 10,
        marginBottom: 10,
        alignSelf: "center",
        tintColor: "#fff"
    },
    statimage: {
        width: 22,
        height: 22,
        marginTop: 25,
        marginLeft: 10,
        marginBottom: 10,
        alignSelf: "center",
        justifyContent: "center",
        tintColor: "#fff",
    },
    shareimage: {
        width: 20,
        height: 20,
        marginTop: 0,
        marginLeft: 10,
        marginBottom: 0,
        alignSelf: "center",
        justifyContent: "center",
        tintColor: "#fff",
    },
    otherButton: {
        // flex: 1,
        width: 20,
        height: 20,
        marginTop: 10,
        marginBottom: 10,
        alignSelf: "center",
        tintColor: "#fff",
    },
    heartButton: {
        // flex: 1,
        width: 20,
        height: 20,
        marginTop: 10,
        marginBottom: 10,
        alignSelf: "center",
        tintColor: "#000",
    },
    control1: {
        // height: "15%",
        flexDirection: "row",
        height: 60,
        marginTop: 20,
        marginBottom: 20,
        width: "70%",
        alignSelf: "center"
    },
    songtitle: {
        alignSelf: "center",
        marginTop: 3,
        marginBottom: 10,
        fontSize: 13,
        fontStyle: "italic",
        // fontWeight: "bold",
        color: "#fff"
    },
    artistname: {
        alignSelf: "center",
        marginTop: 5,
        marginBottom: 3,
        fontSize: 13,
        fontWeight: "bold",
        color: "#fff"
    },
    text: {
        color: "#fff",
        fontSize: 12,
        // lineHeight: 20,
        alignSelf: "center"
    },
    headtext: {
        color: "#666",
        fontSize: 15,
        // lineHeight: 20,
        // alignSelf: "flex-start",
        marginLeft: 20,
        marginTop: 20,
        marginBottom: 15,
        fontWeight: "bold",
        fontStyle: "italic"
    },
    rect3: {
        height: 35,
        // flex: 1,
        // width: 
        padding: 10,
        backgroundColor: "rgba(241, 98, 35, 1)",
        borderRadius: 100,
        // justifyContent: "center",
        marginTop: 20,
        marginBottom: 20,
        marginLeft: 10,
        marginRight: 20,
        // alignSelf: "center",
    },
    scrollArea_contentContainerStyle: {
        // height: "100%"
    },
    textInput: {
        width: "90%",
        color: "#666",
        height: 45,
        borderRadius: 100,
        justifyContent: "center",
        fontSize: 13,
        borderColor: "#ddd",
        borderWidth: 1,
        backgroundColor: "#fff",
        lineHeight: 20,
        marginTop: 25,
        marginLeft: 0,
        marginRight: 0,
        alignSelf: "center",
        padding: 15
    },
    controlButtonText: {
        fontSize: 18,
        textAlign: "center"
    }
});