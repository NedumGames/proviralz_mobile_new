import React from 'react';
import {Text, View, StyleSheet, Image, Platform, TouchableOpacity} from 'react-native';

function DownloadList(props){    
    
    const _deleteDownloads = (id) =>{
        props.deletedownloads(id)
    }

    return (
    <View style={styles.capsules}>
        <View style={{width: "25%", height: 50 }}>
            <Image
                source={{uri: props.musicImage}}
                resizeMode="cover"
                style={styles.musicimage}
            ></Image>
        </View>
        <View style={{ width: "50%",height: 50 }}>
            <Text 
                style={{ 
                    marginLeft: 0,
                    marginTop: 5, 
                    color: "rgba(241, 98, 35, 1)", 
                    fontWeight: "bold", 
                    fontSize: 16}}>
                {props.title}
            </Text>
            <Text style={{ 
                marginLeft: 0, 
                marginTop: 5, 
                color: "#666", 
                fontSize: 13, 
                fontStyle: "italic", 
                fontWeight: "100"}}>
                by {props.artist} : {props.title}
            </Text>
        </View>
        <View style={{width: "12.5%", alignItems: "center", flexDirection: "row"}}>
            <TouchableOpacity
                onPress={() => {
                    props.navigation.push("MusicPlayer", {
                        src: props.musicImage,
                        title: props.title,
                        artist: props.artist,
                        audio: props.audio,
                        video: props.video,
                        genre: props.genre,
                        id: props.id,
                        token: props.usertoken,
                        navigation: props.navigation,
                    })
                }}
                style={{flex: 1}}
            >
                <Image
                    source={require("../assets/images/play.png")}
                    resizeMode="contain"
                    style={styles.statimage}
                ></Image>
            </TouchableOpacity>
        </View>
        <View style={{width: "12.5%", alignItems: "center", flexDirection: "row"}}>            
            <TouchableOpacity
                onPress={() => {
                    _deleteDownloads(props.id)
                }}
                style={{flex: 1}}
            >
                <Image
                    source={require("../assets/images/delete.png")}
                    resizeMode="contain"
                    style={styles.statimage}
                ></Image>
            </TouchableOpacity>
        </View>
    </View>
    );
}


const styles = StyleSheet.create({
    statimage: {
        width: 15,
        height: 15,
        marginTop: 20,
        marginRight: 0,
        flex: 1,
        marginLeft: 0,
        alignSelf: "center",
        tintColor: "rgba(241, 98, 35, 1)",
        justifyContent: "center",        
    },
    musicimage: {
        width: 50,
        height: 50,
        borderRadius: 5,
        marginTop: 5,
        marginLeft: 0,
        // tintColor: "rgba(241, 98, 35, 1)",
        // alignSelf: "center"
    },
    commentimage: {
        width: 15,
        height: 15,
        marginTop: 0,        
        // tintColor: "#05cd51",
        alignSelf: "flex-end"
    },
    capsules: {
        // borderWidth: 1,
        // borderTopColor: "transparent",
        // borderLeftColor: "transparent",
        // borderRightColor: "transparent",
        // borderBottomColor: "#ddd",
        flexDirection: "row",
        width: "85%",
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 10
    },
    divider: {
        height: 1,
        backgroundColor: "#000",
        alignSelf: "stretch"
    },
});


export default DownloadList;