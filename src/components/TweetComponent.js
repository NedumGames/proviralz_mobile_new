import React, { Component } from "react";
import { StyleSheet, View, Image, Text } from "react-native";
import IoniconsIcon from "react-native-vector-icons/Ionicons";
import EvilIconsIcon from "react-native-vector-icons/EvilIcons";
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";

function TweetComponent(props) {
  return (
    <View style={[styles.rect, props.style]}>
      <Image
        source={require("../assets/images/WcF2F1c9_400x400.png")}
        resizeMode="cover"
        style={styles.image}
      ></Image>
      <Text style={styles.text}>BuilderX</Text>
      <Text style={styles.text2}>@BuilderXio</Text>
      <Text style={styles.text3}>• 20 Jul</Text>
      <IoniconsIcon name="ios-arrow-down" style={styles.icon}></IoniconsIcon>
      <Text style={styles.text4}>
        We are coming to @react_india{"\n"}Hype for the greatest Design -&gt;
        Code #Tool {"\n"}is real &amp; we will celebrate it in #Goa this
        September.{"\n"}
        {"\n"}#design #react
      </Text>
      <View style={styles.rect2}>
        <View style={styles.rect3}>
          <EvilIconsIcon name="comment" style={styles.icon2}></EvilIconsIcon>
          <Text style={styles.text5}>20</Text>
        </View>
        <View style={styles.rect4}>
          <EvilIconsIcon name="retweet" style={styles.icon3}></EvilIconsIcon>
          <Text style={styles.text6}>2K</Text>
        </View>
        <View style={styles.rect5}>
          <MaterialCommunityIconsIcon
            name="heart"
            style={styles.icon4}
          ></MaterialCommunityIconsIcon>
          <Text style={styles.text7}>20K</Text>
        </View>
        <EvilIconsIcon name="share-google" style={styles.icon5}></EvilIconsIcon>
        <IoniconsIcon name="ios-stats" style={styles.icon6}></IoniconsIcon>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  rect: {},
  image: {
    top: 2,
    left: 0,
    width: 60,
    height: 60,
    position: "absolute",
    borderRadius: 100,
    borderColor: "#1c2a38",
    borderWidth: 0
  },
  text: {
    top: 0,
    left: 68,
    color: "rgba(255,255,255,1)",
    position: "absolute",
    fontSize: 16,
    lineHeight: 20
  },
  text2: {
    top: 0,
    left: 138,
    color: "#798894",
    position: "absolute",
    fontSize: 14,
    lineHeight: 20
  },
  text3: {
    top: 0,
    left: 230,
    color: "#798894",
    position: "absolute",
    fontSize: 14,
    lineHeight: 20
  },
  icon: {
    top: 0,
    left: 319,
    position: "absolute",
    color: "#798894",
    fontSize: 20
  },
  text4: {
    top: 22,
    left: 68,
    color: "rgba(255,255,255,1)",
    position: "absolute",
    fontSize: 10,
    lineHeight: 15
  },
  rect2: {
    top: "82.26%",
    left: 68,
    width: 258,
    height: 20,
    position: "absolute",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  rect3: {
    width: 39,
    height: 20
  },
  icon2: {
    color: "#8394a1",
    fontSize: 20,
    position: "absolute",
    left: 0,
    top: 0
  },
  text5: {
    top: 4,
    left: 24,
    color: "#8394a1",
    position: "absolute",
    fontSize: 12
  },
  rect4: {
    width: 37,
    height: 20
  },
  icon3: {
    color: "#17b45e",
    fontSize: 20,
    position: "absolute",
    left: 0,
    top: 0
  },
  text6: {
    top: 4,
    left: 22,
    color: "#8394a1",
    position: "absolute",
    fontSize: 12
  },
  rect5: {
    width: 45,
    height: 20,
    flexDirection: "row",
    alignItems: "stretch",
    alignSelf: "center",
    justifyContent: "space-between"
  },
  icon4: {
    color: "rgba(208,2,27,1)",
    fontSize: 20,
    alignSelf: "stretch"
  },
  text7: {
    color: "#8394a1",
    alignSelf: "center",
    fontSize: 12
  },
  icon5: {
    color: "#8394a1",
    fontSize: 20,
    alignSelf: "stretch"
  },
  icon6: {
    color: "#8394a1",
    fontSize: 20,
    alignSelf: "stretch"
  }
});

export default TweetComponent;
