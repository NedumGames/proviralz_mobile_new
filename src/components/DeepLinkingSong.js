import React, { useState, useEffect } from "react";
import {
    Linking,
    Platform
} from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
import Loading from "../components/Loading";
import * as base64 from "base-64";
import * as utf8 from "utf8";


function SongLinking(props) {

    const [loading, setLoading] = useState(false);

    useEffect(() => {

        AsyncStorage.getItem('userdetails', (err, _data) => {
            if (_data){
                if (Platform.OS === 'android') {
                    Linking.getInitialURL().then(url => {
                        navigate(url);
                    }).catch(e => {});
                } else {
                    Linking.addEventListener('url', handleOpenURL);
                }
            }
        })

        Linking.removeEventListener('url', handleOpenURL);
    }, [])

    const getSong = async (id) => {
        setLoading(true)
        await fetch("https://proviralzmobileapp.proviralz.com/api/musicplay/" + id, {
            method: 'GET', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
        })
            .then(res => res.json())
            .then(rest__ => {
                setLoading(false)
                if (rest__.success) {
                    var music_ = rest__.data
                    props.navigation.push('MusicPlayer',
                        {
                            id: music_.id,
                            src: music_.cover,
                            title: music_.title,
                            artist: music_.artist,
                            audio: music_.audio,
                            video: music_.video,
                            genre: music_.genre
                        }
                    )
                }
            })
            .catch((e) => {
                setLoading(false)
            })
    }

    const handleOpenURL = (event) => { // D
        navigate(event.url);
    }
    const navigate = (url) => { // E    
        const route = url.replace(/.*?:\/\//g, '');
        const id = route.match(/\/([^\/]+)\/?$/)[1];
        const routeName = route.split('/')[1];
        if (routeName === 'share') {
            var encodedText = utf8.decode(id);
            getSong(base64.decode(encodedText))
        };
    }

    return(
        <Loading load={false} />
    )

}

export default SongLinking;