import React, { Component, useEffect, useState } from "react";
import { Alert, StyleSheet, View, Image, Text, TouchableOpacity } from "react-native";
import Divider from "../components/Divider";
import AsyncStorage from '@react-native-community/async-storage';
import Loading from "../components/Loading";


function Transaction(props) {

    const [showdata, setShowdata] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        fetchTransaction()
    }, [])

    async function fetchTransaction() {

        const userdetails = await AsyncStorage.getItem('userdetails', async (err, res) => {
            const _res = JSON.parse(res);
            if (_res)
                setLoading(true)
            await fetch("https://proviralzmobileapp.proviralz.com/api/viewmytrans/", {
                method: 'GET', // *GET, POST, PUT, DELETE, etc.
                mode: 'cors', // no-cors, *cors, same-origin
                cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                credentials: 'same-origin', // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    "Authorization": `Bearer ${_res.token}`
                },
                // body: JSON.stringify(_data) // body data type must match "Content-Type" header
            })
                .then(res => res.json())
                .then((result) => {
                    setLoading(false)
                    if (!result.success) {
                        Alert.alert(
                            'Data Error.',
                            `Hi!, ${result.message}`,
                            [
                                {
                                    text: 'Ok',
                                }
                            ],
                            { cancelable: false }
                        );
                        return;
                    } else {
                        setShowdata(result.data)
                    }
                })
                .catch((e) => {
                    setLoading(false)
                    Alert.alert(
                        'Data Error.',
                        `Hi!, there is an issue fetching your transaction.`,
                        [
                            {
                                text: 'Ok',
                            },
                            {
                                text: 'Retry',
                                onPress: () => fetchTransaction()
                            }
                        ],
                        { cancelable: false }
                    );
                })
        });
    }

    function displayTrans() {

        if (Array.isArray(showdata) && showdata.length) {

            for (let i = 0; i < showdata.length; i++) {

                return (
                    <View key={i} style={styles.capsules}>
                        <View style={{ flex: 1, height: 50 }}>
                            <Text style={{ marginLeft: 20, marginTop: 5, color: "#666" }}>Date</Text>
                            <Text style={{ marginLeft: 20, marginTop: 5, color: "#000", fontWeight: "bold" }}>{showdata[i].created_at.substring(0, showdata[i].created_at.lastIndexOf('T'))}</Text>
                        </View>
                        <View style={{ flex: 1, height: 50 }}>
                            <Text style={{ marginLeft: 20, marginTop: 5, color: "#666" }}>Amount</Text>
                            <Text style={{ marginLeft: 20, marginTop: 5, color: "#000", fontWeight: "bold" }}>&#8358; {showdata[i].amount}</Text>
                        </View>
                        <View style={{ flex: 1, height: 50 }}>
                            <Text style={{ marginLeft: 20, marginTop: 5, color: "#666" }}>Status</Text>
                            {showdata[i].status !== 'PENDING' ?
                                <Text style={{ marginLeft: 20, marginTop: 5, color: "#05cd51" }}>Done</Text> :
                                <Text style={{ marginLeft: 20, marginTop: 5, color: "rgba(241, 98, 35, 1)" }}>Pending</Text>}
                        </View>
                    </View>
                )
            }
        }
        else return (
            <View
                style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: "center"
                }}
            >
                <Text
                    style={{
                        width: 200,
                        color: "rgba(0,0,0,0.3)",
                        fontSize: 13,
                        textAlign: "center",
                        marginBottom: 20,
                        lineHeight: 25
                    }}
                >No transactions available at the moment, make withdrawals on the profile page.</Text>
                <TouchableOpacity
                    style={{
                        borderWidth: 1,
                        borderRadius: 5,
                        borderColor: "#ddd",
                        padding: 10
                    }}
                    onPress={() => props.navigation.push("Profile")}
                >
                    <Text
                        style={{
                            color: "#666",
                            fontSize: 14
                        }}
                    >Go to Profile </Text>
                </TouchableOpacity>
            </View>
        )
    }

    return (
        <View style={{ flex: 1 }}>
            {/* <Loading load={loading} /> */}
            {displayTrans()}
        </View>
    )
}

const styles = StyleSheet.create({
    statimage: {
        width: 15,
        height: 15,
        marginTop: 20,
        marginRight: 20,
        alignSelf: "flex-end",
        tintColor: "rgba(241, 98, 35, 1)",
        justifyContent: "center",
    },
    capsules: {
        borderWidth: 1,
        borderTopColor: "transparent",
        borderLeftColor: "transparent",
        borderRightColor: "transparent",
        borderBottomColor: "#ddd",
        flexDirection: "row",
        // width: "85%",
        marginTop: 10,
        paddingBottom: 10
    },
    divider: {
        height: 1,
        backgroundColor: "#000",
        alignSelf: "stretch"
    },
});

export default Transaction;