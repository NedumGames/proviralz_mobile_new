import React, { Component } from "react";
import { StyleSheet, Share, View, Image, Text, TouchableOpacity } from "react-native";
import Divider from "../components/Divider";
import Icon from "react-native-vector-icons/FontAwesome";
import { Avatar, List } from 'react-native-paper';
import * as base64 from "base-64";
import * as utf8 from "utf8";

function Earnings(props) {

    const data = props.musicData

    const show = []

    const editSong = (data) => {
        props.editsong(data)
    }

    if (data) {

        for (let i = 0; i < data.length; i++) {

            const shareText = () => {
                var encodedText = utf8.encode(data[i].id.toString());
                return base64.encode(encodedText)
            }

            const controls = () => {
                return (
                    <View key={i} style={styles.capsules}>
                        <View style={styles.ctrls}>
                            {data[i].status == 1 ?
                                <Icon name="check-circle" size={25} color="#05cd51" /> :
                                <Icon name="hourglass" size={20} color="rgba(241, 98, 35, 1)" />}
                        </View>
                        <View style={styles.ctrls}>
                            <TouchableOpacity
                                onPress={() => Share.share({
                                    message: `hi fella, please view my song on Proviralz mobile app and share it as well; ${data[i].title} By ${data[i].artist} - http://www.proviralzapp.com/share/${shareText()}`,
                                    title: `${data[i].title} By ${data[i].artist}`
                                })}
                            >
                                <Icon name="share" size={20} color="#6d6e6f" />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.ctrls}>
                            <TouchableOpacity
                                onPress={() => {
                                    props.navigation.push("MusicPlayer", {
                                        src: data[i].cover,
                                        title: data[i].title,
                                        artist: data[i].artist,
                                        audio: data[i].audio,
                                        video: data[i].video,
                                        genre: data[i].genre,
                                        id: data[i].id,
                                        token: props.usertoken,
                                        navigation: props.navigation
                                    })
                                }}
                            >
                                <Icon name="play" size={20} color="#6d6e6f" />
                            </TouchableOpacity>
                        </View>

                    </View>
                )
            }

            const title = () => {
                return (
                    <View style={{ flexDirection: "column" }}>
                        <Text style={{ fontSize: 15 }}>{data[i].title}</Text>
                        <Text style={{ fontSize: 11, color: "#660" }}>View: {data[i].views}</Text>
                        <Text style={{ fontSize: 11, color: "#660" }}>Earnings: &#8358; {data[i].earnings}</Text>
                        <Text style={{ fontSize: 11, color: "#660" }}>Likes: {data[i].likes}</Text>
                    </View>
                )
            }

            show.push(
                <>
                    <List.Item
                        style={{
                            marginBottom: 0,
                            marginTop: 0,
                            marginLeft: 20,
                            marginRight: 20,
                            justifyContent: "center",
                            alignContent: "center",
                        }}
                        titleStyle={{ fontSize: 13 }}
                        title={title()}
                        descriptionStyle={{ width: "100%", justifyContent: "space-between", }}                        
                        left={props => <Avatar.Image size={50} style={{ marginRight: 10, marginTop: 15, justifyContent: "center" }} source={{ uri: data[i].cover }} />}
                        right={props => controls()}
                    />
                    <Divider style={{width: "80%", alignSelf: "center"}} />
                </>
            )
        }

        return (show);
    }

    return null
}

const styles = StyleSheet.create({
    ctrls: {
        marginTop: 5,
        marginLeft: 0,
        flex: 1,
        justifyContent: "center",
        alignContent: "center"
    },
    statimage: {
        width: 15,
        height: 15,
        marginTop: 20,
        marginRight: 20,
        alignSelf: "flex-end",
        tintColor: "rgba(241, 98, 35, 1)",
        justifyContent: "center",
    },
    musicimage: {
        width: 50,
        height: 50,
        borderRadius: 100,
        marginTop: 10,
        justifyContent: "center",
        marginLeft: 20,
        alignSelf: "center"
    },
    capsules: {
        flexDirection: "row",
        marginBottom: 10,
        width: "40%",
    },
    divider: {
        height: 1,
        backgroundColor: "#000",
        alignSelf: "stretch"
    },
});

export default Earnings;