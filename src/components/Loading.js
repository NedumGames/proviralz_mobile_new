import React, { useState, Component } from "react";
import { StyleSheet, View, Image, Text, BackHandler, ActivityIndicator } from "react-native";
import Modal from 'react-native-modal';

function Loading(props) {

    const { load } = props

    return (
        <Modal
            isVisible={load}
            animationIn="slideInUp"
            animationInTiming={500}
            animationOut="slideOutDown"
            animationOutTiming={500}
        >
            <View
                style={{
                    flex: 1, justifyContent: "center", alignItems: "center"
                }}
            >
                <Text style={{color: "rgba(241, 98, 35, 0.7)", fontSize: 13}}>Processing.....</Text>
                <ActivityIndicator size="large" animating color="rgba(241, 98, 35, 1)" />
            </View>
        </Modal>
    )
}

export default Loading;