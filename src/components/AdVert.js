import React, { useEffect, useState, Component } from 'react';
import { Button, Text } from 'react-native';
import admob, { BannerAd, BannerAdSize, InterstitialAd, AdEventType, TestIds, MaxAdContentRating } from '@react-native-firebase/admob';

export default class AdVert {

  adUnitIdINTERSTITIAL = __DEV__ ? TestIds.INTERSTITIAL : 'ca-app-pub-4700820342978062/7485885078';
  adUnitIdBANNER = __DEV__ ? TestIds.BANNER : 'ca-app-pub-4700820342978062/7485885078';

  interstitial = InterstitialAd.createForAdRequest(this.adUnitIdINTERSTITIAL, {
    requestNonPersonalizedAdsOnly: true,
    keywords: ['fashion', 'clothing', 'sport', 'medical', 'school', 'business'],
  });

  init() {
    admob()
      .setRequestConfiguration({
        // Update all future requests suitable for parental guidance
        maxAdContentRating: MaxAdContentRating.PG,

        // Indicates that you want your content treated as child-directed for purposes of COPPA.
        tagForChildDirectedTreatment: true,

        // Indicates that you want the ad request to be handled in a
        // manner suitable for users under the age of consent.
        tagForUnderAgeOfConsent: true,
      })
      .then(() => {
        // Request config successfully set!
      });
  }

  showInterstitialAd() {
    this.init();
    // Add event handlers
    this.interstitial.onAdEvent((type, error) => {
      if (type === AdEventType.LOADED) {
        this.interstitial.show();
      } else {
        return;
      }
    });

    // Load a new advert
    this.interstitial.load();
  }

   bannerAdsShow() {
    return (
      <BannerAd
        key={0}        
        unitId={this.adUnitIdBANNER}
        size={BannerAdSize.FULL_BANNER}
        requestOptions={{
          requestNonPersonalizedAdsOnly: true,
        }}
      />
    );
  }
}