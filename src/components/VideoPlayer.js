import React, { Component, useState } from "react";
import {
    StyleSheet,
    Dimensions
} from "react-native";
import Video from 'react-native-video';
import VideoPlayer from 'react-native-video-player';
import Modal from 'react-native-modal';
import * as RNFS from 'react-native-fs';

// import RNFS from 'react-native-fs'
// const downloadToFile = (base64Content, contentId) => {
// 	const path = `file://${RNFS.DocumentDirectoryPath}/${contentId}.mp4`
// 	RNFS.writeFile(path, base64Content, 'base64')
// 	.then(success => {
// 		console.log('FILE WRITTEN: ', versionId)
// 	})
// 	.catch(err => {
// 		console.log('File Write Error: ', err.message)
// 	})
// }
// Then in the video component, you can retrieve the file by its contentId:

// <Video source={{ uri: `file://${RNFS.DocumentDirectoryPath}/${contentId}.mp4`}} />

export default function VideoPlay(props) {

    const { onToggleHideModal, showModal } = props

    return (
        <Modal
            isVisible={showModal}
            animationIn="slideInUp"
            animationInTiming={500}
            animationOut="slideOutDown"
            animationOutTiming={1000}
            onBackButtonPress={() => {
                onToggleHideModal()
            }}
        >
            <VideoPlayer
                video={{ uri: props.video }}
                videoWidth={Math.round(Dimensions.get('window').width)}
                videoHeight={300}
                endWithThumbnail={true}
                fullScreenOnLongPress={true}
                thumbnail={{ uri: props.cover }}
                endThumbnail={{ uri: props.cover }}
                style={{
                    backgroundColor: "#000",
                    padding: 10,
                }}
            />
        </Modal>
    )
}

const styles = StyleSheet.create({
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
});

// export default VideoPlayer;