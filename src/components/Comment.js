import React from 'react';
import {Text, View, StyleSheet, Image, Platform, TouchableOpacity} from 'react-native';

function Comment(props){       
    
    const {username, userpic, comment} = props
    
    return (
    <View style={styles.capsules}>
        <View style={{width: "25%", height: 50 }}>
            <Image
                source={userpic != "" ? {uri: userpic} : require("../assets/images/profileuser.png")}
                resizeMode="cover"
                style={styles.musicimage}
            ></Image> 
        </View>
        <View style={{ width: "60%", marginBottom: 5 }}>
        <Text style={{ 
                marginLeft: 0, 
                marginTop: 5, 
                marginBottom: 0,
                color: "#fff", 
                fontSize: 13, 
                fontStyle: "italic", 
                fontWeight: "100"}}>
                @{username}
            </Text>
            <Text 
                style={{ 
                    marginLeft: 0,
                    marginTop: 0, 
                    color: "#fff", 
                    fontWeight: "bold", 
                    fontSize: 13}}>
                {comment}
            </Text>                   
            
        </View>
        <View style={{width: "15%", height: 50, alignItems: "center" }}>
            {/* <TouchableOpacity
                // onPress={() => {props.navigation.navigate("MusicPlayer")}}
            >
                <Image
                    source={require("../assets/images/heart.png")}
                    resizeMode="contain"
                    style={styles.statimage}
                ></Image>
            </TouchableOpacity> */}
        </View>
    </View>
    );
}


const styles = StyleSheet.create({
    statimage: {
        width: 20,
        height: 20,
        marginTop: 20,
        marginRight: 20,
        marginLeft: 20,
        alignSelf: "center",
        tintColor: "#fff",
        justifyContent: "center",        
    },
    musicimage: {
        width: 50,
        height: 50,
        borderRadius: 100,
        marginTop: 5,
        marginLeft: 0,
        // tintColor: "rgba(241, 98, 35, 1)",
        // alignSelf: "center"
    },
    commentimage: {
        width: 15,
        height: 15,
        marginTop: 0,        
        // tintColor: "#05cd51",
        alignSelf: "flex-end"
    },
    capsules: {
        // borderWidth: 1,
        // borderTopColor: "transparent",
        // borderLeftColor: "transparent",
        // borderRightColor: "transparent",
        // borderBottomColor: "#ddd",
        flexDirection: "row",
        width: "85%",
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 10
    },
    divider: {
        height: 1,
        backgroundColor: "#000",
        alignSelf: "stretch"
    },
});


export default Comment;