import React, { useState, Component } from "react";
import { StyleSheet, Text, BackHandler, View, Image, TouchableOpacity } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
import AdVert from "./AdVert";
import Icon from "react-native-vector-icons/FontAwesome";
import { BannerAd, BannerAdSize, TestIds } from '@react-native-firebase/admob';


export default class ToolBar extends Component {

    adUnitId = __DEV__ ? TestIds.BANNER : 'ca-app-pub-4700820342978062/5098111024';

    prop = null
    active
    state = {
        userid: 0,
        usertype: 1
    }

    constructor(props) {
        super(props)
        this.prop = props
        this.active = this.props.active
    }

    componentDidMount() {
        this._getUserDetails();
    }

    async _getUserDetails() {
        // const userdetails = await AsyncStorage.getItem('userdetails', (err, res) => {
        //     return res
        // })
        // this.setState({
        //     usertype: userdetails.user.type
        // })
    }

    componentWillUnmount() {

    }

    navigatePage(page) {
        this.props.navigation.navigate(page)
    }

    render() {
        return (
            <View style={styles.rect4}>
                <View
                    style={{
                        position: "absolute",
                        bottom: 48,
                        left: 0,
                        right: 0,
                        backgroundColor: "transparent"
                    }}
                >
                    <Text style={{fontSize: 6, marginLeft: 10}}>Ads.</Text>
                    <BannerAd
                        unitId={this.adUnitId}
                        size={BannerAdSize.FULL_BANNER}
                        requestOptions={{
                            requestNonPersonalizedAdsOnly: true,
                        }}
                        key={1}                        
                    />
                </View>
                <TouchableOpacity
                    onPress={() => this.navigatePage("Home")}
                    style={{ flex: 1 }}
                >
                    <Image
                        source={require("../assets/images/home.png")}
                        resizeMode="contain"
                        style={this.active == 1 ? styles.activeButton : styles.localButton}
                    ></Image>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => this.navigatePage("Upload")}
                    style={{ flex: 1 }}
                >
                    <Image
                        source={require("../assets/images/upload.png")}
                        resizeMode="contain"
                        style={this.active == 2 ? styles.activeButton : styles.localButton}
                    ></Image>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => this.navigatePage("Activity")}
                    style={{ flex: 1 }}
                >
                    <Image
                        source={require("../assets/images/activity.png")}
                        resizeMode="contain"
                        style={this.active == 3 ? styles.activeButton : styles.localButton}
                    ></Image>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => this.navigatePage("Profile")}
                    style={{ flex: 1 }}
                >
                    <Image
                        source={require("../assets/images/user.png")}
                        resizeMode="contain"
                        style={this.active == 4 ? styles.activeButton : styles.localButton}
                    ></Image>
                </TouchableOpacity>
                {/* {this.state.usertype === 0 ?
                    <TouchableOpacity
                        onPress={() => this.navigatePage("AdminScreen", {
                            userid: this.state.userid,
                            token: this.state.token,
                            type: this.state.usertype
                        })}
                        style={{ flex: 1 }}
                    >
                        <Image
                            source={require("../assets/images/notebook.png")}
                            resizeMode="contain"
                            style={this.active == 5 ? styles.activeButton : styles.localButton}
                        ></Image>
                    </TouchableOpacity>
                    :
                    null

                } */}
            </View>
        )
    }
}
const styles = StyleSheet.create({
    rect4: {
        height: 50,
        flexDirection: "row",
        backgroundColor: "#000",
        marginTop: 0,
    },
    localButton: {
        flex: 1,
        width: 25,
        height: 25,
        marginTop: 10,
        marginBottom: 10,
        alignSelf: "center",
        tintColor: "#ffffff"
        // tintColor: "rgba(241, 98, 35, 1)"
    },
    activeButton: {
        flex: 1,
        width: 25,
        height: 25,
        marginTop: 10,
        marginBottom: 10,
        alignSelf: "center",
        // tintColor: "rgba(241, 98, 35, 1)"
        tintColor: "#666"
    }
});

// export default ToolBar;