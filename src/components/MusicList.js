import React from 'react';
import { Text, View, StyleSheet, TouchableWithoutFeedback, Image, Platform, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import AdVert from "../components/AdVert";


function MusicList(props) {

    const advert = new AdVert();

    return (
        <TouchableWithoutFeedback
            onPress={() => {
                // advert.showInterstitialAd();
                props.navigation.push("MusicPlayer", {
                    src: props.musicImage,
                    title: props.title,
                    artist: props.artist,
                    audio: props.audio,
                    video: props.video,
                    genre: props.genre,
                    id: props.id,
                    token: props.usertoken,
                    navigation: props.navigation,
                })
            }}
        >
            <View style={styles.capsules}>
                <View style={{ width: "25%", height: 50 }}>
                    <Image
                        source={{ uri: props.musicImage }}
                        resizeMode="cover"
                        style={styles.musicimage}
                    ></Image>
                </View>
                <View style={{ width: "60%", height: 50 }}>
                    <Text
                        style={{
                            marginLeft: 0,
                            marginTop: 5,
                            color: "rgba(241, 98, 35, 1)",
                            fontWeight: "bold",
                            fontSize: 16
                        }}>
                        {props.title}
                    </Text>
                    <Text style={{
                        marginLeft: 0,
                        marginTop: 5,
                        color: "#666",
                        fontSize: 13,
                        fontStyle: "italic",
                        fontWeight: "100"
                    }}>
                        by {props.artist} : {props.title}
                    </Text>
                </View>
                <View style={{ width: "15%", height: 50, alignItems: "center" }}>
                    <TouchableOpacity
                        onPress={() => {
                            // advert.showInterstitialAd();
                            props.navigation.push("MusicPlayer", {
                                src: props.musicImage,
                                title: props.title,
                                artist: props.artist,
                                audio: props.audio,
                                video: props.video,
                                genre: props.genre,
                                id: props.id,
                                token: props.usertoken,
                                navigation: props.navigation,
                            })
                        }}
                    >
                        <Image
                            source={require("../assets/images/play.png")}
                            resizeMode="contain"
                            style={styles.statimage}
                        ></Image>
                    </TouchableOpacity>
                </View>
            </View>
        </TouchableWithoutFeedback>
    );
}


const styles = StyleSheet.create({
    statimage: {
        width: 20,
        height: 20,
        marginTop: 20,
        marginRight: 20,
        marginLeft: 20,
        alignSelf: "center",
        tintColor: "rgba(241, 98, 35, 1)",
        justifyContent: "center",
    },
    musicimage: {
        width: 50,
        height: 50,
        borderRadius: 5,
        marginTop: 5,
        marginLeft: 0,
        // tintColor: "rgba(241, 98, 35, 1)",
        // alignSelf: "center"
    },
    commentimage: {
        width: 15,
        height: 15,
        marginTop: 0,
        // tintColor: "#05cd51",
        alignSelf: "flex-end"
    },
    capsules: {
        // borderWidth: 1,
        // borderTopColor: "transparent",
        // borderLeftColor: "transparent",
        // borderRightColor: "transparent",
        // borderBottomColor: "#ddd",
        flexDirection: "row",
        width: "85%",
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 10
    },
    divider: {
        height: 1,
        backgroundColor: "#000",
        alignSelf: "stretch"
    },
});


export default MusicList;