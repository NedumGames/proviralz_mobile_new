export class NetworkUTIL{
    async GET(URL, {HEADER: HEADER}){
        await fetch(URL, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: HEADER,
      })
        .then(res => res.json())
        .then((result) => {
            return result;
        })
        .catch((e) => {
            return e;
        })
    }

    async POST(URL, {BODY: BODY, HEADER: HEADER}){
        await fetch(URL, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: HEADER,
        body: JSON.stringify(BODY) // body data type must match "Content-Type" header
      })
        .then(res => res.json())
        .then((result) => {
            return result;
        })
        .catch((e) => {
          return e;
        })
    }
}