import React, { useState } from 'react';
// import Datastore from 'react-native-local-mongodb';
var Datastore = require('react-native-local-mongodb')

class DataManager {

    user = ""
    upload = ""
    music = ""

    constructor() {
        this.user = new Datastore({ filename: 'user', autoload: true });
        upload = new Datastore({ filename: 'upload', autoload: true });
        music = new Datastore({ filename: 'music', autoload: true });
    }

    insertUser = (data) => {
        this.user.insert([{a: 5}, {a: 7}], function(err, docs){
            return docs
        })
    }

    updateUser(data) {
        this.user.update({
            _id: data.id,
        }, data, {}, (err, res) => {
            if (err)
                return err;

            return res;
        })
    }

    getUserById() {
        this.user.find({}, function(err, docs){
            return docs
        })
    }

    deleteUser(userid) {
        this.user.remove({}, {}, (err, user) => {
            if (err)
                return err;

            return user
        })
    }

    musicInsert(data) {
        music.insert([data], (err, allmusic) => {
            if (err)
                return err;

            return allmusic
        })
    }

    updateMusic(data) {
        music.update({
            id: data.id,
        }, data, {}, (err, res) => {
            if (err)
                return err;

            return res;
        })
    }

    getmusic() {
        music.find({}, (err, allmusic) => {
            if (err)
                return err;

            return allmusic;
        })
    }
}

export default DataManager;